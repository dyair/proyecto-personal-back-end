import Server from './config/middleware/server';
import Express from 'express';
import path from 'path';
import bodyParser = require('body-parser');
import cors from 'cors';
import router from './rutas/rutas.inicio';
import router_mascota from './rutas/rutas.mascota'
import router_usuario from './rutas/rutas.usuario';
import router_config from './rutas/rutas.config';

const server: Server = Server.getInstance();

//Configuración para capturar parametros en llamadas API.
server.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
server.app.use(bodyParser.json({ limit: '50mb' }));

//Permitir que las consultas sean desde lugares distintos al host del server
server.app.use(cors({ origin: true, credentials: true }));

//Para que se puedan acceder a los archivos del server desde el front. YA NO SE UTILIZA.
server.app.use('/uploads', Express.static(path.join(__dirname, '..', 'uploads')));

//Rutas de servicios
server.app.use('/inicio', router); 
server.app.use('/usuario', router_usuario);
server.app.use('/mascota', router_mascota);
server.app.use('/configuracion', router_config);

server.start(function () {

    // heroku git:remote -a mascota-app
    // YA EJECUTÉ ESE COMANDO EN ESTE REPOSITORIO (BackEnd/).

});

 // mascotaController.crearMensaje({
    //     cabecera_sms_id: 2,
    //     contenido: 'Probando idssddasd',
    //     fecha: new Date(),
    //     mascota_destino_id: 13,
    //     mascota_origen_id: 12,
    // })
    //     .then(rdo => console.log(rdo)); 

// Mascota.create({
    //     nombre_mascota: 'Lupo',
    //     raza_id: 1,
    //     temperamento_id: 1,
    //     tamanio_id: 1,
    //     fecha_nac: new Date(),
    //     domicilio_id: 2,
    //     usuario_id: 9,
    //     sexo_id: 1 
    // })
    // .then(()=> {
    //     console.log('creado');
    // })