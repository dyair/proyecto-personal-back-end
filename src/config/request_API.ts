import http from 'http';
import ErrorGenerico from './customError';

// API request to other server. Just simple HTTP call to get/send data from/to other server.

const external_request = {

    getMapquest(url: string): Promise<any> {

        return new Promise((resolve, reject) => {

            let body: any = '';
            let direccion: any = {};

            http.get(url, (resp) => {

                resp.on('data', (data) => {
                    body += data;

                });

                resp.on('end', () => {
                    var rdo = JSON.parse(body);
                    direccion = rdo.results[0].locations[0];
                    resolve(direccion);

                }).on('error', (error: Error) => {
                    throw new ErrorGenerico(error.message, 503);
                });
            });
        });
    }

}

export default external_request;
