import { Socket } from 'socket.io';
import { Mensaje_, Relacion_ } from '../../../modelos/modelos';
import socketIO from 'socket.io';
import { Mascota_socket_login, Mascota_socket_ } from './mascota_socket';
import { ListaMascotaSocket } from './lista_mascotas_socket';
import mascotaController from '../../../controller/mascota.controller';
import ErrorGenerico from '../../customError';

export const petsConectados = new ListaMascotaSocket();

// Logica para las acciones que se usan en los sockets.

export const desconectar = (cliente: Socket, io: SocketIO.Server) => {

    cliente.on('disconnect', () => {

        // el disconnect() se dispara desde el front-end (cerrar sesión).
        petsConectados.eliminarPetSocketLista(cliente.id);

    });

}

export const configurar_socket_user = (cliente: Socket, io: socketIO.Server) => {

    cliente.on('nueva_mascota_socket', (user_socket: Mascota_socket_login, callback: Function) => {

        let nueva_mascota_socket: Mascota_socket_ = new Mascota_socket_(cliente.id, user_socket.id_mascota, user_socket.nombre_mascota);

        petsConectados.agregarPetSocketLista(nueva_mascota_socket);

        callback(`Mascota conectada: ${nueva_mascota_socket.nombre_mascota}`);

    });

}

export const enviar_mensaje_chat = (cliente: Socket, io: socketIO.Server) => {

    cliente.on('enviar_nuevo_mensaje', async (sms_nuevo: Mensaje_, callback: Function) => {

        let sms: Mensaje_ = {};

        sms = await mascotaController.crearMensaje(sms_nuevo);

        if (sms.mascota_destino_id) {

            let pet: Mascota_socket_ | undefined = petsConectados.getPetSocketFromListaByIDMascota(sms.mascota_destino_id);

            // dispara evento en directo si la mascota está conectada, sino nada.
            if (pet) io.in(pet.id_socket).emit('leer_nuevo_mensaje', sms);

        }

        callback(sms);

    });
}

// sin uso por el momento.
export const notificar_match = (cliente: Socket, io: socketIO.Server) => {

    // pre-condición: ambos likes deben estar en true && vista_receptora debe estar en falso.
    // Se llama desde el front cuando se retorne una relación con match.

    cliente.on('notificar_match_a_destino', async (relacion: Relacion_) => {

        if (!relacion.mascota_destino_id) throw new ErrorGenerico('Relación mal generada.', 404);

        const mascota_receptora: Mascota_socket_ | undefined = petsConectados.getPetSocketFromListaByIDMascota(relacion.mascota_destino_id);

        if (!mascota_receptora || !relacion.match) {
            console.log('mascota_destino a notificar offline o no tiene el match.');
            return;
        }

        io.in(mascota_receptora.id_socket).emit('notificacion_de_un_match', relacion);

        console.log('Notificada la relación a la mascota destino online: ', mascota_receptora.id_mascota);

    });

}
