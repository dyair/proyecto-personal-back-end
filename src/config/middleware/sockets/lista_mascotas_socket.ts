import { Mascota_socket_ } from './mascota_socket';


export class ListaMascotaSocket {

    private lista_sockets_usuarios: Mascota_socket_[];

    constructor() {

        this.lista_sockets_usuarios = []
    }

    public getListaPetSocket(): Mascota_socket_[] {

        return this.lista_sockets_usuarios;

    }

    public agregarPetSocketLista(usuario: Mascota_socket_) {

        this.lista_sockets_usuarios.push(usuario);

        console.log('LISTA AGREGADO: ', this.lista_sockets_usuarios);

    }

    public eliminarPetSocketLista(id_socket: string) {

        this.lista_sockets_usuarios = this.lista_sockets_usuarios.filter(socket => socket.id_socket !== id_socket);

        console.log('LISTA ELIMINADO: ', this.lista_sockets_usuarios);

    }

    public getPetSocketFromListaByIDSocket(id_socket: string): Mascota_socket_ | undefined {

        return this.lista_sockets_usuarios.find(socket => socket.id_socket === id_socket);

    }

    public getPetSocketFromListaByIDMascota(id_mascota: number): Mascota_socket_ | undefined {

        return this.lista_sockets_usuarios.find(socket => socket.id_mascota === id_mascota);

    }



}