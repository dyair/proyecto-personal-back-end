import jwt from 'jsonwebtoken';
import { clave_codificacion } from '../../global/ambiente';
import { User_ } from '../../modelos/modelos';
import { NextFunction, Request, Response } from 'express';


export const crearToken = (usuario: User_): string => {

    let token: string = jwt.sign(usuario, clave_codificacion, {
        expiresIn: '4h' // expires in 4hs
    });

    return `Bearer ${token}`;
}

export const validarToken = (token_cabecera: string): boolean => {

    let token_valido: boolean;
    let token: string = token_cabecera.split(" ")[1];

    try {
        jwt.verify(token, clave_codificacion);
        token_valido = true;
    } catch (error) {
        console.log(error.message);
        token_valido = false;
    }

    return token_valido;
}

export const autorizacion_token = (req: Request, resp: Response, next: NextFunction) => {

    let token: string | undefined = req.headers.authorization;

    if (typeof token === 'undefined') {

        return resp.status(401).json({
            ok: false,
            mensaje: 'Token de autenticación inválido.'
        });

    }

    let token_valido: boolean = validarToken(token);

    if (token_valido) next();
    else
        return resp.status(401).json({
            ok: false,
            mensaje: 'Token de autenticación inválido.'
        });

}