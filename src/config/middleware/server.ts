import Express from 'express';
import { SERVER_PORT } from '../../global/ambiente';
import HTTP from 'http';
import socketIO from 'socket.io';
import * as socket_controller from './sockets/socket_controller';
import { validarToken } from './jwtauth';
import ErrorGenerico from '../customError';

export default class Server {

    private static _instance: Server;

    public app: Express.Application;
    public puerto: string;
    public io: socketIO.Server;
    private http_server: HTTP.Server;

    private constructor() {

        this.app = Express();
        this.puerto = SERVER_PORT;

        this.http_server = new HTTP.Server(this.app);
        this.io = socketIO(this.http_server);

        this.escucharSocket();
    }

    start(calllback: any) {
        this.http_server.listen(this.puerto, calllback);
    }

    public static getInstance(): Server {
        return this._instance || (this._instance = new this());
    }

    private escucharSocket() {

        this.io
            .use((socket: socketIO.Socket, next: Function) => {

                if (!socket.handshake.query.token) throw new ErrorGenerico('Socket sin los parámetros de autenticación necesarios.', 401);

                let token: string = socket.handshake.query.token;
                let token_valido: boolean = validarToken(token);

                if (!token_valido) throw new ErrorGenerico('Error de autenticación. Token no válido.', 401);

                next();

            })
            .on('connection', (cliente: socketIO.Socket) => {

                // Set nueva mascota socket.
                socket_controller.configurar_socket_user(cliente, this.io);

                // Enviar sms de mascota_origen y enviarlo a mascota_destino.
                socket_controller.enviar_mensaje_chat(cliente, this.io);

                // notificar match a usuario si está online. Suspendido momentaneamente.
                socket_controller.notificar_match(cliente, this.io);

                // Desconectar mascota socket.
                socket_controller.desconectar(cliente, this.io);

            });
    }
}