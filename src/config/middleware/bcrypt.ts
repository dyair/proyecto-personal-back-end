import bcrypt from 'bcrypt';
import ErrorGenerico from '../customError';

export const encriptar_pw = async (pw: string): Promise<string> => {

    const saltos_round: number = 11;
    let pw_encriptada: string;

    try {
        pw_encriptada = await bcrypt.hash(pw, saltos_round);
    } catch (e) {
        throw new ErrorGenerico(e.message, 503);
    }

    return pw_encriptada;
}

export const validar_pw = async (pw_ingresada: string, pw_encriptada: string): Promise<boolean> => {

    let pw_valida: boolean = false;

    pw_valida = await bcrypt.compare(pw_ingresada, pw_encriptada);

    return pw_valida;

}