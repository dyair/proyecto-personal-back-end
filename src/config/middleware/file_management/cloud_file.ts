/* GESTIÓN DE IMÁGENES EN LA NUBE (cloudinary) */

// Información para usar multer, cloudinary y expressjs.
// https://medium.com/@joeokpus/uploading-images-to-cloudinary-using-multer-and-expressjs-f0b9a4e14c54

export const cloudinary = require('cloudinary').v2;

const DataURI = require('datauri');

import ErrorGenerico from "../../customError";
import { borrar_file_servidor } from "./uploads";
import { File_multer, Image_cloudinary } from '../../../modelos/modelo_tecnico';
import path from 'path';

cloudinary.config({
    cloud_name: 'df6a7tbc3',
    api_key: '478571369486339',
    api_secret: 'GcIwveycrH42apscedMssxepwPU'
});

export const subirFileCloudinaryDisk = (path_file: string): string => {

    let path_return: string = '';

    cloudinary.uploader.upload(path_file, (e: Error, rdo: any) => {

        if (e) {
            console.error(e);
            throw new ErrorGenerico(e.message + '. || ERROR SUBIENDO FILE A Cloudinary.', 503);
        }

        borrar_file_servidor(path_file);

        path_return = rdo.secure_url;

    });

    return path_return;
}

export const subirFileCloudinaryBuffer = async (buffer_content: any): Promise<string> => {

    let opcional_parameters = {};
    let path_return: string = '';
    let resultado: Image_cloudinary;

    try {
        resultado = await cloudinary.uploader.upload(buffer_content, opcional_parameters);
    } catch (e) {
        throw new ErrorGenerico(e.message + ' || ERROR SUBIENDO FILE A Cloudinary.', 503);
    }

    path_return = resultado.secure_url;

    return path_return;
}

export const getDataURI = (file_from_multer: File_multer) => {

    const datauri = new DataURI();

    datauri.format(path.extname(file_from_multer.originalname).toString(), file_from_multer.buffer);

    return datauri;

}
