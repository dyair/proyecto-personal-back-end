import multer from "multer";
import path from 'path';
import ErrorGenerico from "../../customError";
import fs from 'fs';
import { Request } from 'express';

// Guardar file en memoria as buffer. OJO subir cosas pesadas o muchos files chicos, puede haber overflow memory.
const configMulterMemory = multer.memoryStorage();  

// Config para guardar el file en disco.
const configMulterDisk = multer.diskStorage({
    destination: async function (req, file, cb) {
        await cb(null, path.join(__dirname, '..', '..', '..', '..', 'uploads/'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + path.extname(file.originalname));
    },
});

const filtros = function (req: Request, file: Express.Multer.File, cb: Function) {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        throw new ErrorGenerico('ERROR --> Formato de archivo no permitido, solamente imágenes.', 404);
        // cb(null, false);
    }
};

// esta lanzando siempre una excepción aunque borra el archivo.
export const borrar_file_servidor = (path_servidor_img: string) => {

    // Obtener la ruta absoluta del servidor donde se guardan las imagenes.
    let ruta: string = path_servidor_img.substring(path_servidor_img.indexOf('/uploads/'));
    let ruta_folder_img: string = path.join(__dirname, '..', '..', '..', ruta);

    fs.unlink(ruta_folder_img, (err: NodeJS.ErrnoException) => {
        if (err) {
            console.error(err);
            throw new ErrorGenerico('Excepción guardando el archivo en el servidor local antes de subirlo a la nube. Se suspende el proceso de subida.', 503);
        }

        console.log('File ' + ruta_folder_img + ' was deleted!');
    });
};

export const upload = multer({
    storage: configMulterMemory, // configMulterDisk --> el disk guarda en el disco del servidor.
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter: filtros
});