
export default class ErrorGenerico extends Error {

    public status_code: number;     

    constructor(sms: string, status: number) {
        super(sms);
        this.status_code= status;
    }

    // ErrorGenerico reeemplaza a Error (para poder usar el atributo status) en los endpoint.

}
