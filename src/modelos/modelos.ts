import { File_multer } from './modelo_tecnico';

export interface User_ {
  id?: number;
  nombre?: string;
  email?: string;
  password?: string;
  token?: string;
  google?: boolean
}

export interface Sexo_ {
  id_sexo?: number,
  nombre_sexo?: string
}

export interface Raza_ {
  id_raza?: number,
  nombre_raza?: string
}

export interface Tamanio_ {
  id_tamanio?: number,
  nombre_tamanio?: string
}

export interface Temperamento_ {
  id_temperamento?: number,
  nombre_temperamento?: string
}

export interface Tipo_ {
  id_tipo_mascota: number;
  nombre_tipo_mascota: string;
}

export interface Busqueda_reciente_ {
  id_busqueda?: number;
  fecha_busqueda?: Date;
  mascota_buscadora_id?: number;
  mascota_buscada_id?: number;
  mascota_buscada: Mascota_
}

export interface Rango_fechas_ {
  desde?: Date;
  hasta?: Date
}

export interface User_login {
  email: string;
  password: string;
}

export interface Pais_ {
  id_pais?: number;
  nombre_pais: string
  provincias?: Provincia_[]
}

export interface Mascota_ {
  id_mascota?: number;
  usuario_id?: number;
  nombre_mascota?: string;
  domicilio_id?: number;
  sexo_id?: number;
  biografia?: string;
  fecha_nac?: Date;
  tamanio_id?: number
  temperamento_id?: number;
  profile_pic?: string;
  raza_id?: number;
  tipo_mascota_id?: number;
  file_img?: File_multer;

  raza?: Raza_;
  temperamento?: Temperamento_;
  usuario?: User_;
  sexo?: Sexo_;
  domicilio?: Domicilio_;
  localidad?: Localidad_;
  provincia?: Provincia_;
  pais?: Pais_;
  tamanio?: Tamanio_;
  tipo?: Tipo_;
  busquedas?: Mascota_[]
}

export interface Domicilio_ {
  localidad_id?: number;
  id_domicilio?: number;
  calle?: string;
  numero?: number;
  piso?: number;
  depto?: string;
  latitud_map?: number;
  longitud_map?: number;
  mascotas?: Mascota_[]
}

export interface CoordenadasDom_ {
  latitud: number;
  longitud: number
}

export interface Provincia_ {
  id_provincia?: number;
  nombre_provincia: string;
  pais_id?: number;
  localidades?: Localidad_[]
}

export interface Localidad_ {
  nombre_localidad: string;
  id_localidad?: number;
  provincia_id?: number
}

export interface Relacion_ {
  id_relacion?: number;
  fecha?: Date;
  match?: boolean;
  mascota_origen_id?: number;
  mascota_destino_id?: number;
  mascota_origen?: Mascota_
  mascota_destino?: Mascota_
  visto_destino?: boolean;
  visto_origen?: boolean;
  like_destino?: boolean;
  like_origen?: boolean;
  match_final?: boolean;
}

export interface Relacion_datos_actualizados_ {
  id_mascota: number;
  visto: boolean;
  like: boolean;
}

export interface Mascota_resumen_ {
  id_mascota_resumen?: number;
  mascota_id?: number;
  mascota_fotos?: string[];
  foto_mascota1?: string;
  foto_mascota2?: string;
  foto_mascota3?: string;
}

export interface Post_ {
  id_post?: number,
  descripcion?: string,
  fecha_publicacion?: Date,
  mascota_id?: number,
  archivos_path?: string,
  archivos_array?: string[],
  files_uploaded?: File_multer[]
}

export interface Comunicacion_ {
  id_comunicacion?: number,
  mascota_destino_id?: number,
  mascota_origen_id?: number,
  mascota_destino?: Mascota_,
  mascota_origen?: Mascota_,
  mensajes?: Mensaje_[]
}

export interface Mensaje_ {
  id_mensaje?: number,
  cabecera_sms_id?: number,
  contenido?: string,
  mascota_destino_id?: number,
  mascota_origen_id?: number,
  fecha?: Date,
  visto_origen?: boolean,
  visto_destino?: boolean,
  mascota_origen?: Mascota_,
  mascota_destino?: Mascota_
}

/*
  SOCKETS INTERFACES
*/

// Objeto que guarda el id de una mascota junto con su like otorgado. Es un objeto de referencia.
export interface Par_pet_like_ {
  id_mascota: number,
  like: boolean,
  visto?: boolean
}

export interface Relacion_socket_ {
  origen_id: number;
  destino_id: number;
  like_origen: boolean;
  like_destino: boolean;
}