import { Domicilio_ } from '../modelos/modelos';
import { Domicilio } from '../database/modelos/ubicación/domicilio';
import ErrorGenerico from '../config/customError';

const ubicacionController = {

    async buscarDomicilio(id_dom: number): Promise<Domicilio_> {

        let dom: Domicilio_ = {};

        await Domicilio.findByPk(id_dom)
            .then((domicilio: Domicilio | null) => {

                if (!domicilio) throw new ErrorGenerico('Domicilio inexistente.', 404);

                dom = domicilio.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return dom;
    }
}

export default ubicacionController;