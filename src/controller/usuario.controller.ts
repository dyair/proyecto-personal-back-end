import Usuario from '../database/modelos/usuario';
import { User_, User_login, Mascota_ } from '../modelos/modelos';
import ErrorGenerico from '../config/customError';
import { Mascota } from '../database/modelos/mascota/mascota';
import { CLIENT_ID } from '../global/ambiente';
import mascotaController from './mascota.controller';
import { crearToken } from '../config/middleware/jwtauth';
import { encriptar_pw, validar_pw } from '../config/middleware/bcrypt';
import { filtro_include_mascota, filtro_include_ubicacion_dom_to_country } from '../database/include_sequelize_filters';

const { OAuth2Client } = require('google-auth-library');

const usuarioController = {

    // Verifica autenticación con Google.
    async verify(token: string): Promise<User_> {

        let user: User_ = {}

        try {

            const client = new OAuth2Client(CLIENT_ID);
            const ticket = await client.verifyIdToken({
                idToken: token,
                audience: CLIENT_ID
            });

            const payload = ticket.getPayload();

            user.email = payload.email;
            user.nombre = payload.given_name;
            user.password = '$&g00gle&$';
            user.google = true

        } catch (e) {
            throw new ErrorGenerico(e.message + '. Falla Auth0 de Google.', 503);
        }

        return user;

    },

    async crearUsuario(usuario: User_): Promise<User_> {

        let user: User_ = {};

        if (!usuario.email) throw new ErrorGenerico('Usuario con email vacío', 404);
        if (!usuario.nombre || !usuario.password) throw new ErrorGenerico('Usuario con nombre o pw vacía', 404);

        await Usuario.findAll({
            raw: true,
            where: {
                email: usuario.email
            }
        })
            .then((usuarios: Usuario[]) => {

                if (usuarios.length > 0) throw new ErrorGenerico('Correo electrónico ya utilizado. Intente con otro.', 404);

            }, ((error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            }));

        //encriptar pw y validar encriptación.
        usuario.password = await encriptar_pw(usuario.password);
        if (typeof usuario.password === "undefined") throw new ErrorGenerico('Error generando el hash para cifrar la pw en BD.', 503);

        await Usuario.create({
            nombre: usuario.nombre,
            password: usuario.password,
            email: usuario.email,
            google: usuario.google
        })
            .then((usuarioDB: Usuario) => {

                user = usuarioDB.get();

            }, ((error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            }));

        return user;

    },

    async iniciarSesion(usuario: User_login): Promise<User_> {

        let user: User_ = {};

        await Usuario.findOne({
            raw: true,
            where: {
                email: usuario.email
            }

        })
            .then((us: Usuario | null) => {

                // Valido que el mail no haya sido utilizado antes en el login gmail.

                if (!us) throw new ErrorGenerico('Correo electrónico no utilizado.', 401);

                user = us;

                if (user.google) throw new ErrorGenerico('Correo ya utilizado para registrarse vía Gmail. Inicie sesión por esa vía o utilice otro coreo.', 401);

            }, (error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            });

        if (!user.password) throw new ErrorGenerico('Usuario sin contraseña, error.', 404);

        let pw_valida: boolean = false;

        pw_valida = await validar_pw(usuario.password, user.password);

        if (pw_valida === false) throw new ErrorGenerico('Credenciales incorrectas.', 401);

        user.token = crearToken(user);

        return user;
    },

    async iniciarSesionByGoogle(usuario: User_): Promise<User_> {

        // Crear y loguearse con usuario de google, todo en uno. Funciona!

        let user: User_ = {};

        if (!usuario.email) throw new ErrorGenerico('Correo electrónico vacío', 404);

        await Usuario.findOrCreate({
            where: {
                email: usuario.email
            },
            defaults: {
                email: usuario.email,
                nombre: usuario.nombre,
                password: usuario.password, //no lo uso para gmail auth.
                google: usuario.google
            }
        })
            .then(([usuario, nuevo]) => {

                user = usuario.get();
                user.token = crearToken(usuario.get());

                console.log('usuario de google nuevo?: ', nuevo);

            }, (error) => {
                throw new ErrorGenerico(error.message, 503);
            });

        return user;

    },

    async getMascotasByUser(id_user: number): Promise<Mascota_[]> {

        let mascotas_: Mascota_[] = [];

        await Mascota.findAll({
            where: {
                usuario_id: id_user
            },
            include: [...filtro_include_mascota, filtro_include_ubicacion_dom_to_country]
        })
            .then((mascotas: Mascota[]) => {

                mascotas.forEach(mascota => {

                    let mas: Mascota_ = mascotaController.llenarMascota(mascota)
                    mascotas_.push(mas);

                });


            }, (error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            });

        return mascotas_;
    }
}

export default usuarioController;
