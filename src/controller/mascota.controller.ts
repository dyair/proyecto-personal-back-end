import {
    Mascota_, Domicilio_, Rango_fechas_, Provincia_, Relacion_,
    Mascota_resumen_, Post_, Mensaje_, Comunicacion_, Relacion_datos_actualizados_
} from '../modelos/modelos';
import ErrorGenerico from '../config/customError';
import { loc_hm_dom } from '../database/modelos/ubicación/domicilio';
import { Op } from 'sequelize';
import { SERVER_PORT, IP_SERVER_IMG, imagen_perfil_defecto } from '../global/ambiente';
import { Localidad, loc_bt_prov, prov_hm_loc } from '../database/modelos/ubicación/localidad';
import { Provincia, prov_bt_pais } from '../database/modelos/ubicación/provincia';
import { dom_hm_masc, Mascota } from '../database/modelos/mascota/mascota';
import ubicacionController from './ubicacion.controller';
import { Relacion, rel_bt_masco, rel_bt_mascd } from '../database/modelos/mascota/relacion';
import { Mascota_resumen } from '../database/modelos/mascota/mascota_resume';
import { Post_mascota } from '../database/modelos/mascota/post';
import { Comunicacion, masco_bt_cab, mascd_bt_cab } from '../database/modelos/mascota/comunicacion';
import { Mensaje } from '../database/modelos/mascota/mensaje';
import { Busqueda_reciente } from '../database/modelos/mascota/busqueda_reciente';
import { subirFileCloudinaryBuffer, getDataURI } from '../config/middleware/file_management/cloud_file';
import { filtro_include_mascota, filtro_include_ubicacion_dom_to_country } from '../database/include_sequelize_filters';

const mascotaController = {

    async crearMascota(masc_param: Mascota_, id_user: number): Promise<Mascota_> {

        if (!masc_param.nombre_mascota || !masc_param.tipo_mascota_id || !masc_param.sexo_id || !masc_param.usuario_id || !masc_param.raza_id) throw new ErrorGenerico('Cuerpo de mascota incorrecto.', 404);

        let m: Mascota_ = {};

        if (masc_param.file_img) {

            let data_uri: string = getDataURI(masc_param.file_img).content;
            masc_param.profile_pic = await subirFileCloudinaryBuffer(data_uri);

        } else {
            masc_param.profile_pic = imagen_perfil_defecto;
        }

        await Mascota.findOrCreate({
            where: {
                nombre_mascota: masc_param.nombre_mascota,
                sexo_id: masc_param.sexo_id,
                usuario_id: id_user,
                raza_id: masc_param.raza_id,
                tipo_mascota_id: masc_param.tipo_mascota_id
            },
            defaults: {
                nombre_mascota: masc_param.nombre_mascota,
                domicilio_id: masc_param.domicilio_id,
                sexo_id: masc_param.sexo_id,
                tamanio_id: masc_param.tamanio_id,
                fecha_nac: masc_param.fecha_nac,
                temperamento_id: masc_param.temperamento_id,
                profile_pic: masc_param.profile_pic,
                usuario_id: id_user,
                raza_id: masc_param.raza_id,
                tipo_mascota_id: masc_param.tipo_mascota_id
            }
        })
            .then(([mascotaDB, nuevo]) => {

                if (!nuevo) throw new ErrorGenerico('Ya tienes una mascota con esos datos :)', 404);

                m = mascotaDB.get();

            }, ((error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            }));

        if (!m.id_mascota) throw new ErrorGenerico('ID de la mascota nulo luego de crearla.', 404);

        let mascota: Mascota_ = {};

        mascota = await this.getMascotaByID(m.id_mascota);

        return mascota;
    },

    async editarMascota(masc_: Mascota_): Promise<Mascota_> {

        if (!masc_.id_mascota) throw new ErrorGenerico('Mascota inexistente al tratar de editar.', 404);

        if (masc_.file_img) {
            let data_uri: string = getDataURI(masc_.file_img).content;
            masc_.profile_pic = await subirFileCloudinaryBuffer(data_uri);
        }

        let atributos_a_editar: string[] = [];

        if (masc_.biografia) atributos_a_editar.push('biografia');
        if (masc_.fecha_nac) atributos_a_editar.push('fecha_nac');
        if (masc_.nombre_mascota) atributos_a_editar.push('nombre_mascota');
        if (masc_.domicilio_id) atributos_a_editar.push('domicilio_id');
        if (masc_.tamanio_id) atributos_a_editar.push('tamanio_id');
        if (masc_.file_img) atributos_a_editar.push('profile_pic'); // subio un archivo nuevo, cambio path imagen.
        if (masc_.temperamento_id) atributos_a_editar.push('temperamento_id');
        if (masc_.raza_id) atributos_a_editar.push('raza_id');

        await Mascota.update(masc_, {
            where: { id_mascota: masc_.id_mascota },
            fields: atributos_a_editar
        })
            .then(([cantidadRowsUpdate, rdos]) => {

                console.log(cantidadRowsUpdate);
            })
            .catch((e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        masc_ = await this.getMascotaByID(masc_.id_mascota);

        return masc_;

    },

    getRangoFecha(edad_desde: number, edad_hasta: number): Rango_fechas_ {

        let fechas: Rango_fechas_ = {};

        let fecha_hoy: Date = new Date();
        let mes: number = fecha_hoy.getMonth() + 1;
        let dia: number = fecha_hoy.getDate();

        fechas.desde = new Date(fecha_hoy.getFullYear() - edad_desde, mes, dia);
        fechas.hasta = new Date(fecha_hoy.getFullYear() - edad_hasta, mes, dia);

        return fechas;
    },

    calcularDistancia(lat1: number, long1: number, lat2: number, long2: number): number {

        // Si buscan mascota por radio, calcula la distancia a ver si entra en ese radio de kms.

        if (!lat1 || !long1 || !lat2 || !long2) throw new ErrorGenerico('Alguna coordenada de dirección es nula.', 404);

        let R: number = 6371; // Radius of the earth in km
        let dLat: number = (Math.PI / 180) * (lat2 - lat1);
        let dLon: number = (Math.PI / 180) * (long2 - long1);

        let a: number =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos((Math.PI / 180) * (lat1)) * Math.cos((Math.PI / 180) * (lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);

        let c: number = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d: number = R * c; // Distance in km

        return d;
    },

    async getMascotaByID(id_mascota: number): Promise<Mascota_> {

        let mascota_: Mascota_ = {};

        await Mascota.findByPk(id_mascota, {
            include: [...filtro_include_mascota, filtro_include_ubicacion_dom_to_country]
        })
            .then((mascota: Mascota | null) => {

                if (!mascota) throw new ErrorGenerico('Mascota inexistente', 404);

                mascota_ = this.llenarMascota(mascota);

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return mascota_
    },

    async getMascotasByProvincia(filtros: any, id_prov: number): Promise<Mascota_[]> {

        let mascotas_: Mascota_[] = [];

        await Provincia.findAll({
            where: {
                id_provincia: id_prov
            },
            include: [{
                association: prov_hm_loc,
                as: 'localidades',
                include: [{
                    association: loc_hm_dom,
                    as: 'domicilios',
                    include: [{
                        association: dom_hm_masc,
                        as: 'mascotas',
                        where: filtros,
                        include: filtro_include_mascota
                    }]
                }]
            },
            {
                association: prov_bt_pais,
                as: 'pais'
            }]
        })
            .then((rdo: any[]) => {

                let provincia_sql: any = {};
                let pais_sql: any = {};
                let localidad_sql: any = {};
                let domicilio_sql: any = {};

                for (let i = 0; i < rdo.length; i++) {

                    provincia_sql = rdo[i];
                    pais_sql = rdo[i].pais;

                    for (let x = 0; x < rdo[i].localidades.length; x++) {

                        localidad_sql = rdo[i].localidades[x];

                        for (let y = 0; y < rdo[i].localidades[x].domicilios.length; y++) {

                            domicilio_sql = rdo[i].localidades[x].domicilios[y];

                            for (let z = 0; z < rdo[i].localidades[x].domicilios[y].mascotas.length; z++) {

                                let mascota: Mascota_ = this.llenarMascotaByUbication(rdo[i].localidades[x].domicilios[y].mascotas[z], domicilio_sql, localidad_sql, provincia_sql, pais_sql);

                                mascotas_.push(mascota);

                            }

                        }

                    }

                }

            }, (error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            });

        return mascotas_;
    },

    async getMascotasByLocalidad(id_loc: number, filtros: any): Promise<Mascota_[]> {

        let mascotas_: Mascota_[] = [];

        await Localidad.findAll({

            where: {
                id_localidad: id_loc
            },
            include: [{
                association: loc_hm_dom,
                as: 'domicilios',
                include: [{
                    association: dom_hm_masc,
                    as: 'mascotas',
                    where: filtros,
                    include: filtro_include_mascota
                }]
            },
            {
                association: loc_bt_prov,
                as: 'provincia',
                include: [{
                    association: prov_bt_pais,
                    as: 'pais'
                }]
            }]
        })
            .then((rdo: any) => {

                let provincia_sql: any = {};
                let pais_sql: any = {};
                let localidad_sql: any = {};
                let domicilio_sql: any = {};

                for (let i = 0; i < rdo.length; i++) {

                    provincia_sql = rdo[i].provincia;
                    pais_sql = rdo[i].provincia.pais;
                    localidad_sql = rdo[i];

                    for (let x = 0; x < rdo[i].domicilios.length; x++) {

                        domicilio_sql = rdo[i].domicilios[x];

                        for (let y = 0; y < rdo[i].domicilios[x].mascotas.length; y++) {

                            let mascota: Mascota_ = this.llenarMascotaByUbication(rdo[i].domicilios[x].mascotas[y], domicilio_sql, localidad_sql, provincia_sql, pais_sql);

                            mascotas_.push(mascota);
                        }

                    }

                }

            }, (error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            });

        return mascotas_;
    },

    // capaz haya que editarlo también, busca domicilios 2 veces.
    async getMascotasByRadio(domicilio: Domicilio_, radio: number, filtros: any): Promise<Mascota_[]> {

        if (!domicilio.localidad_id) throw new ErrorGenerico('Domicilio no asociado a una localidad.', 404);

        let prov: Provincia_ = { pais_id: -1, nombre_provincia: '' };

        await Localidad.findAll({
            where: {
                id_localidad: domicilio.localidad_id
            },
            include: [{
                association: loc_bt_prov,
                as: 'provincia'
            }]
        })
            .then((localidad: any) => {

                prov = localidad[0].provincia.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        if (!prov.id_provincia) throw new ErrorGenerico('Provincia inexistente (ID nulo).', 404);

        let mascotas_cercanas: Mascota_[] = [];

        await Provincia.findOne({
            where: {
                id_provincia: prov.id_provincia
            },
            include: [{
                association: prov_hm_loc,
                as: 'localidades',
                include: [{
                    association: loc_hm_dom,
                    as: 'domicilios',
                    where: {
                        id_domicilio: {
                            [Op.ne]: domicilio.id_domicilio
                        }
                    },
                    include: [{
                        association: dom_hm_masc,
                        as: 'mascotas',
                        where: filtros,
                        include: filtro_include_mascota
                    }]
                }]
            }]
        })
            .then((provincia: any) => {

                for (let y = 0; y < provincia.localidades.length; y++) {        //localidades
                    for (let z = 0; z < provincia.localidades[y].domicilios.length; z++) {      //domicilios

                        let dom: Domicilio_ = provincia.localidades[y].domicilios[z].get();

                        if (!dom.longitud_map || !dom.latitud_map || !domicilio.latitud_map || !domicilio.longitud_map) throw new ErrorGenerico('Algún domicilio sin coordenadas.', 404);

                        let distancia: number = this.calcularDistancia(domicilio.latitud_map, domicilio.longitud_map, dom.latitud_map, dom.longitud_map);

                        if (distancia <= radio) {
                            for (let x = 0; x < provincia.localidades[y].domicilios[z].mascotas.length; x++) {      //mascotas de ese domicilio.
                                let masc: Mascota_ = provincia.localidades[y].domicilios[z].mascotas[x];
                                mascotas_cercanas.push(masc);
                            }
                        }
                    }
                }

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return mascotas_cercanas;
    },

    async getMascotasByFiltros(filtros_query: any, id_mascota: number): Promise<Mascota_[]> {

        let filtrosWhere: any = {
            id_mascota: {
                [Op.ne]: id_mascota    // no buscar la mascota que hace la consulta.
            }
        };

        // Establecer los filtros que se hayan seleccionado desde el usuario.

        if (filtros_query.sexo) filtrosWhere.sexo_id = { [Op.ne]: +filtros_query.sexo }  // para que no traiga el mismo sexo que la mascota que busca.
        if (filtros_query.tamanio) filtrosWhere.tamanio_id = +filtros_query.tamanio;
        if (filtros_query.temperamento) filtrosWhere.temperamento_id = +filtros_query.temperamento;
        if (filtros_query.raza) filtrosWhere.raza_id = +filtros_query.raza;
        if (filtros_query.nombre) filtrosWhere.nombre_mascota = filtros_query.nombre;
        if (filtros_query.edad_desde && filtros_query.edad_hasta) {
            let fechas: Rango_fechas_ = this.getRangoFecha(+filtros_query.edad_desde, +filtros_query.edad_hasta);
            filtrosWhere.fecha_nac = {
                [Op.lte]: fechas.desde,     // menor o igual a desde
                [Op.gte]: fechas.hasta      // mayor o igual a hasta
            };
        }

        let mascotas: Mascota_[] = [];

        // Dependiendo si hay filtro por ubicación, entra a alguno if o sale por el último else para buscar mascotas.
        // Radio, prov, localidad y 'ningún filtro ubicación' son if excluyentes (si entra a uno al otro no).

        if (filtros_query.radio) {

            let mascota: Mascota_ = await this.getMascotaByID(id_mascota);
            let dom_mascota: Domicilio_ = {};

            if (mascota.domicilio) dom_mascota = mascota.domicilio;

            if (!dom_mascota.id_domicilio) throw new ErrorGenerico('Mascota con domicilio inexistente.', 404);

            let domicilio: Domicilio_ = await ubicacionController.buscarDomicilio(dom_mascota.id_domicilio);

            mascotas = await this.getMascotasByRadio(domicilio, +filtros_query.radio, filtrosWhere);

        } else if (filtros_query.localidad) {

            mascotas = await this.getMascotasByLocalidad(+filtros_query.localidad, filtrosWhere);

        } else if (filtros_query.provincia) {

            mascotas = await this.getMascotasByProvincia(filtrosWhere, +filtros_query.provincia);

        } else {

            await Mascota.findAll({
                where: filtrosWhere,
                include: filtro_include_mascota
            })
                .then((mascotasBD: Mascota[]) => {

                    mascotasBD.forEach(mascota => {
                        let masc: Mascota_ = this.llenarMascota(mascota);
                        mascotas.push(masc);
                    });


                }, (e: Error) => {
                    throw new ErrorGenerico(e.message, 503);
                });

        }

        return mascotas;
    },

    // útil para las notificaciones. No se usa más.
    async getRelacionesNoVistasMascota(id_mascota: number): Promise<Relacion_[]> {

        let relaciones_: Relacion_[] = [];

        await Relacion.findAll({
            where: {
                match: true,
                [Op.or]: [
                    {
                        [Op.and]: [
                            { mascota_origen_id: id_mascota },
                            { visto_origen: false }
                        ]
                    },
                    {
                        [Op.and]: [
                            { mascota_destino_id: id_mascota },
                            { visto_destino: false }
                        ]
                    }
                ]
            },
            include: [
                {
                    model: Mascota,
                    association: rel_bt_masco,
                    as: 'mascota_origen'
                },
                {
                    model: Mascota,
                    association: rel_bt_mascd,
                    as: 'mascota_destino'
                }
            ]
        })
            .then((relaciones: any[]) => {

                relaciones.forEach(relacion => {

                    let rel: Relacion_ = relacion.get();
                    rel.mascota_destino = relacion.get().mascota_destino.get();
                    rel.mascota_origen = relacion.get().mascota_origen.get();

                    relaciones_.push(rel);

                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return relaciones_;

    },

    async crearPost(post_: Post_): Promise<Post_> {

        if (!post_.mascota_id) throw new ErrorGenerico('Post inválido, lo crea una mascota no existente.', 404);

        let posteo: any = {
            descripcion: post_.descripcion,
            fecha_publicacion: post_.fecha_publicacion,
            mascota_id: post_.mascota_id
        };

        // Upload de las imagenes y set de las rutas de los archivos en cloudinary (si es que hay).
        if (post_.files_uploaded && post_.files_uploaded.length > 0) {

            let rutas_archivos: string[] = [];

            for (let i = 0; i < post_.files_uploaded.length; i++) {

                let data_uri: string = getDataURI(post_.files_uploaded[i]).content;

                let ruta_file_server: string = await subirFileCloudinaryBuffer(data_uri);
                rutas_archivos.push(ruta_file_server);

            }

            posteo.archivos_path = JSON.stringify(rutas_archivos);
        }

        await Post_mascota.create(posteo)
            .then((rdo: Post_mascota) => {

                post_ = rdo.get();

                // transformar el JSON de rutas en BD en un array de string para el front..
                if (post_.archivos_path) {
                    post_.archivos_array = JSON.parse(post_.archivos_path);   // arreglo de string con rutas URL.
                    post_.archivos_path = undefined;                          // stringify que se usa en BD.
                }

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return post_;

    },

    async deletePost(id_post: number): Promise<number> {

        // let archivos_path_borrar: string[] = [];

        // Busco post para poder borrar los files adjuntados si es que tiene.
        await Post_mascota.findByPk(id_post, {
            raw: true
        })
            .then((post: Post_mascota | null) => {

                if (!post) throw new ErrorGenerico('Post sin ID al momento de querer eliminarlo', 404);
                // if (post.archivos_path) archivos_path_borrar = JSON.parse(post.archivos_path);

            }, (e: Error) => {
                throw new ErrorGenerico(e.message + '. Error buscando las img del post para borrar.', 503);
            });

        await Post_mascota.destroy({
            where: { id_post: id_post }
        })
            .then(() => {

                /* 
                Usado para borrar los archivos almacenados en el servidor. No útil ahora.

                if (archivos_path_borrar.length > 0) {
                    for (const file_path of archivos_path_borrar) {
                        borrar_file_servidor(file_path);
                    }
                }
                */

            }, (e: Error) => {
                throw new ErrorGenerico(e.message + '. Error borrando el post.', 503);
            });

        return id_post;
    },

    async getCantidadPostDeMascota(id_mascota: number): Promise<number> {

        let cantidad_posteos: number = 0;

        cantidad_posteos = await Post_mascota.count({
            where: {
                mascota_id: id_mascota
            }
        });

        return cantidad_posteos;
    },

    async getPosteos(id_mascota: number, fecha_desde: Date): Promise<Post_[]> {

        let posteos: Post_[] = [];

        await Post_mascota.findAll({
            raw: true,
            order: [
                ['fecha_publicacion', 'DESC']
            ],
            where: {
                mascota_id: id_mascota,
                fecha_publicacion: {
                    [Op.lt]: fecha_desde
                }
            },
            limit: 20
        })
            .then((posts: Post_mascota[]) => {

                for (let i = 0; i < posts.length; i++) {

                    let post: Post_ = posts[i];

                    // transformar el JSON de rutas en BD en un array.
                    if (post.archivos_path) {
                        post.archivos_array = JSON.parse(post.archivos_path);
                        post.archivos_path = undefined;
                    }

                    posteos.push(post);

                }

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return posteos;

    },

    async traerComunicacionDeMascotaByID(id_com: number): Promise<Comunicacion_> {

        let comunicacion_: Comunicacion_ = {};

        await Comunicacion.findOne({
            where: {
                id_comunicacion: id_com
            },
            include: [{
                association: mascd_bt_cab,
                as: 'mascota_destino'
            }, {
                association: masco_bt_cab,
                as: 'mascota_origen'
            }]
        })
            .then((comunicaciones: any) => {

                let comunicacion: Comunicacion_ = {};

                comunicacion.id_comunicacion = comunicaciones.get().id_comunicacion;
                comunicacion.mascota_origen = comunicaciones.get().mascota_origen.get();
                comunicacion.mascota_destino = comunicaciones.get().mascota_destino.get();

                comunicacion_ = comunicacion;

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return comunicacion_;

    },

    async traerComunicacionesDeMascota(id_mascota: number): Promise<Comunicacion_[]> {

        let comunicaciones_: Comunicacion_[] = [];
        let id_comunicaciones: number[] = [];   // para traer último mensaje (problemas con tipado: null | number).

        await Comunicacion.findAll({
            where: {
                [Op.or]: [{
                    mascota_origen_id: id_mascota
                }, {
                    mascota_destino_id: id_mascota
                }]
            },
            include: [{
                association: mascd_bt_cab,
                as: 'mascota_destino',
                include: filtro_include_mascota
            },
            {
                association: masco_bt_cab,
                as: 'mascota_origen',
                include: filtro_include_mascota
            }]
        })
            .then((comunicaciones: any[]) => {

                for (let i = 0; i < comunicaciones.length; i++) {

                    let comunicacion: Comunicacion_ = {};

                    comunicacion.id_comunicacion = comunicaciones[i].get().id_comunicacion;
                    comunicacion.mascota_origen = this.llenarMascota(comunicaciones[i].get().mascota_origen);
                    comunicacion.mascota_destino = this.llenarMascota(comunicaciones[i].get().mascota_destino);

                    comunicaciones_.push(comunicacion);

                    id_comunicaciones.push(comunicaciones[i].get().id_comunicacion)
                }

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        for (let i = 0; i < comunicaciones_.length; i++) {

            await Mensaje.findAll({
                where: {
                    cabecera_sms_id: id_comunicaciones[i]
                },
                limit: 5,
                order: [['fecha', 'DESC']]
            })
                .then((mensajes: Mensaje[]) => {

                    let messages: Mensaje_[] = [];

                    mensajes.forEach(sms => {
                        messages.push(sms.get());
                    });

                    comunicaciones_[i].mensajes = messages;

                }, (e: Error) => {
                    throw new ErrorGenerico(e.message, 503);
                });

        }

        return comunicaciones_.reverse();

    },

    async crearMensaje(sms: Mensaje_): Promise<Mensaje_> {

        // Traer desde el front el visto_destino: false.
        let sms_: Mensaje_ = {};

        await Mensaje.create(sms)
            .then((mensaje: Mensaje) => {

                sms_ = mensaje.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        if (!sms_.mascota_origen_id || !sms_.mascota_destino_id) throw new ErrorGenerico('Mensaje con problemas de origen y destino. CrearMensaje()', 404);

        sms_.mascota_destino = await this.getMascotaByID(sms_.mascota_destino_id);
        sms_.mascota_origen = await this.getMascotaByID(sms_.mascota_origen_id);

        return sms_;

    },

    async getMensajesComunicacion(id_comunicacion: number, fecha_desde: Date): Promise<Mensaje_[]> {

        let mensajes_: Mensaje_[] = [];
        let cant_sms_a_traer: number = 10;

        await Mensaje.findAll({
            where: {
                [Op.and]: [
                    { cabecera_sms_id: id_comunicacion },
                    {
                        fecha: {
                            [Op.lt]: fecha_desde
                        }
                    }
                ]
            },
            limit: cant_sms_a_traer,
            order: [['id_mensaje', 'DESC']]
        })
            .then((mensajes: Mensaje[]) => {

                mensajes.forEach(sms => {
                    mensajes_.push(sms.get());
                });

                mensajes_ = mensajes_.reverse();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            })

        return mensajes_;

    },

    // útil para las notificaciones.
    async getMensajesNoVistosComunicacion(id_pet: number): Promise<Comunicacion_[]> {

        let comunicaciones_: Comunicacion_[] = [];

        await Comunicacion.findAll({
            where: {
                [Op.or]: [
                    { mascota_origen_id: id_pet },
                    { mascota_destino_id: id_pet }
                ]
            },
            include: [
                {
                    model: Mensaje,
                    as: 'mensajes',
                    where: {
                        visto_destino: false,
                        mascota_destino_id: id_pet
                    },
                    include: [
                        {
                            association: mascd_bt_cab,
                            as: 'mascota_destino'
                        },
                        {
                            association: masco_bt_cab,
                            as: 'mascota_origen'
                        }
                    ]
                }

            ]
        })
            .then((comunicaciones: Comunicacion[]) => {

                comunicaciones.forEach(comu => {

                    const com: any = comu;
                    const mensajes: any[] = com.get().mensajes;

                    let messages: Mensaje_[] = [];

                    for (let i = 0; i < mensajes.length; i++) {

                        let sms: Mensaje_;

                        sms = mensajes[i].get();
                        sms.mascota_destino = mensajes[i].get().mascota_destino.get();
                        sms.mascota_origen = mensajes[i].get().mascota_origen.get();

                        messages.push(sms);

                    }

                    let comunicacion: Comunicacion_ = com.get();
                    comunicacion.mensajes = messages;

                    comunicaciones_.push(comunicacion);
                });
            })

            .catch((e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return comunicaciones_;
    },

    async setMensajeVisto(mensajes_vistos_id: number[]) {

        await Promise.all(mensajes_vistos_id.map(async (sms_id: number) => {

            let sms = { visto_destino: true };

            await Mensaje.update(sms, {
                where: {
                    id_mensaje: sms_id
                },
                fields: ['visto_destino']
            })
                .then(([cantidad, rdo]) => {

                    console.log('cantidad sms vistos: ', cantidad);

                }, (e: Error) => {
                    throw new ErrorGenerico(e.message, 503);
                })
        }))
            .then(() => {
                console.log('Todos los mensajes vistos, fin de promise all.');
            })
            .catch((e: Error) => {
                throw new ErrorGenerico(e.message, 503)
            });
    },

    async setPreferenciaBusquedaMascota(id_mascota_buscadora: number, id_mascota_buscada: number) {

        await Busqueda_reciente.findOrCreate({
            where: {
                mascota_buscadora_id: id_mascota_buscadora,
                mascota_buscada_id: id_mascota_buscada
            },
            defaults: {
                mascota_buscadora_id: id_mascota_buscadora,
                mascota_buscada_id: id_mascota_buscada,
                fecha_busqueda: new Date()
            }
        })
            .then(([busqueda_reciente, nuevo]) => {

                console.log("nuevo: ", nuevo);
                console.log(busqueda_reciente.get());

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

    },

    async getBusquedasRecientes(id_mascota: number): Promise<Mascota_[]> {

        let mascotas_buscadas_: Mascota_[] = [];

        await Busqueda_reciente.findAll({
            where: {
                mascota_buscadora_id: id_mascota
            },
            order: [
                ['fecha_busqueda', 'DESC']
            ],
            limit: 10
        })
            .then((rdo_busquedas: any) => {

                rdo_busquedas.get().busquedas.forEach((busqueda: any) => {
                    mascotas_buscadas_.push(busqueda.get());
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return mascotas_buscadas_
    },

    llenarMascota(mascota: any): Mascota_ {

        // Podría dejarlo --> usuario: mascota.get().usuario.get(); pero prefiero con todo el detalle.

        let mascota_: Mascota_ = {
            fecha_nac: mascota.get().fecha_nac,
            id_mascota: mascota.get().id_mascota,
            biografia: mascota.get().biografia,
            usuario: {
                nombre: mascota.get().usuario.get().nombre,
                email: mascota.get().usuario.get().email,
                id: mascota.get().usuario.get().id,
                password: mascota.get().usuario.get().password
            },
            raza: {
                nombre_raza: mascota.get().raza.get().nombre_raza,
                id_raza: mascota.get().raza.get().id_raza
            },
            tipo: {
                nombre_tipo_mascota: mascota.get().tipo.get().nombre_tipo_mascota,
                id_tipo_mascota: mascota.get().tipo.get().id_tipo_mascota
            },
            temperamento: {
                nombre_temperamento: mascota.get().temperamento.get().nombre_temperamento,
                id_temperamento: mascota.get().temperamento.get().id_temperamento
            },
            tamanio: {
                nombre_tamanio: mascota.get().tamanio.get().nombre_tamanio,
                id_tamanio: mascota.get().tamanio.get().id_tamanio
            },
            sexo: {
                nombre_sexo: mascota.get().sexo.get().nombre_sexo,
                id_sexo: mascota.get().sexo.get().id_sexo
            },
            domicilio: {
                id_domicilio: mascota.get().domicilio.get().id_domicilio,
                calle: mascota.get().domicilio.get().calle,
                numero: mascota.get().domicilio.get().numero,
                piso: mascota.get().domicilio.get().piso,
                depto: mascota.get().domicilio.get().depto,
                latitud_map: mascota.get().domicilio.get().latitud_map,
                longitud_map: mascota.get().domicilio.get().longitud_map,
                localidad_id: mascota.get().domicilio.get().localidad_id
            },
            localidad: {
                nombre_localidad: mascota.get().domicilio.get().localidad.get().nombre_localidad,
                id_localidad: mascota.get().domicilio.get().localidad.get().id_localidad,
                provincia_id: mascota.get().domicilio.get().localidad.get().provincia_id
            },
            provincia: {
                id_provincia: mascota.get().domicilio.get().localidad.get().provincia.get().id_provincia,
                nombre_provincia: mascota.get().domicilio.get().localidad.get().provincia.get().nombre_provincia,
                pais_id: mascota.get().domicilio.get().localidad.get().provincia.get().pais_id,
            },
            pais: {
                id_pais: mascota.get().domicilio.get().localidad.get().provincia.get().pais.get().id_pais,
                nombre_pais: mascota.get().domicilio.get().localidad.get().provincia.get().pais.get().nombre_pais
            },
            profile_pic: mascota.get().profile_pic,
            nombre_mascota: mascota.get().nombre_mascota
        };

        return mascota_;
    },

    llenarMascotaByUbication(mascota: any, mascota_dom: any, mascota_loc: any, mascota_prov: any, mascota_pais: any): Mascota_ {

        let mascota_: Mascota_ = {
            fecha_nac: mascota.get().fecha_nac,
            id_mascota: mascota.get().id_mascota,
            biografia: mascota.get().biografia,
            usuario: {
                nombre: mascota.get().usuario.get().nombre,
                email: mascota.get().usuario.get().email,
                id: mascota.get().usuario.get().id,
                password: mascota.get().usuario.get().password
            },
            raza: {
                nombre_raza: mascota.get().raza.get().nombre_raza,
                id_raza: mascota.get().raza.get().id_raza
            },
            tipo: {
                nombre_tipo_mascota: mascota.get().tipo.get().nombre_tipo_mascota,
                id_tipo_mascota: mascota.get().tipo.get().id_tipo_mascota
            },
            temperamento: {
                nombre_temperamento: mascota.get().temperamento.get().nombre_temperamento,
                id_temperamento: mascota.get().temperamento.get().id_temperamento
            },
            tamanio: {
                nombre_tamanio: mascota.get().tamanio.get().nombre_tamanio,
                id_tamanio: mascota.get().tamanio.get().id_tamanio
            },
            sexo: {
                nombre_sexo: mascota.get().sexo.get().nombre_sexo,
                id_sexo: mascota.get().sexo.get().id_sexo
            },
            domicilio: {
                id_domicilio: mascota_dom.get().id_domicilio,
                calle: mascota_dom.get().calle,
                numero: mascota_dom.get().numero,
                piso: mascota_dom.get().piso,
                depto: mascota_dom.get().depto,
                latitud_map: mascota_dom.get().latitud_map,
                longitud_map: mascota_dom.get().longitud_map,
                localidad_id: mascota_dom.get().localidad_id
            },
            localidad: {
                nombre_localidad: mascota_loc.get().nombre_localidad,
                id_localidad: mascota_loc.get().id_localidad,
                provincia_id: mascota_loc.get().provincia_id
            },
            provincia: {
                id_provincia: mascota_prov.get().id_provincia,
                nombre_provincia: mascota_prov.get().nombre_provincia,
                pais_id: mascota_prov.get().pais_id,
            },
            pais: {
                id_pais: mascota_pais.get().id_pais,
                nombre_pais: mascota_pais.get().nombre_pais
            },
            profile_pic: mascota.get().profile_pic,
            nombre_mascota: mascota.get().nombre_mascota
        };

        return mascota_;
    },

    /*----------------------------------------------------------------------------------------------------------------------------------------------------
                                        INICIO DE LISTADO DE METODOS QUE QUEDARON SIN USO.
    ----------------------------------------------------------------------------------------------------------------------------------------------------*/

    // Con cloudinary queda obsoleto, ahora no guarda en el servidor. Util cuando guardaba en el servidor.
    editarPathImagenes(array_path: string): string {

        // cortar path desde la posición 60 de la imagen y arranque desde /uploads. El string adjuntado varía dependiendo dónde estén las imágenes (en este caso, localhost).

        if (array_path) {
            array_path = `http://${IP_SERVER_IMG}:${SERVER_PORT}` + array_path.substring(array_path.indexOf('/uploads/'));
        }

        return array_path;
    },

    // Las comunicaciones se generan una vez que hubo match entre las mascotas. Ya no es así.
    async crearComunicacion(com_sms: Comunicacion_): Promise<Comunicacion_> {

        if (!com_sms.mascota_destino_id || !com_sms.mascota_origen_id) throw new ErrorGenerico('Error al recibir solicitud de comunicación.', 404);
        if (com_sms.mascota_destino_id === com_sms.mascota_origen_id) throw new ErrorGenerico('No tiene sentido la comunicación con uno mismo :).', 404);

        let comunicacion: Comunicacion_ = {};

        // No permitir crear conexiones que ya existen, de un lado o del otro.
        await Comunicacion.findOrCreate({
            where: {
                [Op.or]: [
                    {
                        [Op.and]: [
                            { mascota_origen_id: com_sms.mascota_origen_id },
                            { mascota_destino_id: com_sms.mascota_destino_id }
                        ]
                    },
                    {
                        [Op.and]: [
                            { mascota_origen_id: com_sms.mascota_destino_id },
                            { mascota_destino_id: com_sms.mascota_origen_id }
                        ]
                    }
                ]
            },
            defaults: {
                mascota_origen_id: com_sms.mascota_origen_id,
                mascota_destino_id: com_sms.mascota_destino_id
            }
        })
            .then(([com, nuevo]) => {

                comunicacion = com.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return comunicacion;
    },

    // no se usa más.
    async editarRelacion(rel: Relacion_, datos_o: Relacion_datos_actualizados_, datos_d: Relacion_datos_actualizados_): Promise<Relacion_> {

        // Edita el like, match y visto del destino/origen. 

        // if (rel.match) return rel;  // relación ya matcheada. No se puede editar.

        let relacion_: Relacion_ = rel;
        let datos_origen: Relacion_datos_actualizados_;
        let datos_destino: Relacion_datos_actualizados_;

        if (relacion_.mascota_origen_id === datos_o.id_mascota) {
            datos_origen = datos_o;
            datos_destino = datos_d;
        } else {
            datos_origen = datos_d;
            datos_destino = datos_o;
        }

        relacion_.visto_destino = datos_destino.visto;
        relacion_.visto_origen = datos_origen.visto;
        relacion_.like_destino = datos_destino.like;
        relacion_.like_origen = datos_origen.like;

        if (relacion_.like_destino && relacion_.like_origen) relacion_.match = true;
        else relacion_.match = false;

        if (!relacion_.id_relacion) throw new ErrorGenerico('Relación inexistente en editarRelacion()', 404);

        Relacion.update(relacion_, {
            where: {
                id_relacion: relacion_.id_relacion
            },
            fields: ['visto_origen', 'visto_destino', 'like_origen', 'like_destino', 'match']
        })
            .then(async ([rowsUpdated, rel]) => {

                if (relacion_.like_destino && relacion_.like_origen) {

                    let com: Comunicacion_ = {
                        mascota_origen_id: relacion_.mascota_origen_id,
                        mascota_destino_id: relacion_.mascota_destino_id
                    };

                    let comunicacion: Comunicacion_ = await this.crearComunicacion(com);

                    console.log('Match, comunicación creada => ', comunicacion);

                }

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return relacion_;
    },

    // no se usa.
    async crearResumenFotosMascota(id_mascota: number, fotos: any): Promise<Mascota_resumen_> {

        let fotos_mascotas: string[] = [];
        let resumen: Mascota_resumen_ = {};

        for (let i = 0; i < fotos.length; i++) {
            if (fotos[i].path) fotos_mascotas.push(fotos[i].path);
        }

        await Mascota_resumen.findOrCreate({
            where: {
                mascota_id: id_mascota
            },
            defaults: {
                foto_mascota1: fotos_mascotas[0],
                foto_mascota2: fotos_mascotas[1],
                foto_mascota3: fotos_mascotas[2]
            }
        })
            .then(([mascotaresumenDB, nuevo]) => {

                resumen = mascotaresumenDB.get();
                console.log(nuevo);

            }, ((error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            }));

        return resumen;

    },

    // no se usa.
    async getResumenFotosMascota(id_mascota: number): Promise<Mascota_resumen_> {

        let resumen: Mascota_resumen_ = {};

        await Mascota_resumen.findOne({
            where: {
                mascota_id: id_mascota
            }
        })
            .then((resumen: Mascota_resumen | null) => {

                if (resumen == null) throw new ErrorGenerico('No hay un resumen para esta mascota', 404);

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return resumen;

    },

    // no se usa.
    async editarResumenFotosMascota(resumen: Mascota_resumen_): Promise<Mascota_resumen_> {

        if (resumen.id_mascota_resumen == null) throw new ErrorGenerico('Resumen inválido. ID nulo, incoherente.', 404);
        if (!resumen.mascota_fotos) throw new ErrorGenerico('Error: editarResumenFotosMascota.', 404);

        let fotos_path: string[] = resumen.mascota_fotos;

        let resumen_: any = {
            mascota_id: resumen.mascota_id,
            foto_mascota1: '',
            foto_mascota2: '',
            foto_mascota3: ''
        };

        if (fotos_path.length > 0) {

            for (let i = 0; i < fotos_path.length; i++) {
                if (resumen_.foto_mascota1 === '') {
                    resumen_.foto_mascota1 = fotos_path[i];
                } else if (resumen_.foto_mascota2 === '') {
                    resumen_.foto_mascota2 = fotos_path[i];
                } else if (resumen_.foto_mascota3 === '') {
                    resumen_.foto_mascota3 = fotos_path[i];
                }
            }

        } else {
            throw new ErrorGenerico('Se necesita al menos una foto cargada.', 404);
        }

        await Mascota_resumen.update(resumen_, {
            where: { id_mascota_resumen: resumen.id_mascota_resumen }
        })
            .then(([cantidadRowsUpdate, mascotaresumenDB]) => {

                console.log('cantidad: ', cantidadRowsUpdate);
                console.log('mascota: ', mascotaresumenDB);

            }, ((error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            }));

        return resumen_;

    },

    // No se usa más.
    async crearRelacion(origen: Relacion_datos_actualizados_, destino: Relacion_datos_actualizados_): Promise<Relacion_> {

        let relacion: Relacion_ = {};

        await Relacion.findOrCreate({
            where: {
                [Op.or]: [
                    {
                        mascota_origen_id: origen.id_mascota,
                        mascota_destino_id: destino.id_mascota
                    },
                    {
                        mascota_origen_id: destino.id_mascota,
                        mascota_destino_id: origen.id_mascota
                    }
                ]
            },
            defaults: {
                fecha: new Date(),
                match: false,
                like_origen: origen.like,
                like_destino: destino.like,
                visto_origen: origen.visto,
                visto_destino: origen.visto,
                mascota_origen_id: origen.id_mascota,
                mascota_destino_id: destino.id_mascota
            }
        })
            .then(async ([rel, nuevo]) => {

                if (!nuevo) {

                    let r: Relacion_ = rel.get();

                    relacion = await this.editarRelacion(r, origen, destino);

                } else {
                    relacion = rel.get();
                }

            });

        return relacion;
    },

    // trae las relaciones que disparó la mascota, no en las que fue destino.
    async getRelacionesMascota(id_mascota: number): Promise<Relacion_[]> {

        let relaciones_: Relacion_[] = [];

        await Relacion.findAll({
            where: {
                mascota_origen_id: id_mascota
            }
        })
            .then((relaciones: Relacion[]) => {

                relaciones.forEach(relacion => {
                    relaciones_.push(relacion.get());
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return relaciones_;

    },

    /*----------------------------------------------------------------------------------------------------------------------------------------------------
                                        FIN DE LISTADO DE METODOS QUE QUEDARON SIN USO.
    ----------------------------------------------------------------------------------------------------------------------------------------------------*/
}

export default mascotaController;