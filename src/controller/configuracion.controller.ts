import { Provincia_, Pais_, Localidad_, Raza_, Tamanio_, Temperamento_, Sexo_, Domicilio_, CoordenadasDom_, Comunicacion_, Tipo_ } from '../modelos/modelos';
import { Pais } from '../database/modelos/ubicación/pais';
import ErrorGenerico from '../config/customError';
import { Provincia, prov_bt_pais } from '../database/modelos/ubicación/provincia';
import { Localidad, loc_bt_prov } from '../database/modelos/ubicación/localidad';
import { Raza } from '../database/modelos/mascota/raza';
import Tamanio from '../database/modelos/mascota/tamanio';
import Temperamento from '../database/modelos/mascota/temperamento';
import Sexo from '../database/modelos/mascota/sexo';
import { Domicilio } from '../database/modelos/ubicación/domicilio';
import { Comunicacion, mascd_bt_cab, masco_bt_cab } from '../database/modelos/mascota/comunicacion';
import { Tipo_mascota } from '../database/modelos/mascota/tipo_mascota';
import { key_apigeocoding } from '../global/ambiente';
import external_request from '../config/request_API';

const configController = {

    async getPais(): Promise<Pais_[]> {

        let paises_: Pais_[] = [];

        await Pais.findAll({
            raw: true
        })
            .then((paises: Pais[]) => {

                paises.forEach(pais => {
                    paises_.push(pais);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return paises_;

    },

    async getProvinciaByIDPais(id_pais: number): Promise<Provincia_[]> {

        let provincias_: Provincia_[] = [];

        await Provincia.findAll({
            raw: true,
            where: {
                pais_id: id_pais
            }
        })
            .then((provincias: Provincia[]) => {

                provincias.forEach(provincia => {
                    provincias_.push(provincia)
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return provincias_;

    },

    async getLocalidadByIDProv(id_prov: number): Promise<Localidad_[]> {

        let localidades_: Localidad_[] = [];

        await Localidad.findAll({
            raw: true,
            where: {
                provincia_id: id_prov
            }
        })
            .then((localidades: Localidad[]) => {

                localidades.forEach(localidad => {
                    localidades_.push(localidad);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return localidades_;

    },

    async getRaza(): Promise<Raza_[]> {

        let razas_: Raza_[] = [];

        await Raza.findAll({
            raw: true
        })
            .then((razas: Raza[]) => {

                razas.forEach(raza => {
                    razas_.push(raza);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return razas_;

    },

    async getRazaByID(id: number): Promise<Raza_> {

        let raza_: Raza_ = {};

        await Raza.findOne({
            raw: true,
            where: {
                id_raza: id
            }

        })
            .then((raza: Raza | null) => {

                if (!raza) throw new ErrorGenerico("Raza inexistente.", 404);

                raza_ = raza.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return raza_;

    },

    async getTamanio(): Promise<Tamanio_[]> {

        let tamanios_: Tamanio_[] = [];

        await Tamanio.findAll({
            raw: true
        })
            .then((tamanios: Tamanio[]) => {

                tamanios.forEach(tamanio => {
                    tamanios_.push(tamanio);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return tamanios_;

    },

    async getTamanioByID(id: number): Promise<Tamanio_> {

        let tamanio_: Tamanio_ = {};

        await Tamanio.findOne({
            raw: true,
            where: {
                id_tamanio: id
            }

        })
            .then((tamanio: Tamanio | null) => {

                if (!tamanio) throw new ErrorGenerico("Tamanio inexistente.", 404);

                tamanio_ = tamanio.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return tamanio_;

    },

    async getTemperamento(): Promise<Temperamento_[]> {

        let temperamentos_: Temperamento_[] = [];

        await Temperamento.findAll({
            raw: true
        })
            .then((temperamentos: Temperamento[]) => {

                temperamentos.forEach(temperamento => {
                    temperamentos_.push(temperamento);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return temperamentos_;

    },

    async getTemperamentoByID(id: number): Promise<Temperamento_> {

        let temperamento_: Temperamento_ = {};

        await Temperamento.findByPk(id)
            .then((temperamento: Temperamento | null) => {

                if (!temperamento) throw new ErrorGenerico("Temperamento inexistente.", 404);

                temperamento_ = temperamento.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return temperamento_;

    },

    async getTipoMascota(): Promise<Tipo_[]> {

        let tipos_mascotas_: Tipo_[] = [];

        await Tipo_mascota.findAll({
            raw: true
        })
            .then((tipos: Tipo_mascota[]) => {

                tipos.forEach(tipo_mascota => {
                    tipos_mascotas_.push(tipo_mascota);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return tipos_mascotas_;

    },

    async getSexo(): Promise<Sexo_[]> {

        let sexos_: Sexo_[] = [];

        await Sexo.findAll({
            raw: true
        })
            .then((sexos: Sexo[]) => {

                sexos.forEach(sexo => {
                    sexos_.push(sexo);
                });

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return sexos_;

    },

    async getSexoByID(id: number): Promise<Sexo_> {

        let sexo_: Sexo_ = {};

        await Sexo.findByPk(id)
            .then((sexo: Sexo | null) => {

                if (!sexo) throw new ErrorGenerico("Sexo inexistente.", 404);

                sexo_ = sexo.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return sexo_;

    },

    async crearDomicilio(domicilio: Domicilio_): Promise<Domicilio_> {

        if (!domicilio.calle || !domicilio.localidad_id || !domicilio.numero) throw new ErrorGenerico('Cuerpo de domicilio incorrecto, valores vacío no válidos.', 404);

        let dom: Domicilio_ = {};
        let coordenadas: CoordenadasDom_ = { latitud: -200, longitud: -200 };
        let loc: string = '', prov: string = '', pais: string = '';

        await Localidad.findOne({
            where: {
                id_localidad: domicilio.localidad_id
            },
            include: [{
                association: loc_bt_prov,
                as: 'provincia',
                include: [{
                    association: prov_bt_pais,
                    as: 'pais'
                }]
            }]
        })
            .then((localidad: any) => {

                if (!localidad) throw new ErrorGenerico('No existe localidad asociada a ese domicilio', 404);

                loc = localidad.get().nombre_localidad;
                prov = localidad.get().provincia.get().nombre_provincia;
                pais = localidad.get().provincia.get().pais.get().nombre_pais;

            }, (error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            });

        coordenadas = await this.getCoordinatesByAddress(domicilio.calle + ' ' + domicilio.numero + ', ' + loc + ', ' + prov + ', ' + pais);

        // filtro para crear bien los domicilios respetando los pisos y departamentos.
        let clausulaSQL: any = {};

        if (domicilio.calle) clausulaSQL.calle = domicilio.calle;
        if (domicilio.numero) clausulaSQL.numero = domicilio.numero;
        if (domicilio.localidad_id) clausulaSQL.localidad_id = domicilio.localidad_id;
        if (domicilio.piso) clausulaSQL.piso = domicilio.piso;
        if (domicilio.depto) clausulaSQL.depto = domicilio.depto;
        clausulaSQL.latitud_map = coordenadas.latitud;
        clausulaSQL.longitud_map = coordenadas.longitud;

        await Domicilio.findOrCreate({
            where: clausulaSQL,
            defaults: clausulaSQL
        })
            .then(([domicilioBD, nuevo]) => {

                dom = domicilioBD.get();
                console.log('domicilio nuevo?: ', nuevo);

            }, ((error: Error) => {
                throw new ErrorGenerico(error.message, 503);
            }));

        return dom;
    },

    async editarDomicilioMascota(domicilio: Domicilio_): Promise<Domicilio_> {

        if (!domicilio.id_domicilio) throw new ErrorGenerico('Domicilio inexistente.', 404);

        let atributos_a_editar: string[] = [];
        let coordenadas: CoordenadasDom_;
        let loc: string = '';
        let prov: string = '';
        let pais: string = '';

        await Localidad.findByPk(domicilio.localidad_id, {
            include: [{
                association: loc_bt_prov,
                as: 'provincia',
                include: [{
                    association: prov_bt_pais,
                    as: 'pais'
                }]
            }]
        })
            .then((localidad: any) => {

                if (!localidad) throw new ErrorGenerico('No hay localidad con ese ID, editarDomicilioMascota()', 404);

                loc = localidad.get().nombre_localidad;
                prov = localidad.get().provincia.get().nombre_provincia;
                pais = localidad.get().provincia.get().pais.get().nombre_pais;

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        // debería estar en configuración controller.
        coordenadas = await configController.getCoordinatesByAddress(domicilio.calle + ' ' + domicilio.numero + ', ' + loc + ', ' + prov + ', ' + pais)

        let dom: any;

        if (domicilio.calle) {
            dom.calle = domicilio.calle;
            atributos_a_editar.push('calle');
        }

        if (domicilio.numero) {
            dom.numero = domicilio.numero;
            atributos_a_editar.push('numero');
        }
        
        if (domicilio.piso) {
            dom.piso = domicilio.piso;
            atributos_a_editar.push('piso');
        }
        
        if (domicilio.depto) {
            dom.depto = domicilio.depto;
            atributos_a_editar.push('depto');
        }
        
        if (domicilio.localidad_id) {
            dom.localidad_id = domicilio.localidad_id;
            atributos_a_editar.push('localidad_id');
        }

        dom.latitud_map = coordenadas.latitud;
        dom.longitud_map = coordenadas.longitud;
        atributos_a_editar.push('latitud_map');
        atributos_a_editar.push('longitud_map');

        await Domicilio.update(dom, {
            where: {
                domicilio_id: domicilio.id_domicilio
            },
            fields: atributos_a_editar
        })
            .then(([cantidad, rdo]) => {

                console.log('domicilio actualizado.', cantidad, rdo);

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return domicilio;

    },

    async getComunicacionByID(id_mascota: number): Promise<Comunicacion_> {

        let com: Comunicacion_ = {};

        await Comunicacion.findByPk(id_mascota, {
            include: [{
                association: mascd_bt_cab,
                as: 'mascota_destino'
            }, {
                association: masco_bt_cab,
                as: 'mascota_origen'
            }]
        })
            .then((comunicacion: any) => {

                if (!comunicacion) throw new ErrorGenerico('Comunicación inexistente.', 404);

                comunicacion.mascota_origen = comunicacion.get().mascota_origen.get();
                comunicacion.mascota_destino = comunicacion.get().mascota_destino.get();
                com = comunicacion.get();

            }, (e: Error) => {
                throw new ErrorGenerico(e.message, 503);
            });

        return com;

    },

    async getCoordinatesByAddress(direccion: string): Promise<CoordenadasDom_> {

        let coordenadas: CoordenadasDom_ = { latitud: 200, longitud: 200 };

        const api_geocoding: string = 'http://www.mapquestapi.com/geocoding/v1/address?key=' + key_apigeocoding + '&location=' + direccion;
        const rdo = await external_request.getMapquest(api_geocoding);

        coordenadas.latitud = rdo.latLng.lat;
        coordenadas.longitud = rdo.latLng.lng;

        return coordenadas;
    },

};

export default configController;