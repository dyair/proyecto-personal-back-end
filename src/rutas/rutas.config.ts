import { Router, Request, Response } from 'express';
import ErrorGenerico from '../config/customError'; 
import { ok_status } from '../global/ambiente';
import configController from '../controller/configuracion.controller';
import { Pais_, Provincia_, Localidad_, Raza_, Tamanio_, Temperamento_, Sexo_, Domicilio_, Comunicacion_, Tipo_ } from '../modelos/modelos';

const router_config = Router();

router_config.get('/pais', (req: Request, resp: Response) => {

    let token: string | undefined = 'asd'; // req.headers.authorization;

    if (token) {

        configController.getPais()
            .then((contenido: Pais_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/provincia/:id_pais', (req: Request, resp: Response) => {

    let token: string | undefined = 'AS'; // req.headers.authorization;
    let id_pais: number = Number(req.params.id_pais);

    if (token) {

        configController.getProvinciaByIDPais(id_pais)
            .then((contenido: Provincia_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }

});

router_config.get('/localidad/:id_prov', (req: Request, resp: Response) => {

    let token: string | undefined ='asd'; //req.headers.authorization;
    let id_prov: number = Number(req.params.id_prov);

    if (token) {

        configController.getLocalidadByIDProv(id_prov)
            .then((contenido: Localidad_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }

});

router_config.get('/raza', (req: Request, resp: Response) => {

    let token: string | undefined ='asd'; //req.headers.authorization;

    if (token) {

        configController.getRaza()
            .then((contenido: Raza_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/raza/:id', (req: Request, resp: Response) => {

    let token: string | undefined = req.headers.authorization;
    let id: number = Number(req.params.id);

    if (token) {
        configController.getRazaByID(id)
            .then((contenido: Raza_) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/tipo_mascota', (req: Request, resp: Response) => {

    let token: string | undefined ='asd'; //req.headers.authorization;

    if (token) {

        configController.getTipoMascota()
            .then((contenido: Tipo_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/tamanio', (req: Request, resp: Response) => {

    let token: string | undefined ='asd'; //req.headers.authorization;

    if (token) {
        configController.getTamanio()
            .then((contenido: Tamanio_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/tamanio/:id', (req: Request, resp: Response) => {

    let token: string | undefined = req.headers.authorization;
    let id: number = Number(req.params.id);

    if (token) {
        configController.getTamanioByID(id)
            .then((contenido: Tamanio_) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/temperamento', (req: Request, resp: Response) => {

    let token: string | undefined = 'asd'; //req.headers.authorization;

    if (token) {
        configController.getTemperamento()
            .then((contenido: Temperamento_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/temperamento/:id', (req: Request, resp: Response) => {

    let token: string | undefined = 'asd'; //req.headers.authorization;
    let id: number = Number(req.params.id);

    if (token) {
        configController.getTemperamentoByID(id)
            .then((contenido: Temperamento_) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/sexo', (req: Request, resp: Response) => {

    let token: string | undefined = 'asd'; //req.headers.authorization;

    if (token) {
        configController.getSexo()
            .then((contenido: Sexo_[]) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/sexo/:id', (req: Request, resp: Response) => {

    let token: string | undefined = req.headers.authorization;
    let id: number = Number(req.params.id);

    if (token) {
        configController.getSexoByID(id)
            .then((contenido: Sexo_) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.get('/comunicacion/:id', (req: Request, resp: Response) => {

    let token: string | undefined =  'das' //req.headers.authorization;
    let id: number = Number(req.params.id);

    if (token) {
        configController.getComunicacionByID(id)
            .then((contenido: Comunicacion_) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });
    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});

router_config.post('/domicilio', (req: Request, resp: Response) => {

    let token: string | undefined = 'asd'; // req.headers.authorization;
    let domicilio: Domicilio_ = req.body.domicilio;

    
    if (token) {
        configController.crearDomicilio(domicilio)
            .then((contenido: Domicilio_) => {
                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((e: ErrorGenerico) => {
                resp.status(e.status_code).json({
                    ok: false,
                    mensaje: e.message,
                    code: e.status_code
                });
            });

    } else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token de autenticación nulo.'
        });
    }
});

export default router_config;