import { Router, Request, Response } from 'express';
import usuarioController from '../controller/usuario.controller';
import { User_, User_login } from '../modelos/modelos';
import ErrorGenerico from '../config/customError';
import { ok_status } from '../global/ambiente';

const router = Router();

router.post('/login_google', async (req: Request, resp: Response) => {

    let token_google: string = req.body.token;

    if (token_google) {

        let usuario_google: User_ = await usuarioController.verify(token_google);

        usuarioController.iniciarSesionByGoogle(usuario_google)
            .then((contenido: User_) => {

                resp.status(ok_status).json({
                    ok: true,
                    contenido
                });
            })
            .catch((error: ErrorGenerico) => {
                console.error(error);
                resp.status(error.status_code).json({
                    ok: false,
                    mensaje: error.message,
                    code: error.status_code
                });
            });

    }

});

router.post('/login', (req: Request, resp: Response) => {

    let usuario: User_login = {
        email: req.body.email,
        password: req.body.password
    }

    usuarioController.iniciarSesion(usuario)
        .then((usuario: User_) => {
            resp.status(ok_status).json({
                contenido: usuario
            });
        })
        .catch((error: ErrorGenerico) => {
            console.error(error);
            resp.status(error.status_code).json({
                ok: false,
                mensaje: error.message,
                code: error.status_code
            });
        });
});

router.post('/registrar', (req: Request, resp: Response) => {

    let usuario: User_ = req.body.usuario;

    if (usuario.password && usuario.password.length < 5) {
        resp.status(404).json({
            ok: false,
            mensaje: 'Mínimo 6 caracteres para la contraseña del usuario.',
            code: 404
        });
    }

    usuarioController.crearUsuario(usuario)
        .then((contenido: User_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((error: ErrorGenerico) => {
            resp.status(error.status_code).json({
                ok: false,
                mensaje: error.message,
                code: error.status_code
            });
        });
});

export default router;  