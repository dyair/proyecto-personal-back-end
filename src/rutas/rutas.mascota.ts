import { Router, Request, Response } from 'express';
import { Mascota_, Domicilio_, Mascota_resumen_, Post_, Mensaje_, Comunicacion_, Relacion_, Relacion_datos_actualizados_ } from '../modelos/modelos';
import ErrorGenerico from '../config/customError';
import mascotaController from '../controller/mascota.controller';
import { upload } from '../config/middleware/file_management/uploads';
import { ok_status } from '../global/ambiente';
import configController from '../controller/configuracion.controller';
import { autorizacion_token } from '../config/middleware/jwtauth';
import { File_multer } from '../modelos/modelo_tecnico';

const router_mascota = Router();

router_mascota.get('/:id_mascota', autorizacion_token, (req: Request, resp: Response) => {

    mascotaController.getMascotaByID(req.params.id_mascota)
        .then((contenido: Mascota_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

// es un put pero el put no permite el formdata por eso queda post. 
router_mascota.post('/:id_mascota', autorizacion_token, upload.single('imagen'), (req: Request, resp: Response) => {

    let mascota_: any = { id_mascota: Number(req.params.id_mascota) }

    if (req.body.sexo_id) mascota_.sexo_id = Number(req.body.sexo_id);
    if (req.body.temperamento_id) mascota_.temperamento_id = Number(req.body.temperamento_id);
    if (req.body.tamanio_id) mascota_.tamanio_id = Number(req.body.tamanio_id);
    if (req.body.usuario_id) mascota_.usuario_id = Number(req.body.usuario_id);
    if (req.body.fecha_nac) mascota_.fecha_nac = new Date(req.body.fecha_nac);
    if (req.body.nombre_mascota) mascota_.nombre_mascota = req.body.nombre_mascota;
    if (req.body.biografia) mascota_.biografia = req.body.biografia;
    if (req.body.raza_id) mascota_.raza_id = Number(req.body.raza_id);
    if (req.file) mascota_.file_img = req.file;
    if (req.body.domicilio_id) mascota_.domicilio_id = Number(req.body.domicilio_id);

    mascotaController.editarMascota(mascota_)
        .then((contenido: Mascota_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.put('/:id_domicilio', autorizacion_token, (req: Request, resp: Response) => {

    let domicilio: Domicilio_ = req.body.domicilio;

    configController.editarDomicilioMascota(domicilio)
        .then((contenido: Domicilio_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.post('/:id_mascota_origen/relacion', autorizacion_token, (req: Request, resp: Response) => {

    const origen: Relacion_datos_actualizados_ = req.body.relacion.origen;
    const destino: Relacion_datos_actualizados_ = req.body.relacion.destino;

    mascotaController.crearRelacion(origen, destino)
        .then((contenido: Relacion_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
});

router_mascota.get('/:id_mascota/search', autorizacion_token, (req: Request, resp: Response) => {

    // Buscar mascotas por filtros.

    let id_mascota: number = +req.params.id_mascota;

    console.log('QUERY DEL SEARCH DE MASCOTA:');
    console.log({ query: req.query });

    mascotaController.getMascotasByFiltros(req.query, id_mascota)
        .then((contenido: Mascota_[]) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });


});

router_mascota.post('/:id_mascota/fotos_resumen', autorizacion_token, upload.array('imagenes'), (req: Request, resp: Response) => {

    const rdo: any = req.files;         // tipo: Express.Multer.File[]

    mascotaController.crearResumenFotosMascota(Number(req.params.id_mascota), rdo)
        .then((contenido: Mascota_resumen_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.put('/:id_mascota/fotos_resumen/:id_resumen', autorizacion_token, upload.array('imagenes'), (req: Request, resp: Response) => {

    const rdo: any = req.files;         // tipo: Express.Multer.File[]
    let arrayPath: string[] = [];


    if (rdo) {
        for (let i = 0; i < rdo.length; i++) {
            if (rdo[i]) arrayPath.push(rdo[i].path);    // Permitir que suban de 1, 2 o 3 fotos sin que explote.
        }
    }

    let resumen: Mascota_resumen_ = {
        id_mascota_resumen: Number(req.params.id_resumen),
        mascota_id: Number(req.params.id_mascota),
        mascota_fotos: arrayPath
    };

    mascotaController.editarResumenFotosMascota(resumen)
        .then((contenido: Mascota_resumen_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.post('/:id_mascota/publicacion/', autorizacion_token, upload.array('imagenes', 3), (req: Request, resp: Response) => {

    let arrayPath: string[] = [];
    const rdo: any = req.files;
    let array_files: File_multer[] = [];

    if (rdo) {
        for (let i = 0; i < rdo.length; i++) {
            array_files.push(rdo[i]);    // Permitir que suban de 1, 2 o 3 fotos sin que explote.
        }
    }

    let post: Post_ = {
        mascota_id: Number(req.params.id_mascota),
        fecha_publicacion: new Date(),
        descripcion: req.body.descripcion,
        files_uploaded: array_files
    }

    mascotaController.crearPost(post)
        .then((contenido: Post_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.get('/:id_mascota/publicacion', autorizacion_token, (req: Request, resp: Response) => {

    let fecha_desde: Date = new Date(JSON.parse(req.query.fecha_desde));

    mascotaController.getPosteos(req.params.id_mascota, fecha_desde)
        .then((contenido: Post_[]) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
});

router_mascota.get('/:id_mascota/publicacion/cantidad_post', autorizacion_token, (req: Request, resp: Response) => {

    let id_mascota: number = Number(req.params.id_mascota);

    mascotaController.getCantidadPostDeMascota(id_mascota)
        .then((contenido: number) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
});

router_mascota.delete('/:id_mascota/publicacion/:id_public', autorizacion_token, (req: Request, resp: Response) => {

    let id_post: number = Number(req.params.id_public);

    mascotaController.deletePost(id_post)
        .then((id_post: number) => {
            resp.status(ok_status).json({
                ok: true,
                contenido: id_post
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
});

router_mascota.post('/:id_mascota/comunicacion', autorizacion_token, (req: Request, resp: Response) => {

    let cab: Comunicacion_ = req.body.comunicacion;

    mascotaController.crearComunicacion(cab)
        .then((contenido: Comunicacion_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });


});

router_mascota.post('/:id_mascota/comunicacion/:id_com/mensaje', autorizacion_token, (req: Request, resp: Response) => {

    let sms: Mensaje_ = req.body.mensaje;

    mascotaController.crearMensaje(sms)
        .then((contenido: Mensaje_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.get('/:id_mascota/comunicacion', autorizacion_token, (req: Request, resp: Response) => {

    mascotaController.traerComunicacionesDeMascota(Number(req.params.id_mascota))
        .then((contenido: Comunicacion_[]) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.get('/:id_mascota/comunicacion/:id_com', autorizacion_token, (req: Request, resp: Response) => {

    const id_com: number = Number(req.params.id_com)

    mascotaController.traerComunicacionDeMascotaByID(id_com)
        .then((contenido: Comunicacion_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.get('/:id_mascota/comunicacion/:id_comunicacion/mensajes/:fecha_desde', autorizacion_token, (req: Request, resp: Response) => {

    const id_com: number = Number(req.params.id_comunicacion);
    const fecha_desde: Date = new Date(req.params.fecha_desde);

    mascotaController.getMensajesComunicacion(id_com, fecha_desde)
        .then((contenido: Mensaje_[]) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });



});

//terminar
router_mascota.post('/:id_mascota/perfil/:id_mascota_vista', autorizacion_token, (req: Request, resp: Response) => {

    let id_mascota: number = Number(req.params.id_mascota);
    let id_mascota_vista: number = Number(req.params.id_mascota_vista);

    mascotaController.setPreferenciaBusquedaMascota(id_mascota, id_mascota_vista)
        .then(() => {
            resp.status(ok_status).json({
                ok: true
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_mascota.put('/:id_mascota/set_sms_visto', autorizacion_token, (req: Request, resp: Response) => {

    const mensajes_vistos: number[] = req.body.mensajes_id;

    mascotaController.setMensajeVisto(mensajes_vistos)
        .then(() => {
            resp.status(ok_status).json({
                ok: true
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
});

//Obtener los mensajes no leidos de comunicaciones
router_mascota.get('/:id_mascota/notificacion/sms_no_visto', autorizacion_token, (req: Request, resp: Response) => {

    let id_mascota: number = Number(req.params.id_mascota);

    mascotaController.getMensajesNoVistosComunicacion(id_mascota)
        .then((contenido: Comunicacion_[]) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

//Obtener las relaciones no leidas donde sea la mascota el destino de la relación.
router_mascota.get('/:id_mascota/notificacion/relacion_no_vista', autorizacion_token, (req: Request, resp: Response) => {

    let id_mascota: number = Number(req.params.id_mascota);

    mascotaController.getRelacionesNoVistasMascota(id_mascota)
        .then((contenido: Relacion_[]) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
});

export default router_mascota;