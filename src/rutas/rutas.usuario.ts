import { Router, Request, Response } from 'express';
import { Mascota_, Mascota_resumen_ } from '../modelos/modelos';
import ErrorGenerico from '../config/customError';
import mascotaController from '../controller/mascota.controller';
import { upload } from '../config/middleware/file_management/uploads';
import { ok_status } from '../global/ambiente';
import usuarioController from '../controller/usuario.controller';
import { autorizacion_token } from '../config/middleware/jwtauth';

const router_usuario = Router();

router_usuario.post('/:usuario_id/mascota', autorizacion_token, upload.single('imagen'), (req: Request, resp: Response) => {

    let mascota_: Mascota_ = {}

    if (req.body.sexo_id) mascota_.sexo_id = Number(req.body.sexo_id);
    if (req.body.tipo_mascota_id) mascota_.tipo_mascota_id = Number(req.body.tipo_mascota_id);
    if (req.body.temperamento_id) mascota_.temperamento_id = Number(req.body.temperamento_id);
    if (req.body.tamanio_id) mascota_.tamanio_id = Number(req.body.tamanio_id);
    if (req.params.usuario_id) mascota_.usuario_id = Number(req.params.usuario_id);
    if (req.body.fecha_nac) mascota_.fecha_nac = new Date(req.body.fecha_nac);
    if (req.body.nombre_mascota) mascota_.nombre_mascota = req.body.nombre_mascota;
    if (req.body.biografia) mascota_.biografia = req.body.biografia;
    if (req.body.tipo_id) mascota_.tipo_mascota_id = Number(req.body.tipo_id);
    if (req.body.raza_id) mascota_.raza_id = Number(req.body.raza_id);
    if (req.file) mascota_.file_img = req.file;
    if (req.body.domicilio_id) mascota_.domicilio_id = Number(req.body.domicilio_id);

    let id_usuario: number = Number(req.params.usuario_id);

    mascotaController.crearMascota(mascota_, id_usuario)
        .then((contenido: Mascota_) => {
            resp.status(ok_status).json({
                ok: true,
                contenido
            });

        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_usuario.post('/:id/mascota/:id_mascota/fotos', autorizacion_token, upload.array('imagenes', 3), (req: Request, resp: Response) => {

    let id: number = Number(req.params.id_mascota);

    mascotaController.crearResumenFotosMascota(id, req.files)
        .then((resumen_mascota: Mascota_resumen_) => {
            resp.status(200).json({
                ok: true,
                resumen_mascota
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });

});

router_usuario.get('/:id_usuario/mascota', autorizacion_token, (req: Request, resp: Response) => {

    let id_user: number = Number(req.params.id_usuario);

    usuarioController.getMascotasByUser(id_user)
        .then((contenido: Mascota_[]) => {
            resp.status(200).json({
                ok: true,
                contenido
            });
        })
        .catch((e: ErrorGenerico) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });


});

export default router_usuario;