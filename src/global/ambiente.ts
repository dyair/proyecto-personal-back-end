// Port configuration
export const SERVER_PORT: string = (process.env.PORT) || JSON.stringify(5000);

// Credenciales para BD postgresSQL en la nube (Postgres-heroku).
export const host_BD: string = 'ec2-54-174-229-152.compute-1.amazonaws.com';
export const schema_BD: string = 'd2mg97tfbv5jjj'; 
export const user_BD: string = 'wglarypesrwbsa';
export const pw_BD: string = '544fe9f0cabb7bc780f72529d0fbe0042b0300d6f180ca8c3cdaae3bc5fdedb8';
export const port_BD: number = 5432;

// Útil cuando guardaba imagenes en el servidor, ya no más.
export const IP_SERVER_IMG: string = 'localhost';

// API Mapquest configuration
export const key_apigeocoding: string = 'HmyB1BHhuYA5qptss8JYFJBfFVAhu2pA';

export const ok_status: number = 200;

// Google sign-in credentials
export const CLIENT_ID: string = '360447745298-q40v2l62qhv4dek16a3bti074fii6lnb.apps.googleusercontent.com';
export const CLIENT_ID_SECRET: string = '5y4kacVdrqq8yprcO9zebOCt';

// Token Auth0.
export const clave_codificacion: string = 'clav3_s3cr3t4';

// Si no suben imagen de perfil de la mascota, usamos esta por defecto.
export const imagen_perfil_defecto: string = 'https://image.flaticon.com/icons/png/512/194/194177.png';
