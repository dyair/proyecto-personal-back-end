import { Sequelize } from 'sequelize';
import { pw_BD, user_BD, schema_BD, host_BD, port_BD } from '../global/ambiente';

export let sequelize: Sequelize;

sequelize = (process.env.PORT)
    ? new Sequelize(`postgres://${user_BD}:${pw_BD}@${host_BD}:${port_BD}/${schema_BD}`)
    : sequelize = new Sequelize('mascota_app', 'yair_mascotas', 'eriksen', {
        host: 'localhost',
        dialect: 'mariadb'
    });

export const testDB = sequelize.authenticate()
    .then(() => {

        // heroku pg:psql
        // sequelize.sync();
    })
    .catch((err: Error) => {
        console.error('Error database connection: ', err.message);
    });

