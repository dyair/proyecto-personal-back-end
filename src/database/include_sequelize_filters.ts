import { masc_bt_dom, masc_bt_sex, masc_bt_us, masc_bt_temp, masc_bt_tam, masc_bt_raz, masc_bt_tipo } from "./modelos/mascota/mascota";
import { dom_bt_loc } from "./modelos/ubicación/domicilio";
import { loc_bt_prov } from "./modelos/ubicación/localidad";
import { prov_bt_pais } from "./modelos/ubicación/provincia";

// Agrupar includes repetitivos y que son standar para varias consultas de tipo get en BD 
// hechas en el controlador de mascotas.

export const filtro_include_ubicacion_dom_to_country = {
    association: masc_bt_dom,
    as: 'domicilio',
    include: [{
        association: dom_bt_loc,
        as: 'localidad',
        include: [{
            association: loc_bt_prov,
            as: 'provincia',
            include: [
                {
                    association: prov_bt_pais,
                    as: 'pais'
                }
            ]
        }]
    }]
};

export const filtro_include_mascota = [
    {
        association: masc_bt_sex,
        as: 'sexo'
    },
    {
        association: masc_bt_us,
        as: 'usuario'
    },
    {
        association: masc_bt_temp,
        as: 'temperamento'
    },
    {
        association: masc_bt_tam,
        as: 'tamanio'
    },
    {
        association: masc_bt_raz,
        as: 'raza'
    },
    {
        association: masc_bt_tipo,
        as: 'tipo'
    }
];
