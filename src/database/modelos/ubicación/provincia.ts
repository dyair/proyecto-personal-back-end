import { DataTypes, Model } from 'sequelize';
import { sequelize } from "../../database";
import { Pais } from './pais';

export class Provincia extends Model {

    public id_provincia!: number;
    public nombre_provincia!: string;
}

Provincia.init({
    id_provincia: {
        type: new DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isNumeric: true,
            min: 0, 
            notNull: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_provincia: {
        type: new DataTypes.STRING(128),
        allowNull: false,
    }
}, 
{
    sequelize,
    timestamps: false,
    tableName: 'provincia',
});

export const prov_bt_pais= Provincia.belongsTo(Pais, { as: 'pais', foreignKey: 'pais_id' });
export const pais_hm_prov= Pais.hasMany(Provincia, { as: 'provincias', foreignKey: 'pais_id' });

// Provincia.sync();