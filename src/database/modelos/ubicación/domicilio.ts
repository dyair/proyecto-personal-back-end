import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Localidad } from './localidad';


export class Domicilio extends Model { 

    public id_domicilio!: number;
    public calle!: string;
    public numero!: number;
    public piso!: number;
    public depto!: string;
    public latitud_map!: number;
    public longitud_map!: number;
}

Domicilio.init({
    id_domicilio: {
        type: new DataTypes.INTEGER,
        validate:{
          isNumeric: true 
        },
        autoIncrement: true,
        primaryKey: true,
      },
      calle: {
          type: new DataTypes.STRING(128),
          allowNull: false,
      },
      numero: {
          type: new DataTypes.INTEGER,
          validate: {
              min: 0
          },
          allowNull: false,
      },
      piso: {
          type: new DataTypes.INTEGER,
          validate: {
              min: 0
          },
      },
      depto: {
          type: new DataTypes.STRING(128)
      },
      latitud_map: {
          type: new DataTypes.DOUBLE,
          validate: {
              isFloat: true,
              min: -90,
              max: 90
          },
          allowNull: false,
      },
      longitud_map: {
          type: new DataTypes.DOUBLE,
          validate: {
              isFloat: true,
              min: -180,
              max: 180
          },
          allowNull: false,
      }
    }, {
        sequelize,
        timestamps: false,
        tableName: 'domicilio',
});

export const dom_bt_loc= Domicilio.belongsTo(Localidad, { as: 'localidad', foreignKey: 'localidad_id' });
export const loc_hm_dom= Localidad.hasMany(Domicilio, { as: 'domicilios', foreignKey: 'localidad_id' });
    
// Domicilio.sync();