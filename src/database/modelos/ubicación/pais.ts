import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';

export class Pais extends Model {
  public id_pais!: number;
  public nombre_pais!: string;
}

Pais.init({
  id_pais: {
    type: new DataTypes.INTEGER,
    allowNull: false,
    validate: { 
      isNumeric: true, 
      notNull: true
    },
    autoIncrement: true,
    primaryKey: true,
  },
  nombre_pais: {
    type: new DataTypes.STRING(128),
    allowNull: false,
  }
}, {
    sequelize,
    timestamps: false,
    tableName: 'pais',
});

// Pais.sync();
