import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Provincia } from './provincia';


export class Localidad extends Model{
    public id_localidad!: number;
    public nombre_localidad!: string;
}

Localidad.init({

    id_localidad: {
        type: new DataTypes.INTEGER,  
        validate: { 
          isNumeric: true 
        },
        autoIncrement: true,
        primaryKey: true    
      },
      nombre_localidad: {
        type: new DataTypes.STRING(128),
        allowNull: false,
      }
    },
    {
      sequelize,  
      timestamps: false,
      tableName: 'localidad',
});

export const loc_bt_prov= Localidad.belongsTo(Provincia, { as: 'provincia', foreignKey: 'provincia_id' });
export const prov_hm_loc= Provincia.hasMany(Localidad, { as: 'localidades', foreignKey: 'provincia_id' });