import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';

export default class Temperamento extends Model {
    public id_temperamento!: number;
    public nombre_temperamento!: string;
}

Temperamento.init({
    id_temperamento: {
        type: new DataTypes.INTEGER,  
        autoIncrement: true,
        primaryKey: true,
},
    nombre_temperamento: {
        type: new DataTypes.STRING(128),
        allowNull: false,
}
}, {
    sequelize,
    timestamps: false,
    tableName: 'temperamento',
});