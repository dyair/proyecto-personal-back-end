import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';

export default class Sexo extends Model {
    public id_sexo!: number;
    public nombre_sexo!: string;
}

Sexo.init({
    id_sexo: {
      type: new DataTypes.INTEGER,  
      autoIncrement: true,
      primaryKey: true,
    },
    nombre_sexo: {
      type: new DataTypes.STRING(128),
      allowNull: false,
    }
  }, {
    sequelize,
    timestamps: false,
    tableName: 'sexo',
});