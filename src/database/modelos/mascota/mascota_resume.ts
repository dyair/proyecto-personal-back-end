import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';

export class Mascota_resumen extends Model {
    public id_mascota_resumen!: number;
    public foto_mascota1!: string;
    public foto_mascota2!: string;
    public foto_mascota3!: string;
}

Mascota_resumen.init({
    id_mascota_resumen: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    foto_mascota1: {
        type: new DataTypes.STRING(128),
        defaultValue: null
    },
    foto_mascota2: {
        type: new DataTypes.STRING(128),
        defaultValue: null
    },
    foto_mascota3: {
        type: new DataTypes.STRING(128),
        defaultValue: null 
    }
}, {
        sequelize,
        timestamps: false,
        tableName: 'mascota_resumen',
});

export const res_bt_masc = Mascota_resumen.belongsTo(Mascota, { as: 'mascota', foreignKey: 'mascota_id' });
export const masc_ho_res = Mascota.hasOne(Mascota_resumen, { as: 'resumen', foreignKey: 'mascota_id' });  