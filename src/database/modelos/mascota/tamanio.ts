import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';

export default class Tamanio extends Model {
    public id_tamanio!: number;
    public nombre_tamanio!: string;
}

Tamanio.init({
    id_tamanio: {
        type: new DataTypes.INTEGER,  
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_tamanio: {
        type: new DataTypes.STRING(128),
        allowNull: false,
    }
    }, {
        sequelize,
        timestamps: false,
        tableName: 'tamanio',
});