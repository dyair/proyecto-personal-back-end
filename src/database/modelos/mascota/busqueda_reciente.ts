import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';

export class Busqueda_reciente extends Model {
    public id_busqueda!: number;
}

Busqueda_reciente.init({
    id_busqueda: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    fecha_busqueda: {
        type: new DataTypes.DATE,
        allowNull: false
    }
}, {
        sequelize,
        timestamps: false,
        tableName: 'busqueda_reciente_mascotas',
    });

export const busq_bt_mascb = Busqueda_reciente.belongsTo(Mascota, { as: 'mascota_buscada', foreignKey: 'mascota_buscada_id' });
export const mascb_hm_busq = Mascota.hasMany(Busqueda_reciente, { as: 'busquedas', foreignKey: 'mascota_buscada_id' });

export const busq_bt_masc = Busqueda_reciente.belongsTo(Mascota, { as: 'mascota_buscadora', foreignKey: 'mascota_buscadora_id' });
export const busq_ho_masc = Mascota.hasOne(Busqueda_reciente, { as: 'busqueda', foreignKey: 'mascota_buscadora_id' });



