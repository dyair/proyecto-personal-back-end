import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';


export class Tipo_mascota extends Model {
    public id_tipo_mascota!: number;
    public nombre_tipo_mascota!: string;
}

Tipo_mascota.init({
    id_tipo_mascota: {
        type: new DataTypes.INTEGER,  
        validate:{
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_tipo_mascota: {
        type: new DataTypes.STRING(45),
        allowNull: false,
    }
    }, {
    sequelize,
    timestamps: false,
    tableName: 'tipo_mascota',
});

