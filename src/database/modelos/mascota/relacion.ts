import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';

export class Relacion extends Model {
    public id_relacion!: number;
    public fecha!: Date;
    public match!: boolean;
    public like_origen!: boolean;
    public like_destino!: boolean;
    public visto_origen!: boolean;
    public visto_destino!: boolean;
    // public match_final!: boolean;
}

Relacion.init({
    id_relacion: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    fecha: {
        type: new DataTypes.DATE,
        allowNull: false,
    },
    match: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    // match_final: {
    //     type: DataTypes.BOOLEAN,
    //     defaultValue: false,
    //     allowNull: false
    // },
    like_origen: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
        allowNull: false
    },
    like_destino: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    visto_origen: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    visto_destino: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }
}, {
        sequelize,
        timestamps: false,
        tableName: 'relacion',
    });

export const rel_bt_masco = Relacion.belongsTo(Mascota, { as: 'mascota_origen', foreignKey: 'mascota_origen_id' });
export const rel_bt_mascd = Relacion.belongsTo(Mascota, { as: 'mascota_destino', foreignKey: 'mascota_destino_id' });
