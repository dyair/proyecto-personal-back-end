import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';
import { Comunicacion } from './comunicacion';

export class Mensaje extends Model {
    public id_mensaje!: number;
    public fecha!: Date;
    public contenido!: string;
    public visto_origen!: boolean;
    public visto_destino!: boolean;
}

Mensaje.init({
    id_mensaje: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    fecha: {
        type: new DataTypes.DATE,
        allowNull: false
    },
    contenido: {
        type: new DataTypes.STRING,
        allowNull: false
    },
    visto_destino: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    visto_origen: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
        allowNull: false
    }
}, {
        sequelize,
        timestamps: false,
        tableName: 'mensaje',
});

export const sms_bt_cab= Mensaje.belongsTo(Comunicacion, { as: 'cabecera', foreignKey: 'cabecera_sms_id' });
export const cab_hm_sms= Comunicacion.hasMany(Mensaje, { as: 'mensajes', foreignKey: 'cabecera_sms_id' });

export const mascd_bt_sms= Mensaje.belongsTo(Mascota, { as: 'mascota_destino', foreignKey: 'mascota_destino_id' });
export const sms_hm_mascd= Mascota.hasOne(Mensaje, { as: 'mensaje_recibido', foreignKey: 'mascota_destino_id' });

export const masco_bt_sms= Mensaje.belongsTo(Mascota, { as: 'mascota_origen', foreignKey: 'mascota_origen_id' });
export const sms_hm_masco= Mascota.hasOne(Mensaje, { as: 'mensaje_enviado', foreignKey: 'mascota_origen_id' });

