import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';

export class Relacion_detalle extends Model {
    public id_relacion_detalle!: number;
    public like!: boolean;
    public visto!: boolean;
}

Relacion_detalle.init({
    id_relacion_detalle: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    like: {
        type: DataTypes.BOOLEAN,
        defaultValue: false, 
        allowNull: false
    },
    visto: {
        type: DataTypes.BOOLEAN, 
        defaultValue: false,
        allowNull: false
    },
}, {
        sequelize,
        timestamps: false,
        tableName: 'relacion_detalle', 
    });

export const rd_bt_mas = Relacion_detalle.belongsTo(Mascota, { as: 'mascota', foreignKey: 'mascota_id' });  
