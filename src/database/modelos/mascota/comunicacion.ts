import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';

export class Comunicacion extends Model {
    public id_comunicacion!: number;
}

Comunicacion.init({
    id_comunicacion: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    }
}, {
        sequelize,
        timestamps: false,
        tableName: 'comunicacion_sms',
    }); 

export const masco_bt_cab = Comunicacion.belongsTo(Mascota, { as: 'mascota_origen', foreignKey: 'mascota_origen_id' });
export const cab_ho_masco = Mascota.hasOne(Comunicacion, { as: 'cabecera_origen', foreignKey: 'mascota_origen_id' });

export const mascd_bt_cab = Comunicacion.belongsTo(Mascota, { as: 'mascota_destino', foreignKey: 'mascota_destino_id' });
export const cab_ho_mascd = Mascota.hasOne(Comunicacion, { as: 'cabecera_destino', foreignKey: 'mascota_destino_id' });
