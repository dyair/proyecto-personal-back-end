import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Domicilio } from '../ubicación/domicilio';
import Usuario from '../usuario';
import Tamanio from './tamanio';
import { Raza } from './raza';
import Sexo from './sexo';
import Temperamento from './temperamento';
import { Tipo_mascota } from './tipo_mascota';

/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto exporta solo librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/

export class Mascota extends Model {
    public id_mascota!: number;
    public nombre_mascota!: string;
    public fecha_nac!: Date;
    public profile_pic!: string;     //foto de perfil 
    public biografia!: string;
}

Mascota.init({
    id_mascota: {
        type: new DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_mascota: {
        type: new DataTypes.STRING(128),
        allowNull: false
    },
    fecha_nac: {
        type: new DataTypes.DATE,
        validate:{
            isDate: true
        },
        allowNull: false
    },
    profile_pic: {
        type: new DataTypes.STRING(128)
    },
    biografia: {
        type: new DataTypes.STRING(128),
        defaultValue: ''
    }
},
{
    sequelize,
    timestamps: false,
    tableName: 'mascota',
});

export const masc_bt_dom= Mascota.belongsTo(Domicilio, { as: 'domicilio', foreignKey: 'domicilio_id' });
export const dom_hm_masc= Domicilio.hasMany(Mascota, { as: 'mascotas', foreignKey: 'domicilio_id' });

export const masc_bt_us= Mascota.belongsTo(Usuario, { as: 'usuario', foreignKey: 'usuario_id' });
export const us_hm_masc= Usuario.hasMany(Mascota, { as: 'mascotas', foreignKey: 'usuario_id' });

export const masc_bt_sex= Mascota.belongsTo(Sexo, { as: 'sexo', foreignKey: 'sexo_id' });
export const sex_hm_masc= Sexo.hasMany(Mascota, { as: 'mascotas', foreignKey: 'sexo_id' });

export const masc_bt_temp= Mascota.belongsTo(Temperamento, { as: 'temperamento', foreignKey: 'temperamento_id' });
export const temp_hm_masc= Temperamento.hasMany(Mascota, { as: 'mascotas', foreignKey: 'temperamento_id' });

export const masc_bt_tam= Mascota.belongsTo(Tamanio, { as: 'tamanio', foreignKey: 'tamanio_id' });
export const tam_hm_masc= Tamanio.hasMany(Mascota, { as: 'mascotas', foreignKey: 'tamanio_id' });

export const masc_bt_raz= Mascota.belongsTo(Raza, { as: 'raza', foreignKey: 'raza_id' });
export const raz_hm_masc= Raza.hasMany(Mascota, { as: 'mascotas', foreignKey: 'raza_id' });

export const masc_bt_tipo= Mascota.belongsTo(Tipo_mascota, { as: 'tipo', foreignKey: 'tipo_mascota_id' });
export const tipo_hm_masc= Tipo_mascota.hasMany(Mascota, { as: 'mascotas', foreignKey: 'tipo_mascota_id' });
