import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Tipo_mascota } from './tipo_mascota';


export class Raza extends Model {
    public id_raza!: number;
    public nombre_raza!: string;
}

Raza.init({
    id_raza: {
        type: new DataTypes.INTEGER,  
        validate:{
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_raza: {
        type: new DataTypes.STRING(45),
        allowNull: false,
    }
    }, {
        sequelize,
        timestamps: false,
        tableName: 'raza',
    });

export const raz_bt_tmascota= Raza.belongsTo(Tipo_mascota, { as: 'tipo_mascota', foreignKey: 'tipo_mascota_id' });
export const tmascota_hm_raza= Tipo_mascota.hasMany(Raza, { as: 'razas', foreignKey: 'tipo_mascota_id' });