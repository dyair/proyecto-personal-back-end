import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../../database';
import { Mascota } from './mascota';

export class Post_mascota extends Model {
    public id_post!: number;
    public descripcion!: string;
    public fecha_publicacion!: Date;
    public archivos_path!: string
}

Post_mascota.init({
    id_post: {
        type: new DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: new DataTypes.STRING(512),
        allowNull: true,
    },
    fecha_publicacion: {
        type: new DataTypes.DATE,
        allowNull: false,
    },
    archivos_path: {
        type: new DataTypes.STRING(256),
        allowNull: true
    },
}, {
        sequelize,
        timestamps: false,
        tableName: 'post_mascota',
    });

export const post_bt_masc= Post_mascota.belongsTo(Mascota, { as: 'mascota', foreignKey: 'mascota_id' });
export const masc_hm_post= Mascota.hasMany(Post_mascota, { as: 'posteos', foreignKey: 'mascota_id' });