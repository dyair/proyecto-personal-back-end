import { DataTypes, Model } from 'sequelize';
import { sequelize } from '../database';

/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto solo se exporta solo se importan librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/ 

export default class Usuario extends Model {
    public id!: number;
    public nombre!: string;
    public email!: string;
    public password!: string;
    public google!: boolean
}

Usuario.init({
    id: {
      type: new DataTypes.INTEGER,  
      autoIncrement: true,
      primaryKey: true,
    },
    nombre: {
      type: new DataTypes.STRING(128),
      allowNull: false
    },
    email: {
      type: new DataTypes.STRING(128),
      allowNull: false,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: new DataTypes.STRING(128),
      allowNull: true,
    },
    google: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
  }, {
    sequelize,
    timestamps: false,
    tableName: 'usuario',
  });

