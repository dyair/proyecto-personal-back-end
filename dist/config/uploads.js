"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const customError_1 = __importDefault(require("./customError"));
const configMulter = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path_1.default.join(__dirname, '..', '..', 'uploads/'));
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    },
});
const filtros = function (req, file, cb) {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    }
    else {
        throw new customError_1.default('Formato de archivo no permitido. Solamente imágenes.', 404);
        // cb(null, false);
    }
};
exports.upload = multer_1.default({
    storage: configMulter,
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter: filtros
});
