"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ErrorGenerico extends Error {
    constructor(sms, status) {
        super(sms);
        this.status_code = status;
    }
}
exports.default = ErrorGenerico;
