"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const customError_1 = __importDefault(require("./customError"));
// API request to other server. Just simple HTTP call to get/send data from/to other server.
const external_request = {
    getMapquest(url) {
        return new Promise((resolve, reject) => {
            let body = '';
            let direccion = {};
            http_1.default.get(url, (resp) => {
                resp.on('data', (data) => {
                    body += data;
                });
                resp.on('end', () => {
                    var rdo = JSON.parse(body);
                    direccion = rdo.results[0].locations[0];
                    resolve(direccion);
                }).on('error', (error) => {
                    throw new customError_1.default(error.message, 503);
                });
            });
        });
    }
};
exports.default = external_request;
