"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mascota_socket_1 = require("./mascota_socket");
const lista_mascotas_socket_1 = require("./lista_mascotas_socket");
const mascota_controller_1 = __importDefault(require("../../../controller/mascota.controller"));
const customError_1 = __importDefault(require("../../customError"));
exports.petsConectados = new lista_mascotas_socket_1.ListaMascotaSocket();
// Logica para las acciones que se usan en los sockets.
exports.desconectar = (cliente, io) => {
    cliente.on('disconnect', () => {
        // el disconnect() se dispara desde el front-end (cerrar sesión).
        exports.petsConectados.eliminarPetSocketLista(cliente.id);
    });
};
exports.configurar_socket_user = (cliente, io) => {
    cliente.on('nueva_mascota_socket', (user_socket, callback) => {
        let nueva_mascota_socket = new mascota_socket_1.Mascota_socket_(cliente.id, user_socket.id_mascota, user_socket.nombre_mascota);
        exports.petsConectados.agregarPetSocketLista(nueva_mascota_socket);
        callback(`Mascota conectada: ${nueva_mascota_socket.nombre_mascota}`);
    });
};
exports.enviar_mensaje_chat = (cliente, io) => {
    cliente.on('enviar_nuevo_mensaje', (sms_nuevo, callback) => __awaiter(this, void 0, void 0, function* () {
        let sms = {};
        sms = yield mascota_controller_1.default.crearMensaje(sms_nuevo);
        if (sms.mascota_destino_id) {
            let pet = exports.petsConectados.getPetSocketFromListaByIDMascota(sms.mascota_destino_id);
            // dispara evento en directo si la mascota está conectada, sino nada.
            if (pet)
                io.in(pet.id_socket).emit('leer_nuevo_mensaje', sms);
        }
        callback(sms);
    }));
};
// sin uso por el momento.
exports.notificar_match = (cliente, io) => {
    // pre-condición: ambos likes deben estar en true && vista_receptora debe estar en falso.
    // Se llama desde el front cuando se retorne una relación con match.
    cliente.on('notificar_match_a_destino', (relacion) => __awaiter(this, void 0, void 0, function* () {
        if (!relacion.mascota_destino_id)
            throw new customError_1.default('Relación mal generada.', 404);
        const mascota_receptora = exports.petsConectados.getPetSocketFromListaByIDMascota(relacion.mascota_destino_id);
        if (!mascota_receptora || !relacion.match) {
            console.log('mascota_destino a notificar offline o no tiene el match.');
            return;
        }
        io.in(mascota_receptora.id_socket).emit('notificacion_de_un_match', relacion);
        console.log('Notificada la relación a la mascota destino online: ', mascota_receptora.id_mascota);
    }));
};
