"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ListaUsuariosSocket {
    constructor() {
        this.lista_sockets_usuarios = [];
    }
    getListaUsuariosSocket() {
        return this.lista_sockets_usuarios;
    }
    agregarUsuarioSocketLista(usuario) {
        this.lista_sockets_usuarios.push(usuario);
    }
    eliminarUsuarioSocketLista(id_socket) {
        this.lista_sockets_usuarios = this.lista_sockets_usuarios.filter(socket => socket.id_socket !== id_socket);
    }
    getUsuarioSocketFromLista(id_socket) {
        return this.lista_sockets_usuarios.find(socket => socket.id_socket === id_socket);
    }
}
exports.ListaUsuariosSocket = ListaUsuariosSocket;
