"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ListaMascotaSocket {
    constructor() {
        this.lista_sockets_usuarios = [];
    }
    getListaPetSocket() {
        return this.lista_sockets_usuarios;
    }
    agregarPetSocketLista(usuario) {
        this.lista_sockets_usuarios.push(usuario);
        console.log('LISTA AGREGADO: ', this.lista_sockets_usuarios);
    }
    eliminarPetSocketLista(id_socket) {
        this.lista_sockets_usuarios = this.lista_sockets_usuarios.filter(socket => socket.id_socket !== id_socket);
        console.log('LISTA ELIMINADO: ', this.lista_sockets_usuarios);
    }
    getPetSocketFromListaByIDSocket(id_socket) {
        return this.lista_sockets_usuarios.find(socket => socket.id_socket === id_socket);
    }
    getPetSocketFromListaByIDMascota(id_mascota) {
        return this.lista_sockets_usuarios.find(socket => socket.id_mascota === id_mascota);
    }
}
exports.ListaMascotaSocket = ListaMascotaSocket;
