"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const customError_1 = __importDefault(require("../customError"));
exports.encriptar_pw = (pw) => __awaiter(this, void 0, void 0, function* () {
    const saltos_round = 11;
    let pw_encriptada;
    try {
        pw_encriptada = yield bcrypt_1.default.hash(pw, saltos_round);
    }
    catch (e) {
        throw new customError_1.default(e.message, 503);
    }
    return pw_encriptada;
});
exports.validar_pw = (pw_ingresada, pw_encriptada) => __awaiter(this, void 0, void 0, function* () {
    let pw_valida = false;
    pw_valida = yield bcrypt_1.default.compare(pw_ingresada, pw_encriptada);
    return pw_valida;
});
