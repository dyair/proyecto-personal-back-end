"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const customError_1 = __importDefault(require("../../customError"));
const fs_1 = __importDefault(require("fs"));
// Guardar file en memoria as buffer. OJO subir cosas pesadas o muchos files chicos, puede haber overflow memory.
const configMulterMemory = multer_1.default.memoryStorage();
// Config para guardar el file en disco.
const configMulterDisk = multer_1.default.diskStorage({
    destination: function (req, file, cb) {
        return __awaiter(this, void 0, void 0, function* () {
            yield cb(null, path_1.default.join(__dirname, '..', '..', '..', '..', 'uploads/'));
        });
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + path_1.default.extname(file.originalname));
    },
});
const filtros = function (req, file, cb) {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    }
    else {
        throw new customError_1.default('ERROR --> Formato de archivo no permitido, solamente imágenes.', 404);
        // cb(null, false);
    }
};
// esta lanzando siempre una excepción aunque borra el archivo.
exports.borrar_file_servidor = (path_servidor_img) => {
    // Obtener la ruta absoluta del servidor donde se guardan las imagenes.
    let ruta = path_servidor_img.substring(path_servidor_img.indexOf('/uploads/'));
    let ruta_folder_img = path_1.default.join(__dirname, '..', '..', '..', ruta);
    fs_1.default.unlink(ruta_folder_img, (err) => {
        if (err) {
            console.error(err);
            throw new customError_1.default('Excepción guardando el archivo en el servidor local antes de subirlo a la nube. Se suspende el proceso de subida.', 503);
        }
        console.log('File ' + ruta_folder_img + ' was deleted!');
    });
};
exports.upload = multer_1.default({
    storage: configMulterMemory,
    limits: { fileSize: 1024 * 1024 * 5 },
    fileFilter: filtros
});
