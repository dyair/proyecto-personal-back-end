"use strict";
/* GESTIÓN DE IMÁGENES EN LA NUBE (cloudinary) */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Información para usar multer, cloudinary y expressjs.
// https://medium.com/@joeokpus/uploading-images-to-cloudinary-using-multer-and-expressjs-f0b9a4e14c54
exports.cloudinary = require('cloudinary').v2;
const DataURI = require('datauri');
const customError_1 = __importDefault(require("../../customError"));
const uploads_1 = require("./uploads");
const path_1 = __importDefault(require("path"));
exports.cloudinary.config({
    cloud_name: 'df6a7tbc3',
    api_key: '478571369486339',
    api_secret: 'GcIwveycrH42apscedMssxepwPU'
});
exports.subirFileCloudinaryDisk = (path_file) => {
    let path_return = '';
    exports.cloudinary.uploader.upload(path_file, (e, rdo) => {
        if (e) {
            console.error(e);
            throw new customError_1.default(e.message + '. || ERROR SUBIENDO FILE A Cloudinary.', 503);
        }
        uploads_1.borrar_file_servidor(path_file);
        path_return = rdo.secure_url;
    });
    return path_return;
};
exports.subirFileCloudinaryBuffer = (buffer_content) => __awaiter(this, void 0, void 0, function* () {
    let opcional_parameters = {};
    let path_return = '';
    let resultado;
    try {
        resultado = yield exports.cloudinary.uploader.upload(buffer_content, opcional_parameters);
    }
    catch (e) {
        throw new customError_1.default(e.message + ' || ERROR SUBIENDO FILE A Cloudinary.', 503);
    }
    path_return = resultado.secure_url;
    return path_return;
});
exports.getDataURI = (file_from_multer) => {
    const datauri = new DataURI();
    datauri.format(path_1.default.extname(file_from_multer.originalname).toString(), file_from_multer.buffer);
    return datauri;
};
