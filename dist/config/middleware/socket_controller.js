"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Logica para las acciones que se usan en los sockets.
exports.desconectar = (cliente) => {
    cliente.on('disconnect', () => {
        console.log('Cliente desconectado');
    });
};
exports.nuevo_mensaje = (cliente, io) => {
    cliente.on('nuevo_mensaje_al_server', (sms_nuevo, callback) => {
        console.log(sms_nuevo);
        callback('probando callback del servidor');
        // io.emit('nuevo_mensaje_al_cliente')
    });
};
