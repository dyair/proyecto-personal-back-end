"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const ambiente_1 = require("../../global/ambiente");
exports.crearToken = (usuario) => {
    let token = jsonwebtoken_1.default.sign(usuario, ambiente_1.clave_codificacion, {
        expiresIn: '4h' // expires in 4hs
    });
    return `Bearer ${token}`;
};
exports.validarToken = (token_cabecera) => {
    let token_valido;
    let token = token_cabecera.split(" ")[1];
    try {
        jsonwebtoken_1.default.verify(token, ambiente_1.clave_codificacion);
        token_valido = true;
    }
    catch (error) {
        console.log(error.message);
        token_valido = false;
    }
    return token_valido;
};
exports.autorizacion_token = (req, resp, next) => {
    let token = req.headers.authorization;
    if (typeof token === 'undefined') {
        return resp.status(401).json({
            ok: false,
            mensaje: 'Token de autenticación inválido.'
        });
    }
    let token_valido = exports.validarToken(token);
    if (token_valido)
        next();
    else
        return resp.status(401).json({
            ok: false,
            mensaje: 'Token de autenticación inválido.'
        });
};
