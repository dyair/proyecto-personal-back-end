"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ambiente_1 = require("../../global/ambiente");
const http_1 = __importDefault(require("http"));
const socket_io_1 = __importDefault(require("socket.io"));
const socket_controller = __importStar(require("./sockets/socket_controller"));
const jwtauth_1 = require("./jwtauth");
const customError_1 = __importDefault(require("../customError"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.puerto = ambiente_1.SERVER_PORT;
        this.http_server = new http_1.default.Server(this.app);
        this.io = socket_io_1.default(this.http_server);
        this.escucharSocket();
    }
    start(calllback) {
        this.http_server.listen(this.puerto, calllback);
    }
    static getInstance() {
        return this._instance || (this._instance = new this());
    }
    escucharSocket() {
        this.io
            .use((socket, next) => {
            if (!socket.handshake.query.token)
                throw new customError_1.default('Socket sin los parámetros de autenticación necesarios.', 401);
            let token = socket.handshake.query.token;
            let token_valido = jwtauth_1.validarToken(token);
            if (!token_valido)
                throw new customError_1.default('Error de autenticación. Token no válido.', 401);
            next();
        })
            .on('connection', (cliente) => {
            // Set nueva mascota socket.
            socket_controller.configurar_socket_user(cliente, this.io);
            // Enviar sms de mascota_origen y enviarlo a mascota_destino.
            socket_controller.enviar_mensaje_chat(cliente, this.io);
            // notificar match a usuario si está online. Suspendido momentaneamente.
            socket_controller.notificar_match(cliente, this.io);
            // Desconectar mascota socket.
            socket_controller.desconectar(cliente, this.io);
        });
    }
}
exports.default = Server;
