"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mascota_controller_1 = __importDefault(require("../controller/mascota.controller"));
const uploads_1 = require("../config/middleware/file_management/uploads");
const ambiente_1 = require("../global/ambiente");
const configuracion_controller_1 = __importDefault(require("../controller/configuracion.controller"));
const jwtauth_1 = require("../config/middleware/jwtauth");
const router_mascota = express_1.Router();
router_mascota.get('/:id_mascota', jwtauth_1.autorizacion_token, (req, resp) => {
    mascota_controller_1.default.getMascotaByID(req.params.id_mascota)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
// es un put pero el put no permite el formdata por eso queda post. 
router_mascota.post('/:id_mascota', jwtauth_1.autorizacion_token, uploads_1.upload.single('imagen'), (req, resp) => {
    let mascota_ = { id_mascota: Number(req.params.id_mascota) };
    if (req.body.sexo_id)
        mascota_.sexo_id = Number(req.body.sexo_id);
    if (req.body.temperamento_id)
        mascota_.temperamento_id = Number(req.body.temperamento_id);
    if (req.body.tamanio_id)
        mascota_.tamanio_id = Number(req.body.tamanio_id);
    if (req.body.usuario_id)
        mascota_.usuario_id = Number(req.body.usuario_id);
    if (req.body.fecha_nac)
        mascota_.fecha_nac = new Date(req.body.fecha_nac);
    if (req.body.nombre_mascota)
        mascota_.nombre_mascota = req.body.nombre_mascota;
    if (req.body.biografia)
        mascota_.biografia = req.body.biografia;
    if (req.body.raza_id)
        mascota_.raza_id = Number(req.body.raza_id);
    if (req.file)
        mascota_.file_img = req.file;
    if (req.body.domicilio_id)
        mascota_.domicilio_id = Number(req.body.domicilio_id);
    mascota_controller_1.default.editarMascota(mascota_)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.put('/:id_domicilio', jwtauth_1.autorizacion_token, (req, resp) => {
    let domicilio = req.body.domicilio;
    configuracion_controller_1.default.editarDomicilioMascota(domicilio)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.post('/:id_mascota_origen/relacion', jwtauth_1.autorizacion_token, (req, resp) => {
    const origen = req.body.relacion.origen;
    const destino = req.body.relacion.destino;
    mascota_controller_1.default.crearRelacion(origen, destino)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.get('/:id_mascota/search', jwtauth_1.autorizacion_token, (req, resp) => {
    // Buscar mascotas por filtros.
    let id_mascota = +req.params.id_mascota;
    console.log('QUERY DEL SEARCH DE MASCOTA:');
    console.log({ query: req.query });
    mascota_controller_1.default.getMascotasByFiltros(req.query, id_mascota)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.post('/:id_mascota/fotos_resumen', jwtauth_1.autorizacion_token, uploads_1.upload.array('imagenes'), (req, resp) => {
    const rdo = req.files; // tipo: Express.Multer.File[]
    mascota_controller_1.default.crearResumenFotosMascota(Number(req.params.id_mascota), rdo)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.put('/:id_mascota/fotos_resumen/:id_resumen', jwtauth_1.autorizacion_token, uploads_1.upload.array('imagenes'), (req, resp) => {
    const rdo = req.files; // tipo: Express.Multer.File[]
    let arrayPath = [];
    if (rdo) {
        for (let i = 0; i < rdo.length; i++) {
            if (rdo[i])
                arrayPath.push(rdo[i].path); // Permitir que suban de 1, 2 o 3 fotos sin que explote.
        }
    }
    let resumen = {
        id_mascota_resumen: Number(req.params.id_resumen),
        mascota_id: Number(req.params.id_mascota),
        mascota_fotos: arrayPath
    };
    mascota_controller_1.default.editarResumenFotosMascota(resumen)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.post('/:id_mascota/publicacion/', jwtauth_1.autorizacion_token, uploads_1.upload.array('imagenes', 3), (req, resp) => {
    let arrayPath = [];
    const rdo = req.files;
    let array_files = [];
    if (rdo) {
        for (let i = 0; i < rdo.length; i++) {
            array_files.push(rdo[i]); // Permitir que suban de 1, 2 o 3 fotos sin que explote.
        }
    }
    let post = {
        mascota_id: Number(req.params.id_mascota),
        fecha_publicacion: new Date(),
        descripcion: req.body.descripcion,
        files_uploaded: array_files
    };
    mascota_controller_1.default.crearPost(post)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.get('/:id_mascota/publicacion', jwtauth_1.autorizacion_token, (req, resp) => {
    let fecha_desde = new Date(JSON.parse(req.query.fecha_desde));
    mascota_controller_1.default.getPosteos(req.params.id_mascota, fecha_desde)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.get('/:id_mascota/publicacion/cantidad_post', jwtauth_1.autorizacion_token, (req, resp) => {
    let id_mascota = Number(req.params.id_mascota);
    mascota_controller_1.default.getCantidadPostDeMascota(id_mascota)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.delete('/:id_mascota/publicacion/:id_public', jwtauth_1.autorizacion_token, (req, resp) => {
    let id_post = Number(req.params.id_public);
    mascota_controller_1.default.deletePost(id_post)
        .then((id_post) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido: id_post
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.post('/:id_mascota/comunicacion', jwtauth_1.autorizacion_token, (req, resp) => {
    let cab = req.body.comunicacion;
    mascota_controller_1.default.crearComunicacion(cab)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.post('/:id_mascota/comunicacion/:id_com/mensaje', jwtauth_1.autorizacion_token, (req, resp) => {
    let sms = req.body.mensaje;
    mascota_controller_1.default.crearMensaje(sms)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.get('/:id_mascota/comunicacion', jwtauth_1.autorizacion_token, (req, resp) => {
    mascota_controller_1.default.traerComunicacionesDeMascota(Number(req.params.id_mascota))
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.get('/:id_mascota/comunicacion/:id_com', jwtauth_1.autorizacion_token, (req, resp) => {
    const id_com = Number(req.params.id_com);
    mascota_controller_1.default.traerComunicacionDeMascotaByID(id_com)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.get('/:id_mascota/comunicacion/:id_comunicacion/mensajes/:fecha_desde', jwtauth_1.autorizacion_token, (req, resp) => {
    const id_com = Number(req.params.id_comunicacion);
    const fecha_desde = new Date(req.params.fecha_desde);
    mascota_controller_1.default.getMensajesComunicacion(id_com, fecha_desde)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
//terminar
router_mascota.post('/:id_mascota/perfil/:id_mascota_vista', jwtauth_1.autorizacion_token, (req, resp) => {
    let id_mascota = Number(req.params.id_mascota);
    let id_mascota_vista = Number(req.params.id_mascota_vista);
    mascota_controller_1.default.setPreferenciaBusquedaMascota(id_mascota, id_mascota_vista)
        .then(() => {
        resp.status(ambiente_1.ok_status).json({
            ok: true
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_mascota.put('/:id_mascota/set_sms_visto', jwtauth_1.autorizacion_token, (req, resp) => {
    const mensajes_vistos = req.body.mensajes_id;
    mascota_controller_1.default.setMensajeVisto(mensajes_vistos)
        .then(() => {
        resp.status(ambiente_1.ok_status).json({
            ok: true
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
//Obtener los mensajes no leidos de comunicaciones
router_mascota.get('/:id_mascota/notificacion/sms_no_visto', jwtauth_1.autorizacion_token, (req, resp) => {
    let id_mascota = Number(req.params.id_mascota);
    mascota_controller_1.default.getMensajesNoVistosComunicacion(id_mascota)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
//Obtener las relaciones no leidas donde sea la mascota el destino de la relación.
router_mascota.get('/:id_mascota/notificacion/relacion_no_vista', jwtauth_1.autorizacion_token, (req, resp) => {
    let id_mascota = Number(req.params.id_mascota);
    mascota_controller_1.default.getRelacionesNoVistasMascota(id_mascota)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
exports.default = router_mascota;
