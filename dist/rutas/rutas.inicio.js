"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const usuario_controller_1 = __importDefault(require("../controller/usuario.controller"));
const ambiente_1 = require("../global/ambiente");
const router = express_1.Router();
router.post('/login_google', (req, resp) => __awaiter(this, void 0, void 0, function* () {
    let token_google = req.body.token;
    if (token_google) {
        let usuario_google = yield usuario_controller_1.default.verify(token_google);
        usuario_controller_1.default.iniciarSesionByGoogle(usuario_google)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((error) => {
            console.error(error);
            resp.status(error.status_code).json({
                ok: false,
                mensaje: error.message,
                code: error.status_code
            });
        });
    }
}));
router.post('/login', (req, resp) => {
    let usuario = {
        email: req.body.email,
        password: req.body.password
    };
    usuario_controller_1.default.iniciarSesion(usuario)
        .then((usuario) => {
        resp.status(ambiente_1.ok_status).json({
            contenido: usuario
        });
    })
        .catch((error) => {
        console.error(error);
        resp.status(error.status_code).json({
            ok: false,
            mensaje: error.message,
            code: error.status_code
        });
    });
});
router.post('/registrar', (req, resp) => {
    let usuario = req.body.usuario;
    if (usuario.password && usuario.password.length < 5) {
        resp.status(404).json({
            ok: false,
            mensaje: 'Mínimo 6 caracteres para la contraseña del usuario.',
            code: 404
        });
    }
    usuario_controller_1.default.crearUsuario(usuario)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((error) => {
        resp.status(error.status_code).json({
            ok: false,
            mensaje: error.message,
            code: error.status_code
        });
    });
});
exports.default = router;
