"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mascota_controller_1 = __importDefault(require("../controller/mascota.controller"));
const uploads_1 = require("../config/middleware/file_management/uploads");
const ambiente_1 = require("../global/ambiente");
const usuario_controller_1 = __importDefault(require("../controller/usuario.controller"));
const jwtauth_1 = require("../config/middleware/jwtauth");
const router_usuario = express_1.Router();
router_usuario.post('/:usuario_id/mascota', jwtauth_1.autorizacion_token, uploads_1.upload.single('imagen'), (req, resp) => {
    let mascota_ = {};
    if (req.body.sexo_id)
        mascota_.sexo_id = Number(req.body.sexo_id);
    if (req.body.tipo_mascota_id)
        mascota_.tipo_mascota_id = Number(req.body.tipo_mascota_id);
    if (req.body.temperamento_id)
        mascota_.temperamento_id = Number(req.body.temperamento_id);
    if (req.body.tamanio_id)
        mascota_.tamanio_id = Number(req.body.tamanio_id);
    if (req.params.usuario_id)
        mascota_.usuario_id = Number(req.params.usuario_id);
    if (req.body.fecha_nac)
        mascota_.fecha_nac = new Date(req.body.fecha_nac);
    if (req.body.nombre_mascota)
        mascota_.nombre_mascota = req.body.nombre_mascota;
    if (req.body.biografia)
        mascota_.biografia = req.body.biografia;
    if (req.body.tipo_id)
        mascota_.tipo_mascota_id = Number(req.body.tipo_id);
    if (req.body.raza_id)
        mascota_.raza_id = Number(req.body.raza_id);
    if (req.file)
        mascota_.file_img = req.file;
    if (req.body.domicilio_id)
        mascota_.domicilio_id = Number(req.body.domicilio_id);
    let id_usuario = Number(req.params.usuario_id);
    mascota_controller_1.default.crearMascota(mascota_, id_usuario)
        .then((contenido) => {
        resp.status(ambiente_1.ok_status).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_usuario.post('/:id/mascota/:id_mascota/fotos', jwtauth_1.autorizacion_token, uploads_1.upload.array('imagenes', 3), (req, resp) => {
    let id = Number(req.params.id_mascota);
    mascota_controller_1.default.crearResumenFotosMascota(id, req.files)
        .then((resumen_mascota) => {
        resp.status(200).json({
            ok: true,
            resumen_mascota
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
router_usuario.get('/:id_usuario/mascota', jwtauth_1.autorizacion_token, (req, resp) => {
    let id_user = Number(req.params.id_usuario);
    usuario_controller_1.default.getMascotasByUser(id_user)
        .then((contenido) => {
        resp.status(200).json({
            ok: true,
            contenido
        });
    })
        .catch((e) => {
        resp.status(e.status_code).json({
            ok: false,
            mensaje: e.message,
            code: e.status_code
        });
    });
});
exports.default = router_usuario;
