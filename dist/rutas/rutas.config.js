"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ambiente_1 = require("../global/ambiente");
const configuracion_controller_1 = __importDefault(require("../controller/configuracion.controller"));
const router_config = express_1.Router();
router_config.get('/pais', (req, resp) => {
    let token = 'asd'; // req.headers.authorization;
    if (token) {
        configuracion_controller_1.default.getPais()
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/provincia/:id_pais', (req, resp) => {
    let token = 'AS'; // req.headers.authorization;
    let id_pais = Number(req.params.id_pais);
    if (token) {
        configuracion_controller_1.default.getProvinciaByIDPais(id_pais)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/localidad/:id_prov', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    let id_prov = Number(req.params.id_prov);
    if (token) {
        configuracion_controller_1.default.getLocalidadByIDProv(id_prov)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/raza', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    if (token) {
        configuracion_controller_1.default.getRaza()
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/raza/:id', (req, resp) => {
    let token = req.headers.authorization;
    let id = Number(req.params.id);
    if (token) {
        configuracion_controller_1.default.getRazaByID(id)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/tipo_mascota', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    if (token) {
        configuracion_controller_1.default.getTipoMascota()
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/tamanio', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    if (token) {
        configuracion_controller_1.default.getTamanio()
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/tamanio/:id', (req, resp) => {
    let token = req.headers.authorization;
    let id = Number(req.params.id);
    if (token) {
        configuracion_controller_1.default.getTamanioByID(id)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/temperamento', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    if (token) {
        configuracion_controller_1.default.getTemperamento()
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/temperamento/:id', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    let id = Number(req.params.id);
    if (token) {
        configuracion_controller_1.default.getTemperamentoByID(id)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/sexo', (req, resp) => {
    let token = 'asd'; //req.headers.authorization;
    if (token) {
        configuracion_controller_1.default.getSexo()
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/sexo/:id', (req, resp) => {
    let token = req.headers.authorization;
    let id = Number(req.params.id);
    if (token) {
        configuracion_controller_1.default.getSexoByID(id)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.get('/comunicacion/:id', (req, resp) => {
    let token = 'das'; //req.headers.authorization;
    let id = Number(req.params.id);
    if (token) {
        configuracion_controller_1.default.getComunicacionByID(id)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token nulo.',
            code: 401
        });
    }
});
router_config.post('/domicilio', (req, resp) => {
    let token = 'asd'; // req.headers.authorization;
    let domicilio = req.body.domicilio;
    if (token) {
        configuracion_controller_1.default.crearDomicilio(domicilio)
            .then((contenido) => {
            resp.status(ambiente_1.ok_status).json({
                ok: true,
                contenido
            });
        })
            .catch((e) => {
            resp.status(e.status_code).json({
                ok: false,
                mensaje: e.message,
                code: e.status_code
            });
        });
    }
    else {
        resp.status(401).json({
            ok: false,
            mensaje: 'Token de autenticación nulo.'
        });
    }
});
exports.default = router_config;
