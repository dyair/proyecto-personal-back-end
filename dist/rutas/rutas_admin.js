"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const AdminBro = require('admin-bro');
const AdminBroExpress = require('admin-bro-expressjs');
const database_1 = require("../database/database");
AdminBro.registerAdapter(require('admin-bro-sequelizejs'));
const adminBro = new AdminBro({
    databases: [database_1.sequelize],
    rootPath: '/admin'
});
exports.router_admin = AdminBroExpress.buildRouter(adminBro);
exports.path_admin = adminBro.options.rootPath;
