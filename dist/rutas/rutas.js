"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const usuario_controller_1 = __importDefault(require("../controller/usuario.controller"));
const router = express_1.Router();
exports.ok_status = 200;
// Todavía no encuentro forma de castear el body para rechazar la basura que no coincide con los modelos.
router.post('/login', (req, resp) => {
    let usuario = req.body.usuario;
    usuario_controller_1.default.iniciarSesion(usuario)
        .then((usuario) => {
        resp.status(exports.ok_status).json({
            nombre: usuario.nombre,
            token: usuario.token,
            correo: usuario.email
        });
    })
        .catch((error) => {
        console.error(error);
        resp.status(error.status_code).json({
            ok: false,
            mensaje: error.message,
            code: error.status_code
        });
    });
});
router.post('/registrar', (req, resp) => {
    let usuario = req.body.usuario;
    usuario_controller_1.default.crearUsuario(usuario)
        .then((usuario) => {
        console.log(usuario);
        resp.status(exports.ok_status).json({
            ok: true,
            id: usuario.id
        });
    })
        .catch((error) => {
        console.error(error);
        resp.status(error.status_code).json({
            ok: false,
            mensaje: error.message,
            code: error.status_code
        });
    });
});
exports.default = router;
