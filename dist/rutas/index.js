"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./config/server"));
const rutas_inicio_1 = __importDefault(require("./rutas/rutas.inicio"));
const bodyParser = require("body-parser");
const cors_1 = __importDefault(require("cors"));
const rutas_mascota_1 = __importDefault(require("./rutas/rutas.mascota"));
const rutas_usuario_1 = __importDefault(require("./rutas/rutas.usuario"));
const rutas_config_1 = __importDefault(require("./rutas/rutas.config"));
const server = new server_1.default();
//Configuración para capturar parametros en llamadas API.
server.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
server.app.use(bodyParser.json({ limit: '50mb' }));
//Permitir que las consultas sean desde lugares distintos al host del server
server.app.use(cors_1.default({ origin: true, credentials: true }));
//Rutas de servicios
server.app.use('/inicio', rutas_inicio_1.default);
server.app.use('/usuario', rutas_usuario_1.default);
server.app.use('/mascota/', rutas_mascota_1.default);
server.app.use('/configuracion', rutas_config_1.default);
server.start(() => {
});
// Mascota.create({
//     nombre_mascota: 'Lupo',
//     raza_id: 1,
//     temperamento_id: 1,
//     tamanio_id: 1,
//     fecha_nac: new Date(),
//     domicilio_id: 2,
//     usuario_id: 9,
//     sexo_id: 1 
// })
// .then(()=> {
//     console.log('creado');
// })
