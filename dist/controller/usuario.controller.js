"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const usuario_1 = __importDefault(require("../database/modelos/usuario"));
const customError_1 = __importDefault(require("../config/customError"));
const mascota_1 = require("../database/modelos/mascota/mascota");
const ambiente_1 = require("../global/ambiente");
const mascota_controller_1 = __importDefault(require("./mascota.controller"));
const domicilio_1 = require("../database/modelos/ubicaci\u00F3n/domicilio");
const localidad_1 = require("../database/modelos/ubicaci\u00F3n/localidad");
const provincia_1 = require("../database/modelos/ubicaci\u00F3n/provincia");
const jwtauth_1 = require("../config/middleware/jwtauth");
const bcrypt_1 = require("../config/middleware/bcrypt");
const { OAuth2Client } = require('google-auth-library');
const usuarioController = {
    // Verifica autenticación con Google.
    verify(token) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = {};
            try {
                const client = new OAuth2Client(ambiente_1.CLIENT_ID);
                const ticket = yield client.verifyIdToken({
                    idToken: token,
                    audience: ambiente_1.CLIENT_ID
                });
                const payload = ticket.getPayload();
                user.email = payload.email;
                user.nombre = payload.given_name;
                user.password = '$&g00gle&$';
                user.google = true;
            }
            catch (e) {
                throw new customError_1.default(e.message + '. Falla Auth0 de Google.', 503);
            }
            return user;
        });
    },
    crearUsuario(usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = {};
            if (!usuario.email)
                throw new customError_1.default('Usuario con email vacío', 404);
            if (!usuario.nombre || !usuario.password)
                throw new customError_1.default('Usuario con nombre o pw vacía', 404);
            yield usuario_1.default.findAll({
                raw: true,
                where: {
                    email: usuario.email
                }
            })
                .then((usuarios) => {
                if (usuarios.length > 0)
                    throw new customError_1.default('Correo electrónico ya utilizado. Intente con otro.', 404);
            }, ((error) => {
                throw new customError_1.default(error.message, 503);
            }));
            //encriptar pw y validar encriptación.
            usuario.password = yield bcrypt_1.encriptar_pw(usuario.password);
            if (typeof usuario.password === "undefined")
                throw new customError_1.default('Error generando el hash para cifrar la pw en BD.', 503);
            yield usuario_1.default.create({
                nombre: usuario.nombre,
                password: usuario.password,
                email: usuario.email,
                google: usuario.google
            })
                .then((usuarioDB) => {
                user = usuarioDB.get();
            }, ((error) => {
                throw new customError_1.default(error.message, 503);
            }));
            return user;
        });
    },
    iniciarSesion(usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            let user = {};
            yield usuario_1.default.findOne({
                raw: true,
                where: {
                    email: usuario.email
                }
            })
                .then((us) => {
                // Valido que el mail no haya sido utilizado antes en el login gmail.
                if (!us)
                    throw new customError_1.default('Correo electrónico no utilizado.', 401);
                user = us;
                if (user.google)
                    throw new customError_1.default('Correo ya utilizado para registrarse vía Gmail. Inicie sesión por esa vía o utilice otro coreo.', 401);
            }, (error) => {
                throw new customError_1.default(error.message, 503);
            });
            if (!user.password)
                throw new customError_1.default('Usuario sin contraseña, error.', 404);
            let pw_valida = false;
            pw_valida = yield bcrypt_1.validar_pw(usuario.password, user.password);
            if (pw_valida === false)
                throw new customError_1.default('Credenciales incorrectas.', 401);
            user.token = jwtauth_1.crearToken(user);
            return user;
        });
    },
    iniciarSesionByGoogle(usuario) {
        return __awaiter(this, void 0, void 0, function* () {
            // Crear y loguearse con usuario de google, todo en uno. Funciona!
            let user = {};
            if (!usuario.email)
                throw new customError_1.default('Correo electrónico vacío', 404);
            yield usuario_1.default.findOrCreate({
                where: {
                    email: usuario.email
                },
                defaults: {
                    email: usuario.email,
                    nombre: usuario.nombre,
                    password: usuario.password,
                    google: usuario.google
                }
            })
                .then(([usuario, nuevo]) => {
                user = usuario.get();
                user.token = jwtauth_1.crearToken(usuario.get());
                console.log('usuario de google nuevo?: ', nuevo);
            }, (error) => {
                throw new customError_1.default(error.message, 503);
            });
            return user;
        });
    },
    getMascotasByUser(id_user) {
        return __awaiter(this, void 0, void 0, function* () {
            let mascotas_ = [];
            yield mascota_1.Mascota.findAll({
                where: {
                    usuario_id: id_user
                },
                include: [{
                        association: mascota_1.masc_bt_sex,
                        as: 'sexo'
                    },
                    {
                        association: mascota_1.masc_bt_dom,
                        as: 'domicilio',
                        include: [
                            {
                                association: domicilio_1.dom_bt_loc,
                                as: 'localidad',
                                include: [{
                                        association: localidad_1.loc_bt_prov,
                                        as: 'provincia',
                                        include: [
                                            {
                                                association: provincia_1.prov_bt_pais,
                                                as: 'pais'
                                            }
                                        ]
                                    }]
                            }
                        ]
                    },
                    {
                        association: mascota_1.masc_bt_us,
                        as: 'usuario'
                    },
                    {
                        association: mascota_1.masc_bt_temp,
                        as: 'temperamento'
                    },
                    {
                        association: mascota_1.masc_bt_tam,
                        as: 'tamanio'
                    },
                    {
                        association: mascota_1.masc_bt_raz,
                        as: 'raza'
                    },
                    {
                        association: mascota_1.masc_bt_tipo,
                        as: 'tipo'
                    }]
            })
                .then((mascotas) => {
                mascotas.forEach(mascota => {
                    let mas = mascota_controller_1.default.llenarMascota(mascota);
                    mascotas_.push(mas);
                });
            }, (error) => {
                throw new customError_1.default(error.message, 503);
            });
            return mascotas_;
        });
    }
};
exports.default = usuarioController;
