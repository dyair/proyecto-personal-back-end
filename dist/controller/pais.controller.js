"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const usuario_1 = __importDefault(require("../database/modelos/usuario"));
const domicilio_1 = require("../database/modelos/ubicaci\u00F3n/domicilio");
const customError_1 = __importDefault(require("../config/customError"));
const ubicacionController = {
    buscarDomicilio(id_dom) {
        return __awaiter(this, void 0, void 0, function* () {
            let dom = {};
            yield domicilio_1.Domicilio.findByPk(id_dom).then((domicilio) => {
                if (domicilio == null)
                    throw new customError_1.default('Domicilio inexistente.', 404);
                dom = domicilio.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return dom;
        });
    },
    crearPais(pais) {
        return __awaiter(this, void 0, void 0, function* () {
            pais.nombre_pais = 'Alemania';
            yield usuario_1.default.findAll({
                where: { nombre: pais.nombre_pais }
            })
                .then((pais) => {
                if (pais.length > 0) {
                    throw new Error('nombre de país repetido.');
                }
            })
                .catch(e => {
                console.log('Error BD, tabla país: ', e);
                return;
            });
            yield usuario_1.default.create({
                nombre_pais: pais.nombre_pais
            })
                .then((pais) => {
                console.log(pais);
            })
                .catch(e => console.log(e));
        });
    },
    eliminarPais(id) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    },
    actualizarPais(id) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
};
exports.default = ubicacionController;
