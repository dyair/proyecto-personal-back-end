"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const customError_1 = __importDefault(require("../config/customError"));
const domicilio_1 = require("../database/modelos/ubicaci\u00F3n/domicilio");
const sequelize_1 = require("sequelize");
const ambiente_1 = require("../global/ambiente");
const localidad_1 = require("../database/modelos/ubicaci\u00F3n/localidad");
const provincia_1 = require("../database/modelos/ubicaci\u00F3n/provincia");
const mascota_1 = require("../database/modelos/mascota/mascota");
const ubicacion_controller_1 = __importDefault(require("./ubicacion.controller"));
const relacion_1 = require("../database/modelos/mascota/relacion");
const mascota_resume_1 = require("../database/modelos/mascota/mascota_resume");
const post_1 = require("../database/modelos/mascota/post");
const comunicacion_1 = require("../database/modelos/mascota/comunicacion");
const mensaje_1 = require("../database/modelos/mascota/mensaje");
const busqueda_reciente_1 = require("../database/modelos/mascota/busqueda_reciente");
const cloud_file_1 = require("../config/middleware/file_management/cloud_file");
const include_sequelize_filters_1 = require("../database/include_sequelize_filters");
const mascotaController = {
    crearMascota(masc_param, id_user) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!masc_param.nombre_mascota || !masc_param.tipo_mascota_id || !masc_param.sexo_id || !masc_param.usuario_id || !masc_param.raza_id)
                throw new customError_1.default('Cuerpo de mascota incorrecto.', 404);
            let m = {};
            if (masc_param.file_img) {
                let data_uri = cloud_file_1.getDataURI(masc_param.file_img).content;
                masc_param.profile_pic = yield cloud_file_1.subirFileCloudinaryBuffer(data_uri);
            }
            else {
                masc_param.profile_pic = ambiente_1.imagen_perfil_defecto;
            }
            yield mascota_1.Mascota.findOrCreate({
                where: {
                    nombre_mascota: masc_param.nombre_mascota,
                    sexo_id: masc_param.sexo_id,
                    usuario_id: id_user,
                    raza_id: masc_param.raza_id,
                    tipo_mascota_id: masc_param.tipo_mascota_id
                },
                defaults: {
                    nombre_mascota: masc_param.nombre_mascota,
                    domicilio_id: masc_param.domicilio_id,
                    sexo_id: masc_param.sexo_id,
                    tamanio_id: masc_param.tamanio_id,
                    fecha_nac: masc_param.fecha_nac,
                    temperamento_id: masc_param.temperamento_id,
                    profile_pic: masc_param.profile_pic,
                    usuario_id: id_user,
                    raza_id: masc_param.raza_id,
                    tipo_mascota_id: masc_param.tipo_mascota_id
                }
            })
                .then(([mascotaDB, nuevo]) => {
                if (!nuevo)
                    throw new customError_1.default('Ya tienes una mascota con esos datos :)', 404);
                m = mascotaDB.get();
            }, ((error) => {
                throw new customError_1.default(error.message, 503);
            }));
            if (!m.id_mascota)
                throw new customError_1.default('ID de la mascota nulo luego de crearla.', 404);
            let mascota = {};
            mascota = yield this.getMascotaByID(m.id_mascota);
            return mascota;
        });
    },
    editarMascota(masc_) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!masc_.id_mascota)
                throw new customError_1.default('Mascota inexistente al tratar de editar.', 404);
            if (masc_.file_img) {
                let data_uri = cloud_file_1.getDataURI(masc_.file_img).content;
                masc_.profile_pic = yield cloud_file_1.subirFileCloudinaryBuffer(data_uri);
            }
            let atributos_a_editar = [];
            if (masc_.biografia)
                atributos_a_editar.push('biografia');
            if (masc_.fecha_nac)
                atributos_a_editar.push('fecha_nac');
            if (masc_.nombre_mascota)
                atributos_a_editar.push('nombre_mascota');
            if (masc_.domicilio_id)
                atributos_a_editar.push('domicilio_id');
            if (masc_.tamanio_id)
                atributos_a_editar.push('tamanio_id');
            if (masc_.file_img)
                atributos_a_editar.push('profile_pic'); // subio un archivo nuevo, cambio path imagen.
            if (masc_.temperamento_id)
                atributos_a_editar.push('temperamento_id');
            if (masc_.raza_id)
                atributos_a_editar.push('raza_id');
            yield mascota_1.Mascota.update(masc_, {
                where: { id_mascota: masc_.id_mascota },
                fields: atributos_a_editar
            })
                .then(([cantidadRowsUpdate, rdos]) => {
                console.log(cantidadRowsUpdate);
            })
                .catch((e) => {
                throw new customError_1.default(e.message, 503);
            });
            masc_ = yield this.getMascotaByID(masc_.id_mascota);
            return masc_;
        });
    },
    getRangoFecha(edad_desde, edad_hasta) {
        let fechas = {};
        let fecha_hoy = new Date();
        let mes = fecha_hoy.getMonth() + 1;
        let dia = fecha_hoy.getDate();
        fechas.desde = new Date(fecha_hoy.getFullYear() - edad_desde, mes, dia);
        fechas.hasta = new Date(fecha_hoy.getFullYear() - edad_hasta, mes, dia);
        return fechas;
    },
    calcularDistancia(lat1, long1, lat2, long2) {
        // Si buscan mascota por radio, calcula la distancia a ver si entra en ese radio de kms.
        if (!lat1 || !long1 || !lat2 || !long2)
            throw new customError_1.default('Alguna coordenada de dirección es nula.', 404);
        let R = 6371; // Radius of the earth in km
        let dLat = (Math.PI / 180) * (lat2 - lat1);
        let dLon = (Math.PI / 180) * (long2 - long1);
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos((Math.PI / 180) * (lat1)) * Math.cos((Math.PI / 180) * (lat2)) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c; // Distance in km
        return d;
    },
    getMascotaByID(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let mascota_ = {};
            yield mascota_1.Mascota.findByPk(id_mascota, {
                include: [...include_sequelize_filters_1.filtro_include_mascota, include_sequelize_filters_1.filtro_include_ubicacion_dom_to_country]
            })
                .then((mascota) => {
                if (!mascota)
                    throw new customError_1.default('Mascota inexistente', 404);
                mascota_ = this.llenarMascota(mascota);
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return mascota_;
        });
    },
    getMascotasByProvincia(filtros, id_prov) {
        return __awaiter(this, void 0, void 0, function* () {
            let mascotas_ = [];
            yield provincia_1.Provincia.findAll({
                where: {
                    id_provincia: id_prov
                },
                include: [{
                        association: localidad_1.prov_hm_loc,
                        as: 'localidades',
                        include: [{
                                association: domicilio_1.loc_hm_dom,
                                as: 'domicilios',
                                include: [{
                                        association: mascota_1.dom_hm_masc,
                                        as: 'mascotas',
                                        where: filtros,
                                        include: include_sequelize_filters_1.filtro_include_mascota
                                    }]
                            }]
                    },
                    {
                        association: provincia_1.prov_bt_pais,
                        as: 'pais'
                    }]
            })
                .then((rdo) => {
                let provincia_sql = {};
                let pais_sql = {};
                let localidad_sql = {};
                let domicilio_sql = {};
                for (let i = 0; i < rdo.length; i++) {
                    provincia_sql = rdo[i];
                    pais_sql = rdo[i].pais;
                    for (let x = 0; x < rdo[i].localidades.length; x++) {
                        localidad_sql = rdo[i].localidades[x];
                        for (let y = 0; y < rdo[i].localidades[x].domicilios.length; y++) {
                            domicilio_sql = rdo[i].localidades[x].domicilios[y];
                            for (let z = 0; z < rdo[i].localidades[x].domicilios[y].mascotas.length; z++) {
                                let mascota = this.llenarMascotaByUbication(rdo[i].localidades[x].domicilios[y].mascotas[z], domicilio_sql, localidad_sql, provincia_sql, pais_sql);
                                mascotas_.push(mascota);
                            }
                        }
                    }
                }
            }, (error) => {
                throw new customError_1.default(error.message, 503);
            });
            return mascotas_;
        });
    },
    getMascotasByLocalidad(id_loc, filtros) {
        return __awaiter(this, void 0, void 0, function* () {
            let mascotas_ = [];
            yield localidad_1.Localidad.findAll({
                where: {
                    id_localidad: id_loc
                },
                include: [{
                        association: domicilio_1.loc_hm_dom,
                        as: 'domicilios',
                        include: [{
                                association: mascota_1.dom_hm_masc,
                                as: 'mascotas',
                                where: filtros,
                                include: include_sequelize_filters_1.filtro_include_mascota
                            }]
                    },
                    {
                        association: localidad_1.loc_bt_prov,
                        as: 'provincia',
                        include: [{
                                association: provincia_1.prov_bt_pais,
                                as: 'pais'
                            }]
                    }]
            })
                .then((rdo) => {
                let provincia_sql = {};
                let pais_sql = {};
                let localidad_sql = {};
                let domicilio_sql = {};
                for (let i = 0; i < rdo.length; i++) {
                    provincia_sql = rdo[i].provincia;
                    pais_sql = rdo[i].provincia.pais;
                    localidad_sql = rdo[i];
                    for (let x = 0; x < rdo[i].domicilios.length; x++) {
                        domicilio_sql = rdo[i].domicilios[x];
                        for (let y = 0; y < rdo[i].domicilios[x].mascotas.length; y++) {
                            let mascota = this.llenarMascotaByUbication(rdo[i].domicilios[x].mascotas[y], domicilio_sql, localidad_sql, provincia_sql, pais_sql);
                            mascotas_.push(mascota);
                        }
                    }
                }
            }, (error) => {
                throw new customError_1.default(error.message, 503);
            });
            return mascotas_;
        });
    },
    // capaz haya que editarlo también, busca domicilios 2 veces.
    getMascotasByRadio(domicilio, radio, filtros) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!domicilio.localidad_id)
                throw new customError_1.default('Domicilio no asociado a una localidad.', 404);
            let prov = { pais_id: -1, nombre_provincia: '' };
            yield localidad_1.Localidad.findAll({
                where: {
                    id_localidad: domicilio.localidad_id
                },
                include: [{
                        association: localidad_1.loc_bt_prov,
                        as: 'provincia'
                    }]
            })
                .then((localidad) => {
                prov = localidad[0].provincia.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            if (!prov.id_provincia)
                throw new customError_1.default('Provincia inexistente (ID nulo).', 404);
            let mascotas_cercanas = [];
            yield provincia_1.Provincia.findOne({
                where: {
                    id_provincia: prov.id_provincia
                },
                include: [{
                        association: localidad_1.prov_hm_loc,
                        as: 'localidades',
                        include: [{
                                association: domicilio_1.loc_hm_dom,
                                as: 'domicilios',
                                where: {
                                    id_domicilio: {
                                        [sequelize_1.Op.ne]: domicilio.id_domicilio
                                    }
                                },
                                include: [{
                                        association: mascota_1.dom_hm_masc,
                                        as: 'mascotas',
                                        where: filtros,
                                        include: include_sequelize_filters_1.filtro_include_mascota
                                    }]
                            }]
                    }]
            })
                .then((provincia) => {
                for (let y = 0; y < provincia.localidades.length; y++) { //localidades
                    for (let z = 0; z < provincia.localidades[y].domicilios.length; z++) { //domicilios
                        let dom = provincia.localidades[y].domicilios[z].get();
                        if (!dom.longitud_map || !dom.latitud_map || !domicilio.latitud_map || !domicilio.longitud_map)
                            throw new customError_1.default('Algún domicilio sin coordenadas.', 404);
                        let distancia = this.calcularDistancia(domicilio.latitud_map, domicilio.longitud_map, dom.latitud_map, dom.longitud_map);
                        if (distancia <= radio) {
                            for (let x = 0; x < provincia.localidades[y].domicilios[z].mascotas.length; x++) { //mascotas de ese domicilio.
                                let masc = provincia.localidades[y].domicilios[z].mascotas[x];
                                mascotas_cercanas.push(masc);
                            }
                        }
                    }
                }
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return mascotas_cercanas;
        });
    },
    getMascotasByFiltros(filtros_query, id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let filtrosWhere = {
                id_mascota: {
                    [sequelize_1.Op.ne]: id_mascota // no buscar la mascota que hace la consulta.
                }
            };
            // Establecer los filtros que se hayan seleccionado desde el usuario.
            if (filtros_query.sexo)
                filtrosWhere.sexo_id = { [sequelize_1.Op.ne]: +filtros_query.sexo }; // para que no traiga el mismo sexo que la mascota que busca.
            if (filtros_query.tamanio)
                filtrosWhere.tamanio_id = +filtros_query.tamanio;
            if (filtros_query.temperamento)
                filtrosWhere.temperamento_id = +filtros_query.temperamento;
            if (filtros_query.raza)
                filtrosWhere.raza_id = +filtros_query.raza;
            if (filtros_query.nombre)
                filtrosWhere.nombre_mascota = filtros_query.nombre;
            if (filtros_query.edad_desde && filtros_query.edad_hasta) {
                let fechas = this.getRangoFecha(+filtros_query.edad_desde, +filtros_query.edad_hasta);
                filtrosWhere.fecha_nac = {
                    [sequelize_1.Op.lte]: fechas.desde,
                    [sequelize_1.Op.gte]: fechas.hasta // mayor o igual a hasta
                };
            }
            let mascotas = [];
            // Dependiendo si hay filtro por ubicación, entra a alguno if o sale por el último else para buscar mascotas.
            // Radio, prov, localidad y 'ningún filtro ubicación' son if excluyentes (si entra a uno al otro no).
            if (filtros_query.radio) {
                let mascota = yield this.getMascotaByID(id_mascota);
                let dom_mascota = {};
                if (mascota.domicilio)
                    dom_mascota = mascota.domicilio;
                if (!dom_mascota.id_domicilio)
                    throw new customError_1.default('Mascota con domicilio inexistente.', 404);
                let domicilio = yield ubicacion_controller_1.default.buscarDomicilio(dom_mascota.id_domicilio);
                mascotas = yield this.getMascotasByRadio(domicilio, +filtros_query.radio, filtrosWhere);
            }
            else if (filtros_query.localidad) {
                mascotas = yield this.getMascotasByLocalidad(+filtros_query.localidad, filtrosWhere);
            }
            else if (filtros_query.provincia) {
                mascotas = yield this.getMascotasByProvincia(filtrosWhere, +filtros_query.provincia);
            }
            else {
                yield mascota_1.Mascota.findAll({
                    where: filtrosWhere,
                    include: include_sequelize_filters_1.filtro_include_mascota
                })
                    .then((mascotasBD) => {
                    mascotasBD.forEach(mascota => {
                        let masc = this.llenarMascota(mascota);
                        mascotas.push(masc);
                    });
                }, (e) => {
                    throw new customError_1.default(e.message, 503);
                });
            }
            return mascotas;
        });
    },
    // útil para las notificaciones. No se usa más.
    getRelacionesNoVistasMascota(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let relaciones_ = [];
            yield relacion_1.Relacion.findAll({
                where: {
                    match: true,
                    [sequelize_1.Op.or]: [
                        {
                            [sequelize_1.Op.and]: [
                                { mascota_origen_id: id_mascota },
                                { visto_origen: false }
                            ]
                        },
                        {
                            [sequelize_1.Op.and]: [
                                { mascota_destino_id: id_mascota },
                                { visto_destino: false }
                            ]
                        }
                    ]
                },
                include: [
                    {
                        model: mascota_1.Mascota,
                        association: relacion_1.rel_bt_masco,
                        as: 'mascota_origen'
                    },
                    {
                        model: mascota_1.Mascota,
                        association: relacion_1.rel_bt_mascd,
                        as: 'mascota_destino'
                    }
                ]
            })
                .then((relaciones) => {
                relaciones.forEach(relacion => {
                    let rel = relacion.get();
                    rel.mascota_destino = relacion.get().mascota_destino.get();
                    rel.mascota_origen = relacion.get().mascota_origen.get();
                    relaciones_.push(rel);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return relaciones_;
        });
    },
    crearPost(post_) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!post_.mascota_id)
                throw new customError_1.default('Post inválido, lo crea una mascota no existente.', 404);
            let posteo = {
                descripcion: post_.descripcion,
                fecha_publicacion: post_.fecha_publicacion,
                mascota_id: post_.mascota_id
            };
            // Upload de las imagenes y set de las rutas de los archivos en cloudinary (si es que hay).
            if (post_.files_uploaded && post_.files_uploaded.length > 0) {
                let rutas_archivos = [];
                for (let i = 0; i < post_.files_uploaded.length; i++) {
                    let data_uri = cloud_file_1.getDataURI(post_.files_uploaded[i]).content;
                    let ruta_file_server = yield cloud_file_1.subirFileCloudinaryBuffer(data_uri);
                    rutas_archivos.push(ruta_file_server);
                }
                posteo.archivos_path = JSON.stringify(rutas_archivos);
            }
            yield post_1.Post_mascota.create(posteo)
                .then((rdo) => {
                post_ = rdo.get();
                // transformar el JSON de rutas en BD en un array de string para el front..
                if (post_.archivos_path) {
                    post_.archivos_array = JSON.parse(post_.archivos_path); // arreglo de string con rutas URL.
                    post_.archivos_path = undefined; // stringify que se usa en BD.
                }
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return post_;
        });
    },
    deletePost(id_post) {
        return __awaiter(this, void 0, void 0, function* () {
            // let archivos_path_borrar: string[] = [];
            // Busco post para poder borrar los files adjuntados si es que tiene.
            yield post_1.Post_mascota.findByPk(id_post, {
                raw: true
            })
                .then((post) => {
                if (!post)
                    throw new customError_1.default('Post sin ID al momento de querer eliminarlo', 404);
                // if (post.archivos_path) archivos_path_borrar = JSON.parse(post.archivos_path);
            }, (e) => {
                throw new customError_1.default(e.message + '. Error buscando las img del post para borrar.', 503);
            });
            yield post_1.Post_mascota.destroy({
                where: { id_post: id_post }
            })
                .then(() => {
                /*
                Usado para borrar los archivos almacenados en el servidor. No útil ahora.

                if (archivos_path_borrar.length > 0) {
                    for (const file_path of archivos_path_borrar) {
                        borrar_file_servidor(file_path);
                    }
                }
                */
            }, (e) => {
                throw new customError_1.default(e.message + '. Error borrando el post.', 503);
            });
            return id_post;
        });
    },
    getCantidadPostDeMascota(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let cantidad_posteos = 0;
            cantidad_posteos = yield post_1.Post_mascota.count({
                where: {
                    mascota_id: id_mascota
                }
            });
            return cantidad_posteos;
        });
    },
    getPosteos(id_mascota, fecha_desde) {
        return __awaiter(this, void 0, void 0, function* () {
            let posteos = [];
            yield post_1.Post_mascota.findAll({
                raw: true,
                order: [
                    ['fecha_publicacion', 'DESC']
                ],
                where: {
                    mascota_id: id_mascota,
                    fecha_publicacion: {
                        [sequelize_1.Op.lt]: fecha_desde
                    }
                },
                limit: 20
            })
                .then((posts) => {
                for (let i = 0; i < posts.length; i++) {
                    let post = posts[i];
                    // transformar el JSON de rutas en BD en un array.
                    if (post.archivos_path) {
                        post.archivos_array = JSON.parse(post.archivos_path);
                        post.archivos_path = undefined;
                    }
                    posteos.push(post);
                }
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return posteos;
        });
    },
    traerComunicacionDeMascotaByID(id_com) {
        return __awaiter(this, void 0, void 0, function* () {
            let comunicacion_ = {};
            yield comunicacion_1.Comunicacion.findOne({
                where: {
                    id_comunicacion: id_com
                },
                include: [{
                        association: comunicacion_1.mascd_bt_cab,
                        as: 'mascota_destino'
                    }, {
                        association: comunicacion_1.masco_bt_cab,
                        as: 'mascota_origen'
                    }]
            })
                .then((comunicaciones) => {
                let comunicacion = {};
                comunicacion.id_comunicacion = comunicaciones.get().id_comunicacion;
                comunicacion.mascota_origen = comunicaciones.get().mascota_origen.get();
                comunicacion.mascota_destino = comunicaciones.get().mascota_destino.get();
                comunicacion_ = comunicacion;
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return comunicacion_;
        });
    },
    traerComunicacionesDeMascota(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let comunicaciones_ = [];
            let id_comunicaciones = []; // para traer último mensaje (problemas con tipado: null | number).
            yield comunicacion_1.Comunicacion.findAll({
                where: {
                    [sequelize_1.Op.or]: [{
                            mascota_origen_id: id_mascota
                        }, {
                            mascota_destino_id: id_mascota
                        }]
                },
                include: [{
                        association: comunicacion_1.mascd_bt_cab,
                        as: 'mascota_destino',
                        include: include_sequelize_filters_1.filtro_include_mascota
                    },
                    {
                        association: comunicacion_1.masco_bt_cab,
                        as: 'mascota_origen',
                        include: include_sequelize_filters_1.filtro_include_mascota
                    }]
            })
                .then((comunicaciones) => {
                for (let i = 0; i < comunicaciones.length; i++) {
                    let comunicacion = {};
                    comunicacion.id_comunicacion = comunicaciones[i].get().id_comunicacion;
                    comunicacion.mascota_origen = this.llenarMascota(comunicaciones[i].get().mascota_origen);
                    comunicacion.mascota_destino = this.llenarMascota(comunicaciones[i].get().mascota_destino);
                    comunicaciones_.push(comunicacion);
                    id_comunicaciones.push(comunicaciones[i].get().id_comunicacion);
                }
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            for (let i = 0; i < comunicaciones_.length; i++) {
                yield mensaje_1.Mensaje.findAll({
                    where: {
                        cabecera_sms_id: id_comunicaciones[i]
                    },
                    limit: 5,
                    order: [['fecha', 'DESC']]
                })
                    .then((mensajes) => {
                    let messages = [];
                    mensajes.forEach(sms => {
                        messages.push(sms.get());
                    });
                    comunicaciones_[i].mensajes = messages;
                }, (e) => {
                    throw new customError_1.default(e.message, 503);
                });
            }
            return comunicaciones_.reverse();
        });
    },
    crearMensaje(sms) {
        return __awaiter(this, void 0, void 0, function* () {
            // Traer desde el front el visto_destino: false.
            let sms_ = {};
            yield mensaje_1.Mensaje.create(sms)
                .then((mensaje) => {
                sms_ = mensaje.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            if (!sms_.mascota_origen_id || !sms_.mascota_destino_id)
                throw new customError_1.default('Mensaje con problemas de origen y destino. CrearMensaje()', 404);
            sms_.mascota_destino = yield this.getMascotaByID(sms_.mascota_destino_id);
            sms_.mascota_origen = yield this.getMascotaByID(sms_.mascota_origen_id);
            return sms_;
        });
    },
    getMensajesComunicacion(id_comunicacion, fecha_desde) {
        return __awaiter(this, void 0, void 0, function* () {
            let mensajes_ = [];
            let cant_sms_a_traer = 10;
            yield mensaje_1.Mensaje.findAll({
                where: {
                    [sequelize_1.Op.and]: [
                        { cabecera_sms_id: id_comunicacion },
                        {
                            fecha: {
                                [sequelize_1.Op.lt]: fecha_desde
                            }
                        }
                    ]
                },
                limit: cant_sms_a_traer,
                order: [['id_mensaje', 'DESC']]
            })
                .then((mensajes) => {
                mensajes.forEach(sms => {
                    mensajes_.push(sms.get());
                });
                mensajes_ = mensajes_.reverse();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return mensajes_;
        });
    },
    // útil para las notificaciones.
    getMensajesNoVistosComunicacion(id_pet) {
        return __awaiter(this, void 0, void 0, function* () {
            let comunicaciones_ = [];
            yield comunicacion_1.Comunicacion.findAll({
                where: {
                    [sequelize_1.Op.or]: [
                        { mascota_origen_id: id_pet },
                        { mascota_destino_id: id_pet }
                    ]
                },
                include: [
                    {
                        model: mensaje_1.Mensaje,
                        as: 'mensajes',
                        where: {
                            visto_destino: false,
                            mascota_destino_id: id_pet
                        },
                        include: [
                            {
                                association: comunicacion_1.mascd_bt_cab,
                                as: 'mascota_destino'
                            },
                            {
                                association: comunicacion_1.masco_bt_cab,
                                as: 'mascota_origen'
                            }
                        ]
                    }
                ]
            })
                .then((comunicaciones) => {
                comunicaciones.forEach(comu => {
                    const com = comu;
                    const mensajes = com.get().mensajes;
                    let messages = [];
                    for (let i = 0; i < mensajes.length; i++) {
                        let sms;
                        sms = mensajes[i].get();
                        sms.mascota_destino = mensajes[i].get().mascota_destino.get();
                        sms.mascota_origen = mensajes[i].get().mascota_origen.get();
                        messages.push(sms);
                    }
                    let comunicacion = com.get();
                    comunicacion.mensajes = messages;
                    comunicaciones_.push(comunicacion);
                });
            })
                .catch((e) => {
                throw new customError_1.default(e.message, 503);
            });
            return comunicaciones_;
        });
    },
    setMensajeVisto(mensajes_vistos_id) {
        return __awaiter(this, void 0, void 0, function* () {
            yield Promise.all(mensajes_vistos_id.map((sms_id) => __awaiter(this, void 0, void 0, function* () {
                let sms = { visto_destino: true };
                yield mensaje_1.Mensaje.update(sms, {
                    where: {
                        id_mensaje: sms_id
                    },
                    fields: ['visto_destino']
                })
                    .then(([cantidad, rdo]) => {
                    console.log('cantidad sms vistos: ', cantidad);
                }, (e) => {
                    throw new customError_1.default(e.message, 503);
                });
            })))
                .then(() => {
                console.log('Todos los mensajes vistos, fin de promise all.');
            })
                .catch((e) => {
                throw new customError_1.default(e.message, 503);
            });
        });
    },
    setPreferenciaBusquedaMascota(id_mascota_buscadora, id_mascota_buscada) {
        return __awaiter(this, void 0, void 0, function* () {
            yield busqueda_reciente_1.Busqueda_reciente.findOrCreate({
                where: {
                    mascota_buscadora_id: id_mascota_buscadora,
                    mascota_buscada_id: id_mascota_buscada
                },
                defaults: {
                    mascota_buscadora_id: id_mascota_buscadora,
                    mascota_buscada_id: id_mascota_buscada,
                    fecha_busqueda: new Date()
                }
            })
                .then(([busqueda_reciente, nuevo]) => {
                console.log("nuevo: ", nuevo);
                console.log(busqueda_reciente.get());
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
        });
    },
    getBusquedasRecientes(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let mascotas_buscadas_ = [];
            yield busqueda_reciente_1.Busqueda_reciente.findAll({
                where: {
                    mascota_buscadora_id: id_mascota
                },
                order: [
                    ['fecha_busqueda', 'DESC']
                ],
                limit: 10
            })
                .then((rdo_busquedas) => {
                rdo_busquedas.get().busquedas.forEach((busqueda) => {
                    mascotas_buscadas_.push(busqueda.get());
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return mascotas_buscadas_;
        });
    },
    llenarMascota(mascota) {
        // Podría dejarlo --> usuario: mascota.get().usuario.get(); pero prefiero con todo el detalle.
        let mascota_ = {
            fecha_nac: mascota.get().fecha_nac,
            id_mascota: mascota.get().id_mascota,
            biografia: mascota.get().biografia,
            usuario: {
                nombre: mascota.get().usuario.get().nombre,
                email: mascota.get().usuario.get().email,
                id: mascota.get().usuario.get().id,
                password: mascota.get().usuario.get().password
            },
            raza: {
                nombre_raza: mascota.get().raza.get().nombre_raza,
                id_raza: mascota.get().raza.get().id_raza
            },
            tipo: {
                nombre_tipo_mascota: mascota.get().tipo.get().nombre_tipo_mascota,
                id_tipo_mascota: mascota.get().tipo.get().id_tipo_mascota
            },
            temperamento: {
                nombre_temperamento: mascota.get().temperamento.get().nombre_temperamento,
                id_temperamento: mascota.get().temperamento.get().id_temperamento
            },
            tamanio: {
                nombre_tamanio: mascota.get().tamanio.get().nombre_tamanio,
                id_tamanio: mascota.get().tamanio.get().id_tamanio
            },
            sexo: {
                nombre_sexo: mascota.get().sexo.get().nombre_sexo,
                id_sexo: mascota.get().sexo.get().id_sexo
            },
            domicilio: {
                id_domicilio: mascota.get().domicilio.get().id_domicilio,
                calle: mascota.get().domicilio.get().calle,
                numero: mascota.get().domicilio.get().numero,
                piso: mascota.get().domicilio.get().piso,
                depto: mascota.get().domicilio.get().depto,
                latitud_map: mascota.get().domicilio.get().latitud_map,
                longitud_map: mascota.get().domicilio.get().longitud_map,
                localidad_id: mascota.get().domicilio.get().localidad_id
            },
            localidad: {
                nombre_localidad: mascota.get().domicilio.get().localidad.get().nombre_localidad,
                id_localidad: mascota.get().domicilio.get().localidad.get().id_localidad,
                provincia_id: mascota.get().domicilio.get().localidad.get().provincia_id
            },
            provincia: {
                id_provincia: mascota.get().domicilio.get().localidad.get().provincia.get().id_provincia,
                nombre_provincia: mascota.get().domicilio.get().localidad.get().provincia.get().nombre_provincia,
                pais_id: mascota.get().domicilio.get().localidad.get().provincia.get().pais_id,
            },
            pais: {
                id_pais: mascota.get().domicilio.get().localidad.get().provincia.get().pais.get().id_pais,
                nombre_pais: mascota.get().domicilio.get().localidad.get().provincia.get().pais.get().nombre_pais
            },
            profile_pic: mascota.get().profile_pic,
            nombre_mascota: mascota.get().nombre_mascota
        };
        return mascota_;
    },
    llenarMascotaByUbication(mascota, mascota_dom, mascota_loc, mascota_prov, mascota_pais) {
        let mascota_ = {
            fecha_nac: mascota.get().fecha_nac,
            id_mascota: mascota.get().id_mascota,
            biografia: mascota.get().biografia,
            usuario: {
                nombre: mascota.get().usuario.get().nombre,
                email: mascota.get().usuario.get().email,
                id: mascota.get().usuario.get().id,
                password: mascota.get().usuario.get().password
            },
            raza: {
                nombre_raza: mascota.get().raza.get().nombre_raza,
                id_raza: mascota.get().raza.get().id_raza
            },
            tipo: {
                nombre_tipo_mascota: mascota.get().tipo.get().nombre_tipo_mascota,
                id_tipo_mascota: mascota.get().tipo.get().id_tipo_mascota
            },
            temperamento: {
                nombre_temperamento: mascota.get().temperamento.get().nombre_temperamento,
                id_temperamento: mascota.get().temperamento.get().id_temperamento
            },
            tamanio: {
                nombre_tamanio: mascota.get().tamanio.get().nombre_tamanio,
                id_tamanio: mascota.get().tamanio.get().id_tamanio
            },
            sexo: {
                nombre_sexo: mascota.get().sexo.get().nombre_sexo,
                id_sexo: mascota.get().sexo.get().id_sexo
            },
            domicilio: {
                id_domicilio: mascota_dom.get().id_domicilio,
                calle: mascota_dom.get().calle,
                numero: mascota_dom.get().numero,
                piso: mascota_dom.get().piso,
                depto: mascota_dom.get().depto,
                latitud_map: mascota_dom.get().latitud_map,
                longitud_map: mascota_dom.get().longitud_map,
                localidad_id: mascota_dom.get().localidad_id
            },
            localidad: {
                nombre_localidad: mascota_loc.get().nombre_localidad,
                id_localidad: mascota_loc.get().id_localidad,
                provincia_id: mascota_loc.get().provincia_id
            },
            provincia: {
                id_provincia: mascota_prov.get().id_provincia,
                nombre_provincia: mascota_prov.get().nombre_provincia,
                pais_id: mascota_prov.get().pais_id,
            },
            pais: {
                id_pais: mascota_pais.get().id_pais,
                nombre_pais: mascota_pais.get().nombre_pais
            },
            profile_pic: mascota.get().profile_pic,
            nombre_mascota: mascota.get().nombre_mascota
        };
        return mascota_;
    },
    /*----------------------------------------------------------------------------------------------------------------------------------------------------
                                        INICIO DE LISTADO DE METODOS QUE QUEDARON SIN USO.
    ----------------------------------------------------------------------------------------------------------------------------------------------------*/
    // Con cloudinary queda obsoleto, ahora no guarda en el servidor. Util cuando guardaba en el servidor.
    editarPathImagenes(array_path) {
        // cortar path desde la posición 60 de la imagen y arranque desde /uploads. El string adjuntado varía dependiendo dónde estén las imágenes (en este caso, localhost).
        if (array_path) {
            array_path = `http://${ambiente_1.IP_SERVER_IMG}:${ambiente_1.SERVER_PORT}` + array_path.substring(array_path.indexOf('/uploads/'));
        }
        return array_path;
    },
    // Las comunicaciones se generan una vez que hubo match entre las mascotas. Ya no es así.
    crearComunicacion(com_sms) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!com_sms.mascota_destino_id || !com_sms.mascota_origen_id)
                throw new customError_1.default('Error al recibir solicitud de comunicación.', 404);
            if (com_sms.mascota_destino_id === com_sms.mascota_origen_id)
                throw new customError_1.default('No tiene sentido la comunicación con uno mismo :).', 404);
            let comunicacion = {};
            // No permitir crear conexiones que ya existen, de un lado o del otro.
            yield comunicacion_1.Comunicacion.findOrCreate({
                where: {
                    [sequelize_1.Op.or]: [
                        {
                            [sequelize_1.Op.and]: [
                                { mascota_origen_id: com_sms.mascota_origen_id },
                                { mascota_destino_id: com_sms.mascota_destino_id }
                            ]
                        },
                        {
                            [sequelize_1.Op.and]: [
                                { mascota_origen_id: com_sms.mascota_destino_id },
                                { mascota_destino_id: com_sms.mascota_origen_id }
                            ]
                        }
                    ]
                },
                defaults: {
                    mascota_origen_id: com_sms.mascota_origen_id,
                    mascota_destino_id: com_sms.mascota_destino_id
                }
            })
                .then(([com, nuevo]) => {
                comunicacion = com.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return comunicacion;
        });
    },
    // no se usa más.
    editarRelacion(rel, datos_o, datos_d) {
        return __awaiter(this, void 0, void 0, function* () {
            // Edita el like, match y visto del destino/origen. 
            // if (rel.match) return rel;  // relación ya matcheada. No se puede editar.
            let relacion_ = rel;
            let datos_origen;
            let datos_destino;
            if (relacion_.mascota_origen_id === datos_o.id_mascota) {
                datos_origen = datos_o;
                datos_destino = datos_d;
            }
            else {
                datos_origen = datos_d;
                datos_destino = datos_o;
            }
            relacion_.visto_destino = datos_destino.visto;
            relacion_.visto_origen = datos_origen.visto;
            relacion_.like_destino = datos_destino.like;
            relacion_.like_origen = datos_origen.like;
            if (relacion_.like_destino && relacion_.like_origen)
                relacion_.match = true;
            else
                relacion_.match = false;
            if (!relacion_.id_relacion)
                throw new customError_1.default('Relación inexistente en editarRelacion()', 404);
            relacion_1.Relacion.update(relacion_, {
                where: {
                    id_relacion: relacion_.id_relacion
                },
                fields: ['visto_origen', 'visto_destino', 'like_origen', 'like_destino', 'match']
            })
                .then(([rowsUpdated, rel]) => __awaiter(this, void 0, void 0, function* () {
                if (relacion_.like_destino && relacion_.like_origen) {
                    let com = {
                        mascota_origen_id: relacion_.mascota_origen_id,
                        mascota_destino_id: relacion_.mascota_destino_id
                    };
                    let comunicacion = yield this.crearComunicacion(com);
                    console.log('Match, comunicación creada => ', comunicacion);
                }
            }), (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return relacion_;
        });
    },
    // no se usa.
    crearResumenFotosMascota(id_mascota, fotos) {
        return __awaiter(this, void 0, void 0, function* () {
            let fotos_mascotas = [];
            let resumen = {};
            for (let i = 0; i < fotos.length; i++) {
                if (fotos[i].path)
                    fotos_mascotas.push(fotos[i].path);
            }
            yield mascota_resume_1.Mascota_resumen.findOrCreate({
                where: {
                    mascota_id: id_mascota
                },
                defaults: {
                    foto_mascota1: fotos_mascotas[0],
                    foto_mascota2: fotos_mascotas[1],
                    foto_mascota3: fotos_mascotas[2]
                }
            })
                .then(([mascotaresumenDB, nuevo]) => {
                resumen = mascotaresumenDB.get();
                console.log(nuevo);
            }, ((error) => {
                throw new customError_1.default(error.message, 503);
            }));
            return resumen;
        });
    },
    // no se usa.
    getResumenFotosMascota(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let resumen = {};
            yield mascota_resume_1.Mascota_resumen.findOne({
                where: {
                    mascota_id: id_mascota
                }
            })
                .then((resumen) => {
                if (resumen == null)
                    throw new customError_1.default('No hay un resumen para esta mascota', 404);
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return resumen;
        });
    },
    // no se usa.
    editarResumenFotosMascota(resumen) {
        return __awaiter(this, void 0, void 0, function* () {
            if (resumen.id_mascota_resumen == null)
                throw new customError_1.default('Resumen inválido. ID nulo, incoherente.', 404);
            if (!resumen.mascota_fotos)
                throw new customError_1.default('Error: editarResumenFotosMascota.', 404);
            let fotos_path = resumen.mascota_fotos;
            let resumen_ = {
                mascota_id: resumen.mascota_id,
                foto_mascota1: '',
                foto_mascota2: '',
                foto_mascota3: ''
            };
            if (fotos_path.length > 0) {
                for (let i = 0; i < fotos_path.length; i++) {
                    if (resumen_.foto_mascota1 === '') {
                        resumen_.foto_mascota1 = fotos_path[i];
                    }
                    else if (resumen_.foto_mascota2 === '') {
                        resumen_.foto_mascota2 = fotos_path[i];
                    }
                    else if (resumen_.foto_mascota3 === '') {
                        resumen_.foto_mascota3 = fotos_path[i];
                    }
                }
            }
            else {
                throw new customError_1.default('Se necesita al menos una foto cargada.', 404);
            }
            yield mascota_resume_1.Mascota_resumen.update(resumen_, {
                where: { id_mascota_resumen: resumen.id_mascota_resumen }
            })
                .then(([cantidadRowsUpdate, mascotaresumenDB]) => {
                console.log('cantidad: ', cantidadRowsUpdate);
                console.log('mascota: ', mascotaresumenDB);
            }, ((error) => {
                throw new customError_1.default(error.message, 503);
            }));
            return resumen_;
        });
    },
    // No se usa más.
    crearRelacion(origen, destino) {
        return __awaiter(this, void 0, void 0, function* () {
            let relacion = {};
            yield relacion_1.Relacion.findOrCreate({
                where: {
                    [sequelize_1.Op.or]: [
                        {
                            mascota_origen_id: origen.id_mascota,
                            mascota_destino_id: destino.id_mascota
                        },
                        {
                            mascota_origen_id: destino.id_mascota,
                            mascota_destino_id: origen.id_mascota
                        }
                    ]
                },
                defaults: {
                    fecha: new Date(),
                    match: false,
                    like_origen: origen.like,
                    like_destino: destino.like,
                    visto_origen: origen.visto,
                    visto_destino: origen.visto,
                    mascota_origen_id: origen.id_mascota,
                    mascota_destino_id: destino.id_mascota
                }
            })
                .then(([rel, nuevo]) => __awaiter(this, void 0, void 0, function* () {
                if (!nuevo) {
                    let r = rel.get();
                    relacion = yield this.editarRelacion(r, origen, destino);
                }
                else {
                    relacion = rel.get();
                }
            }));
            return relacion;
        });
    },
    // trae las relaciones que disparó la mascota, no en las que fue destino.
    getRelacionesMascota(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let relaciones_ = [];
            yield relacion_1.Relacion.findAll({
                where: {
                    mascota_origen_id: id_mascota
                }
            })
                .then((relaciones) => {
                relaciones.forEach(relacion => {
                    relaciones_.push(relacion.get());
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return relaciones_;
        });
    },
};
exports.default = mascotaController;
