"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const pais_1 = require("../database/modelos/ubicaci\u00F3n/pais");
const customError_1 = __importDefault(require("../config/customError"));
const provincia_1 = require("../database/modelos/ubicaci\u00F3n/provincia");
const localidad_1 = require("../database/modelos/ubicaci\u00F3n/localidad");
const raza_1 = require("../database/modelos/mascota/raza");
const tamanio_1 = __importDefault(require("../database/modelos/mascota/tamanio"));
const temperamento_1 = __importDefault(require("../database/modelos/mascota/temperamento"));
const sexo_1 = __importDefault(require("../database/modelos/mascota/sexo"));
const domicilio_1 = require("../database/modelos/ubicaci\u00F3n/domicilio");
const comunicacion_1 = require("../database/modelos/mascota/comunicacion");
const tipo_mascota_1 = require("../database/modelos/mascota/tipo_mascota");
const ambiente_1 = require("../global/ambiente");
const request_API_1 = __importDefault(require("../config/request_API"));
const configController = {
    getPais() {
        return __awaiter(this, void 0, void 0, function* () {
            let paises_ = [];
            yield pais_1.Pais.findAll({
                raw: true
            })
                .then((paises) => {
                paises.forEach(pais => {
                    paises_.push(pais);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return paises_;
        });
    },
    getProvinciaByIDPais(id_pais) {
        return __awaiter(this, void 0, void 0, function* () {
            let provincias_ = [];
            yield provincia_1.Provincia.findAll({
                raw: true,
                where: {
                    pais_id: id_pais
                }
            })
                .then((provincias) => {
                provincias.forEach(provincia => {
                    provincias_.push(provincia);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return provincias_;
        });
    },
    getLocalidadByIDProv(id_prov) {
        return __awaiter(this, void 0, void 0, function* () {
            let localidades_ = [];
            yield localidad_1.Localidad.findAll({
                raw: true,
                where: {
                    provincia_id: id_prov
                }
            })
                .then((localidades) => {
                localidades.forEach(localidad => {
                    localidades_.push(localidad);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return localidades_;
        });
    },
    getRaza() {
        return __awaiter(this, void 0, void 0, function* () {
            let razas_ = [];
            yield raza_1.Raza.findAll({
                raw: true
            })
                .then((razas) => {
                razas.forEach(raza => {
                    razas_.push(raza);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return razas_;
        });
    },
    getRazaByID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let raza_ = {};
            yield raza_1.Raza.findOne({
                raw: true,
                where: {
                    id_raza: id
                }
            })
                .then((raza) => {
                if (!raza)
                    throw new customError_1.default("Raza inexistente.", 404);
                raza_ = raza.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return raza_;
        });
    },
    getTamanio() {
        return __awaiter(this, void 0, void 0, function* () {
            let tamanios_ = [];
            yield tamanio_1.default.findAll({
                raw: true
            })
                .then((tamanios) => {
                tamanios.forEach(tamanio => {
                    tamanios_.push(tamanio);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return tamanios_;
        });
    },
    getTamanioByID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let tamanio_ = {};
            yield tamanio_1.default.findOne({
                raw: true,
                where: {
                    id_tamanio: id
                }
            })
                .then((tamanio) => {
                if (!tamanio)
                    throw new customError_1.default("Tamanio inexistente.", 404);
                tamanio_ = tamanio.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return tamanio_;
        });
    },
    getTemperamento() {
        return __awaiter(this, void 0, void 0, function* () {
            let temperamentos_ = [];
            yield temperamento_1.default.findAll({
                raw: true
            })
                .then((temperamentos) => {
                temperamentos.forEach(temperamento => {
                    temperamentos_.push(temperamento);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return temperamentos_;
        });
    },
    getTemperamentoByID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let temperamento_ = {};
            yield temperamento_1.default.findByPk(id)
                .then((temperamento) => {
                if (!temperamento)
                    throw new customError_1.default("Temperamento inexistente.", 404);
                temperamento_ = temperamento.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return temperamento_;
        });
    },
    getTipoMascota() {
        return __awaiter(this, void 0, void 0, function* () {
            let tipos_mascotas_ = [];
            yield tipo_mascota_1.Tipo_mascota.findAll({
                raw: true
            })
                .then((tipos) => {
                tipos.forEach(tipo_mascota => {
                    tipos_mascotas_.push(tipo_mascota);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return tipos_mascotas_;
        });
    },
    getSexo() {
        return __awaiter(this, void 0, void 0, function* () {
            let sexos_ = [];
            yield sexo_1.default.findAll({
                raw: true
            })
                .then((sexos) => {
                sexos.forEach(sexo => {
                    sexos_.push(sexo);
                });
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return sexos_;
        });
    },
    getSexoByID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            let sexo_ = {};
            yield sexo_1.default.findByPk(id)
                .then((sexo) => {
                if (!sexo)
                    throw new customError_1.default("Sexo inexistente.", 404);
                sexo_ = sexo.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return sexo_;
        });
    },
    crearDomicilio(domicilio) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!domicilio.calle || !domicilio.localidad_id || !domicilio.numero)
                throw new customError_1.default('Cuerpo de domicilio incorrecto, valores vacío no válidos.', 404);
            let dom = {};
            let coordenadas = { latitud: -200, longitud: -200 };
            let loc = '', prov = '', pais = '';
            yield localidad_1.Localidad.findOne({
                where: {
                    id_localidad: domicilio.localidad_id
                },
                include: [{
                        association: localidad_1.loc_bt_prov,
                        as: 'provincia',
                        include: [{
                                association: provincia_1.prov_bt_pais,
                                as: 'pais'
                            }]
                    }]
            })
                .then((localidad) => {
                if (!localidad)
                    throw new customError_1.default('No existe localidad asociada a ese domicilio', 404);
                loc = localidad.get().nombre_localidad;
                prov = localidad.get().provincia.get().nombre_provincia;
                pais = localidad.get().provincia.get().pais.get().nombre_pais;
            }, (error) => {
                throw new customError_1.default(error.message, 503);
            });
            coordenadas = yield this.getCoordinatesByAddress(domicilio.calle + ' ' + domicilio.numero + ', ' + loc + ', ' + prov + ', ' + pais);
            // filtro para crear bien los domicilios respetando los pisos y departamentos.
            let clausulaSQL = {};
            if (domicilio.calle)
                clausulaSQL.calle = domicilio.calle;
            if (domicilio.numero)
                clausulaSQL.numero = domicilio.numero;
            if (domicilio.localidad_id)
                clausulaSQL.localidad_id = domicilio.localidad_id;
            if (domicilio.piso)
                clausulaSQL.piso = domicilio.piso;
            if (domicilio.depto)
                clausulaSQL.depto = domicilio.depto;
            clausulaSQL.latitud_map = coordenadas.latitud;
            clausulaSQL.longitud_map = coordenadas.longitud;
            yield domicilio_1.Domicilio.findOrCreate({
                where: clausulaSQL,
                defaults: clausulaSQL
            })
                .then(([domicilioBD, nuevo]) => {
                dom = domicilioBD.get();
                console.log('domicilio nuevo?: ', nuevo);
            }, ((error) => {
                throw new customError_1.default(error.message, 503);
            }));
            return dom;
        });
    },
    editarDomicilioMascota(domicilio) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!domicilio.id_domicilio)
                throw new customError_1.default('Domicilio inexistente.', 404);
            let atributos_a_editar = [];
            let coordenadas;
            let loc = '';
            let prov = '';
            let pais = '';
            yield localidad_1.Localidad.findByPk(domicilio.localidad_id, {
                include: [{
                        association: localidad_1.loc_bt_prov,
                        as: 'provincia',
                        include: [{
                                association: provincia_1.prov_bt_pais,
                                as: 'pais'
                            }]
                    }]
            })
                .then((localidad) => {
                if (!localidad)
                    throw new customError_1.default('No hay localidad con ese ID, editarDomicilioMascota()', 404);
                loc = localidad.get().nombre_localidad;
                prov = localidad.get().provincia.get().nombre_provincia;
                pais = localidad.get().provincia.get().pais.get().nombre_pais;
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            // debería estar en configuración controller.
            coordenadas = yield configController.getCoordinatesByAddress(domicilio.calle + ' ' + domicilio.numero + ', ' + loc + ', ' + prov + ', ' + pais);
            let dom;
            if (domicilio.calle) {
                dom.calle = domicilio.calle;
                atributos_a_editar.push('calle');
            }
            if (domicilio.numero) {
                dom.numero = domicilio.numero;
                atributos_a_editar.push('numero');
            }
            if (domicilio.piso) {
                dom.piso = domicilio.piso;
                atributos_a_editar.push('piso');
            }
            if (domicilio.depto) {
                dom.depto = domicilio.depto;
                atributos_a_editar.push('depto');
            }
            if (domicilio.localidad_id) {
                dom.localidad_id = domicilio.localidad_id;
                atributos_a_editar.push('localidad_id');
            }
            dom.latitud_map = coordenadas.latitud;
            dom.longitud_map = coordenadas.longitud;
            atributos_a_editar.push('latitud_map');
            atributos_a_editar.push('longitud_map');
            yield domicilio_1.Domicilio.update(dom, {
                where: {
                    domicilio_id: domicilio.id_domicilio
                },
                fields: atributos_a_editar
            })
                .then(([cantidad, rdo]) => {
                console.log('domicilio actualizado.', cantidad, rdo);
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return domicilio;
        });
    },
    getComunicacionByID(id_mascota) {
        return __awaiter(this, void 0, void 0, function* () {
            let com = {};
            yield comunicacion_1.Comunicacion.findByPk(id_mascota, {
                include: [{
                        association: comunicacion_1.mascd_bt_cab,
                        as: 'mascota_destino'
                    }, {
                        association: comunicacion_1.masco_bt_cab,
                        as: 'mascota_origen'
                    }]
            })
                .then((comunicacion) => {
                if (!comunicacion)
                    throw new customError_1.default('Comunicación inexistente.', 404);
                comunicacion.mascota_origen = comunicacion.get().mascota_origen.get();
                comunicacion.mascota_destino = comunicacion.get().mascota_destino.get();
                com = comunicacion.get();
            }, (e) => {
                throw new customError_1.default(e.message, 503);
            });
            return com;
        });
    },
    getCoordinatesByAddress(direccion) {
        return __awaiter(this, void 0, void 0, function* () {
            let coordenadas = { latitud: 200, longitud: 200 };
            const api_geocoding = 'http://www.mapquestapi.com/geocoding/v1/address?key=' + ambiente_1.key_apigeocoding + '&location=' + direccion;
            const rdo = yield request_API_1.default.getMapquest(api_geocoding);
            coordenadas.latitud = rdo.latLng.lat;
            coordenadas.longitud = rdo.latLng.lng;
            return coordenadas;
        });
    },
};
exports.default = configController;
