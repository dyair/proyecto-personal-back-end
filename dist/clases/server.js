"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const ambiente_1 = require("../global/ambiente");
class Server {
    constructor() {
        this.app = express_1.default();
        this.puerto = ambiente_1.SERVER_PORT;
    }
    start(calllback) {
        this.app.listen(this.puerto, calllback);
    }
}
exports.default = Server;
