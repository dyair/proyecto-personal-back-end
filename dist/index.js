"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = __importDefault(require("./config/middleware/server"));
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const bodyParser = require("body-parser");
const cors_1 = __importDefault(require("cors"));
const rutas_inicio_1 = __importDefault(require("./rutas/rutas.inicio"));
const rutas_mascota_1 = __importDefault(require("./rutas/rutas.mascota"));
const rutas_usuario_1 = __importDefault(require("./rutas/rutas.usuario"));
const rutas_config_1 = __importDefault(require("./rutas/rutas.config"));
const server = server_1.default.getInstance();
//Configuración para capturar parametros en llamadas API.
server.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
server.app.use(bodyParser.json({ limit: '50mb' }));
//Permitir que las consultas sean desde lugares distintos al host del server
server.app.use(cors_1.default({ origin: true, credentials: true }));
//Para que se puedan acceder a los archivos del server desde el front. YA NO SE UTILIZA.
server.app.use('/uploads', express_1.default.static(path_1.default.join(__dirname, '..', 'uploads')));
//Rutas de servicios
server.app.use('/inicio', rutas_inicio_1.default);
server.app.use('/usuario', rutas_usuario_1.default);
server.app.use('/mascota', rutas_mascota_1.default);
server.app.use('/configuracion', rutas_config_1.default);
server.start(function () {
    // heroku git:remote -a mascota-app
    // YA EJECUTÉ ESE COMANDO EN ESTE REPOSITORIO (BackEnd/).
});
// mascotaController.crearMensaje({
//     cabecera_sms_id: 2,
//     contenido: 'Probando idssddasd',
//     fecha: new Date(),
//     mascota_destino_id: 13,
//     mascota_origen_id: 12,
// })
//     .then(rdo => console.log(rdo)); 
// Mascota.create({
//     nombre_mascota: 'Lupo',
//     raza_id: 1,
//     temperamento_id: 1,
//     tamanio_id: 1,
//     fecha_nac: new Date(),
//     domicilio_id: 2,
//     usuario_id: 9,
//     sexo_id: 1 
// })
// .then(()=> {
//     console.log('creado');
// })
