"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Port configuration
exports.SERVER_PORT = (process.env.PORT) || JSON.stringify(5000);
// Credenciales para BD postgresSQL en la nube (Postgres-heroku).
exports.host_BD = 'ec2-54-174-229-152.compute-1.amazonaws.com';
exports.schema_BD = 'd2mg97tfbv5jjj';
exports.user_BD = 'wglarypesrwbsa';
exports.pw_BD = '544fe9f0cabb7bc780f72529d0fbe0042b0300d6f180ca8c3cdaae3bc5fdedb8';
exports.port_BD = 5432;
// Útil cuando guardaba imagenes en el servidor, ya no más.
exports.IP_SERVER_IMG = 'localhost';
// API Mapquest configuration
exports.key_apigeocoding = 'HmyB1BHhuYA5qptss8JYFJBfFVAhu2pA';
exports.ok_status = 200;
// Google sign-in credentials
exports.CLIENT_ID = '360447745298-q40v2l62qhv4dek16a3bti074fii6lnb.apps.googleusercontent.com';
exports.CLIENT_ID_SECRET = '5y4kacVdrqq8yprcO9zebOCt';
// Token Auth0.
exports.clave_codificacion = 'clav3_s3cr3t4';
// Si no suben imagen de perfil de la mascota, usamos esta por defecto.
exports.imagen_perfil_defecto = 'https://image.flaticon.com/icons/png/512/194/194177.png';
