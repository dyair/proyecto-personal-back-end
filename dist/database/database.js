"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const ambiente_1 = require("../global/ambiente");
exports.sequelize = (process.env.PORT)
    ? new sequelize_1.Sequelize(`postgres://${ambiente_1.user_BD}:${ambiente_1.pw_BD}@${ambiente_1.host_BD}:${ambiente_1.port_BD}/${ambiente_1.schema_BD}`)
    : exports.sequelize = new sequelize_1.Sequelize('mascota_app', 'yair_mascotas', 'eriksen', {
        host: 'localhost',
        dialect: 'mariadb'
    });
exports.testDB = exports.sequelize.authenticate()
    .then(() => {
    // heroku pg:psql
    // sequelize.sync();
})
    .catch((err) => {
    console.error('Error database connection: ', err.message);
});
