"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mascota_1 = require("./modelos/mascota/mascota");
const domicilio_1 = require("./modelos/ubicaci\u00F3n/domicilio");
const localidad_1 = require("./modelos/ubicaci\u00F3n/localidad");
const provincia_1 = require("./modelos/ubicaci\u00F3n/provincia");
// Agrupar includes repetitivos y que son standar para varias consultas de tipo get en BD 
// hechas en el controlador de mascotas.
exports.filtro_include_ubicacion_dom_to_country = {
    association: mascota_1.masc_bt_dom,
    as: 'domicilio',
    include: [{
            association: domicilio_1.dom_bt_loc,
            as: 'localidad',
            include: [{
                    association: localidad_1.loc_bt_prov,
                    as: 'provincia',
                    include: [
                        {
                            association: provincia_1.prov_bt_pais,
                            as: 'pais'
                        }
                    ]
                }]
        }]
};
exports.filtro_include_mascota = [
    {
        association: mascota_1.masc_bt_sex,
        as: 'sexo'
    },
    {
        association: mascota_1.masc_bt_us,
        as: 'usuario'
    },
    {
        association: mascota_1.masc_bt_temp,
        as: 'temperamento'
    },
    {
        association: mascota_1.masc_bt_tam,
        as: 'tamanio'
    },
    {
        association: mascota_1.masc_bt_raz,
        as: 'raza'
    },
    {
        association: mascota_1.masc_bt_tipo,
        as: 'tipo'
    }
];
