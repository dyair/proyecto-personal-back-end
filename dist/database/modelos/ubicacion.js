"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../database");
/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto solo se exporta solo se importan librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/
exports.Domicilio = database_1.sequelize.define('domicilio', {
    id_domicilio: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNumeric: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    calle: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    numero: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            min: 0
        },
        allowNull: false,
    },
    piso: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            min: 0
        },
    },
    depto: {
        type: new sequelize_1.DataTypes.STRING(128)
    },
    // localidad_id:{
    //     type: new DataTypes.INTEGER,
    //     allowNull: false,
    //     validate:{
    //         notNull: true,
    //         min: 0
    //     }
    // },
    latitud_map: {
        type: new sequelize_1.DataTypes.DOUBLE,
        validate: {
            isFloat: true,
            min: -90,
            max: 90
        },
        allowNull: false,
    },
    longitud_map: {
        type: new sequelize_1.DataTypes.DOUBLE,
        validate: {
            isFloat: true,
            min: -180,
            max: 180
        },
        allowNull: false,
    }
}, {
    timestamps: false
    // ,
    // tableName: 'domicilio',
});
exports.localidad = database_1.sequelize.define('Localidad', {
    id_localidad: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNumeric: true
        },
        autoIncrement: true,
        primaryKey: true
    },
    nombre_localidad: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    provincia_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: true,
            min: 0
        }
    }
}, {
    timestamps: false,
    tableName: 'localidad',
});
exports.provincia = database_1.sequelize.define('Provincia', {
    id_provincia: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNumeric: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_provincia: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    pais_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        validate: {
            notNull: true,
            min: 0
        }
    }
}, {
    timestamps: false,
    tableName: 'provincia',
});
exports.pais = database_1.sequelize.define('Pais', {
    id_pais: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNumeric: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_pais: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    timestamps: false,
    tableName: 'pais',
});
