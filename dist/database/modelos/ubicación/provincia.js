"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const pais_1 = require("./pais");
class Provincia extends sequelize_1.Model {
}
exports.Provincia = Provincia;
Provincia.init({
    id_provincia: {
        type: new sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isNumeric: true,
            min: 0,
            notNull: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_provincia: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'provincia',
});
exports.prov_bt_pais = Provincia.belongsTo(pais_1.Pais, { as: 'pais', foreignKey: 'pais_id' });
exports.pais_hm_prov = pais_1.Pais.hasMany(Provincia, { as: 'provincias', foreignKey: 'pais_id' });
// Provincia.sync();
