"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
class Pais extends sequelize_1.Model {
}
exports.Pais = Pais;
Pais.init({
    id_pais: {
        type: new sequelize_1.DataTypes.INTEGER,
        allowNull: false,
        validate: {
            isNumeric: true,
            notNull: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_pais: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'pais',
});
// Pais.sync();
