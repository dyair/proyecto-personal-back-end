"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const provincia_1 = require("./provincia");
class Localidad extends sequelize_1.Model {
}
exports.Localidad = Localidad;
Localidad.init({
    id_localidad: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNumeric: true
        },
        autoIncrement: true,
        primaryKey: true
    },
    nombre_localidad: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'localidad',
});
exports.loc_bt_prov = Localidad.belongsTo(provincia_1.Provincia, { as: 'provincia', foreignKey: 'provincia_id' });
exports.prov_hm_loc = provincia_1.Provincia.hasMany(Localidad, { as: 'localidades', foreignKey: 'provincia_id' });
