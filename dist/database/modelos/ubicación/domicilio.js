"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const localidad_1 = require("./localidad");
class Domicilio extends sequelize_1.Model {
}
exports.Domicilio = Domicilio;
Domicilio.init({
    id_domicilio: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNumeric: true
        },
        autoIncrement: true,
        primaryKey: true,
    },
    calle: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    numero: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            min: 0
        },
        allowNull: false,
    },
    piso: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            min: 0
        },
    },
    depto: {
        type: new sequelize_1.DataTypes.STRING(128)
    },
    latitud_map: {
        type: new sequelize_1.DataTypes.DOUBLE,
        validate: {
            isFloat: true,
            min: -90,
            max: 90
        },
        allowNull: false,
    },
    longitud_map: {
        type: new sequelize_1.DataTypes.DOUBLE,
        validate: {
            isFloat: true,
            min: -180,
            max: 180
        },
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'domicilio',
});
exports.dom_bt_loc = Domicilio.belongsTo(localidad_1.Localidad, { as: 'localidad', foreignKey: 'localidad_id' });
exports.loc_hm_dom = localidad_1.Localidad.hasMany(Domicilio, { as: 'domicilios', foreignKey: 'localidad_id' });
// Domicilio.sync();
