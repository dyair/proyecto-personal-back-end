"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
class Comunicacion extends sequelize_1.Model {
}
exports.Comunicacion = Comunicacion;
Comunicacion.init({
    id_comunicacion: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'comunicacion_sms',
});
exports.masco_bt_cab = Comunicacion.belongsTo(mascota_1.Mascota, { as: 'mascota_origen', foreignKey: 'mascota_origen_id' });
exports.cab_ho_masco = mascota_1.Mascota.hasOne(Comunicacion, { as: 'cabecera_origen', foreignKey: 'mascota_origen_id' });
exports.mascd_bt_cab = Comunicacion.belongsTo(mascota_1.Mascota, { as: 'mascota_destino', foreignKey: 'mascota_destino_id' });
exports.cab_ho_mascd = mascota_1.Mascota.hasOne(Comunicacion, { as: 'cabecera_destino', foreignKey: 'mascota_destino_id' });
