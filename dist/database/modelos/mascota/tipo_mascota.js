"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
class Tipo_mascota extends sequelize_1.Model {
}
exports.Tipo_mascota = Tipo_mascota;
Tipo_mascota.init({
    id_tipo_mascota: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_tipo_mascota: {
        type: new sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'tipo_mascota',
});
