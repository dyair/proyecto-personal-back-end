"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
class Post_mascota extends sequelize_1.Model {
}
exports.Post_mascota = Post_mascota;
Post_mascota.init({
    id_post: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: new sequelize_1.DataTypes.STRING(512),
        allowNull: true,
    },
    fecha_publicacion: {
        type: new sequelize_1.DataTypes.DATE,
        allowNull: false,
    },
    archivos_path: {
        type: new sequelize_1.DataTypes.STRING(256),
        allowNull: true
    },
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'post_mascota',
});
exports.post_bt_masc = Post_mascota.belongsTo(mascota_1.Mascota, { as: 'mascota', foreignKey: 'mascota_id' });
exports.masc_hm_post = mascota_1.Mascota.hasMany(Post_mascota, { as: 'posteos', foreignKey: 'mascota_id' });
