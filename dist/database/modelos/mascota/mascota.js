"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const domicilio_1 = require("../ubicaci\u00F3n/domicilio");
const usuario_1 = __importDefault(require("../usuario"));
const tamanio_1 = __importDefault(require("./tamanio"));
const raza_1 = require("./raza");
const sexo_1 = __importDefault(require("./sexo"));
const temperamento_1 = __importDefault(require("./temperamento"));
const tipo_mascota_1 = require("./tipo_mascota");
/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto exporta solo librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/
class Mascota extends sequelize_1.Model {
}
exports.Mascota = Mascota;
Mascota.init({
    id_mascota: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_mascota: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false
    },
    fecha_nac: {
        type: new sequelize_1.DataTypes.DATE,
        validate: {
            isDate: true
        },
        allowNull: false
    },
    profile_pic: {
        type: new sequelize_1.DataTypes.STRING(128)
    },
    biografia: {
        type: new sequelize_1.DataTypes.STRING(128),
        defaultValue: ''
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'mascota',
});
exports.masc_bt_dom = Mascota.belongsTo(domicilio_1.Domicilio, { as: 'domicilio', foreignKey: 'domicilio_id' });
exports.dom_hm_masc = domicilio_1.Domicilio.hasMany(Mascota, { as: 'mascotas', foreignKey: 'domicilio_id' });
exports.masc_bt_us = Mascota.belongsTo(usuario_1.default, { as: 'usuario', foreignKey: 'usuario_id' });
exports.us_hm_masc = usuario_1.default.hasMany(Mascota, { as: 'mascotas', foreignKey: 'usuario_id' });
exports.masc_bt_sex = Mascota.belongsTo(sexo_1.default, { as: 'sexo', foreignKey: 'sexo_id' });
exports.sex_hm_masc = sexo_1.default.hasMany(Mascota, { as: 'mascotas', foreignKey: 'sexo_id' });
exports.masc_bt_temp = Mascota.belongsTo(temperamento_1.default, { as: 'temperamento', foreignKey: 'temperamento_id' });
exports.temp_hm_masc = temperamento_1.default.hasMany(Mascota, { as: 'mascotas', foreignKey: 'temperamento_id' });
exports.masc_bt_tam = Mascota.belongsTo(tamanio_1.default, { as: 'tamanio', foreignKey: 'tamanio_id' });
exports.tam_hm_masc = tamanio_1.default.hasMany(Mascota, { as: 'mascotas', foreignKey: 'tamanio_id' });
exports.masc_bt_raz = Mascota.belongsTo(raza_1.Raza, { as: 'raza', foreignKey: 'raza_id' });
exports.raz_hm_masc = raza_1.Raza.hasMany(Mascota, { as: 'mascotas', foreignKey: 'raza_id' });
exports.masc_bt_tipo = Mascota.belongsTo(tipo_mascota_1.Tipo_mascota, { as: 'tipo', foreignKey: 'tipo_mascota_id' });
exports.tipo_hm_masc = tipo_mascota_1.Tipo_mascota.hasMany(Mascota, { as: 'mascotas', foreignKey: 'tipo_mascota_id' });
