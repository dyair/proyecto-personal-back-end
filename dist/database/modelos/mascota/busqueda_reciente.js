"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
class Busqueda_reciente extends sequelize_1.Model {
}
exports.Busqueda_reciente = Busqueda_reciente;
Busqueda_reciente.init({
    id_busqueda: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    fecha_busqueda: {
        type: new sequelize_1.DataTypes.DATE,
        allowNull: false
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'busqueda_reciente_mascotas',
});
exports.busq_bt_mascb = Busqueda_reciente.belongsTo(mascota_1.Mascota, { as: 'mascota_buscada', foreignKey: 'mascota_buscada_id' });
exports.mascb_hm_busq = mascota_1.Mascota.hasMany(Busqueda_reciente, { as: 'busquedas', foreignKey: 'mascota_buscada_id' });
exports.busq_bt_masc = Busqueda_reciente.belongsTo(mascota_1.Mascota, { as: 'mascota_buscadora', foreignKey: 'mascota_buscadora_id' });
exports.busq_ho_masc = mascota_1.Mascota.hasOne(Busqueda_reciente, { as: 'busqueda', foreignKey: 'mascota_buscadora_id' });
