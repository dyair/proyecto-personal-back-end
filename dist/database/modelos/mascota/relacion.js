"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
class Relacion extends sequelize_1.Model {
}
exports.Relacion = Relacion;
Relacion.init({
    id_relacion: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    fecha: {
        type: new sequelize_1.DataTypes.DATE,
        allowNull: false,
    },
    match: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    // match_final: {
    //     type: DataTypes.BOOLEAN,
    //     defaultValue: false,
    //     allowNull: false
    // },
    like_origen: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: true,
        allowNull: false
    },
    like_destino: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    visto_origen: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    visto_destino: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'relacion',
});
exports.rel_bt_masco = Relacion.belongsTo(mascota_1.Mascota, { as: 'mascota_origen', foreignKey: 'mascota_origen_id' });
exports.rel_bt_mascd = Relacion.belongsTo(mascota_1.Mascota, { as: 'mascota_destino', foreignKey: 'mascota_destino_id' });
