"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
class Mascota_resumen extends sequelize_1.Model {
}
exports.Mascota_resumen = Mascota_resumen;
Mascota_resumen.init({
    id_mascota_resumen: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    foto_mascota1: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    foto_mascota2: {
        type: new sequelize_1.DataTypes.STRING(128)
    },
    foto_mascota3: {
        type: new sequelize_1.DataTypes.STRING(128)
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'mascota_resumen',
});
exports.res_bt_masc = Mascota_resumen.belongsTo(mascota_1.Mascota, { as: 'mascota', foreignKey: 'mascota_id' });
exports.masc_ho_res = mascota_1.Mascota.hasOne(Mascota_resumen, { as: 'resumen', foreignKey: 'mascota_id' });
