"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
class Relacion_detalle extends sequelize_1.Model {
}
exports.Relacion_detalle = Relacion_detalle;
Relacion_detalle.init({
    id_relacion_detalle: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    like: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
    visto: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: false,
        allowNull: false
    },
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'relacion_detalle',
});
exports.rd_bt_mas = Relacion_detalle.belongsTo(mascota_1.Mascota, { as: 'mascota', foreignKey: 'mascota_id' });
