"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
class Sexo extends sequelize_1.Model {
}
exports.default = Sexo;
Sexo.init({
    id_sexo: {
        type: new sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_sexo: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'sexo',
});
