"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const mascota_1 = require("./mascota");
const comunicacion_1 = require("./comunicacion");
class Mensaje extends sequelize_1.Model {
}
exports.Mensaje = Mensaje;
Mensaje.init({
    id_mensaje: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    fecha: {
        type: new sequelize_1.DataTypes.DATE,
        allowNull: false
    },
    contenido: {
        type: new sequelize_1.DataTypes.STRING,
        allowNull: false
    },
    visto_destino: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
    },
    visto_origen: {
        type: sequelize_1.DataTypes.BOOLEAN,
        defaultValue: true,
        allowNull: false
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'mensaje',
});
exports.sms_bt_cab = Mensaje.belongsTo(comunicacion_1.Comunicacion, { as: 'cabecera', foreignKey: 'cabecera_sms_id' });
exports.cab_hm_sms = comunicacion_1.Comunicacion.hasMany(Mensaje, { as: 'mensajes', foreignKey: 'cabecera_sms_id' });
exports.mascd_bt_sms = Mensaje.belongsTo(mascota_1.Mascota, { as: 'mascota_destino', foreignKey: 'mascota_destino_id' });
exports.sms_hm_mascd = mascota_1.Mascota.hasOne(Mensaje, { as: 'mensaje_recibido', foreignKey: 'mascota_destino_id' });
exports.masco_bt_sms = Mensaje.belongsTo(mascota_1.Mascota, { as: 'mascota_origen', foreignKey: 'mascota_origen_id' });
exports.sms_hm_masco = mascota_1.Mascota.hasOne(Mensaje, { as: 'mensaje_enviado', foreignKey: 'mascota_origen_id' });
