"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../../database");
const tipo_mascota_1 = require("./tipo_mascota");
class Raza extends sequelize_1.Model {
}
exports.Raza = Raza;
Raza.init({
    id_raza: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_raza: {
        type: new sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'raza',
});
exports.raz_bt_tmascota = Raza.belongsTo(tipo_mascota_1.Tipo_mascota, { as: 'tipo_mascota', foreignKey: 'tipo_mascota_id' });
exports.tmascota_hm_raza = tipo_mascota_1.Tipo_mascota.hasMany(Raza, { as: 'razas', foreignKey: 'tipo_mascota_id' });
