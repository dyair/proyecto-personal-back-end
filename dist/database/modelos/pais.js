"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../database");
const provincia_1 = __importDefault(require("./provincia"));
/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto solo se exporta solo se importan librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/
class Pais extends sequelize_1.Model {
}
Pais.init({
    id_pais: {
        type: new sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_pais: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'pais',
});
Pais.hasMany(provincia_1.default, { foreignKey: 'pais_id' });
exports.default = Pais;
