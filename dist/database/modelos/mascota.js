"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../database");
const ubicacion_1 = require("./ubicacion");
const usuario_1 = require("./usuario");
/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto exporta solo librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/
class Mascota extends sequelize_1.Model {
}
exports.Mascota = Mascota;
class Temperamento extends sequelize_1.Model {
}
exports.Temperamento = Temperamento;
class Raza extends sequelize_1.Model {
}
exports.Raza = Raza;
class Sexo extends sequelize_1.Model {
}
exports.Sexo = Sexo;
class Tamanio extends sequelize_1.Model {
}
exports.Tamanio = Tamanio;
Mascota.init({
    id_mascota: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_mascota: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false
    },
    domicilio_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        references: {
            model: ubicacion_1.Domicilio,
            key: 'id_domicilio'
        }
    },
    fecha_nac: {
        type: new sequelize_1.DataTypes.DATE,
        validate: {
            isDate: true
        },
        allowNull: false
    },
    tamanio_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        references: {
            model: Tamanio,
            key: 'id_tamanio'
        }
    },
    profile_pic: {
        type: new sequelize_1.DataTypes.BLOB('long')
    },
    usuario_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        references: {
            model: usuario_1.Usuario,
            key: 'id_usuario'
        }
    },
    raza_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        references: {
            model: Raza,
            key: 'id_raza'
        }
    },
    sexo_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        references: {
            model: Sexo,
            key: 'id_sexo'
        }
    },
    temperamento_id: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isInt: true,
            isNull: false,
            min: 0
        },
        references: {
            model: Temperamento,
            key: 'id_temperamento'
        }
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'mascota',
});
Temperamento.init({
    id_temperamento: {
        type: new sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_temperamento: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'temperamento',
});
Raza.init({
    id_raza: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_raza: {
        type: new sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'raza',
});
Sexo.init({
    id_sexo: {
        type: new sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_sexo: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'sexo',
});
Tamanio.init({
    id_tamanio: {
        type: new sequelize_1.DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_tamanio: {
        type: new sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'tamanio',
});
