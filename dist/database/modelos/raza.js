"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const database_1 = require("../database");
/*
Acá lo que hago es como crear el molde para la tabla. Luego, las acciones sobre la tabla se generan llamando al
objeto que exporto desde este archivo, justamente. Esto solo se exporta solo se importan librerias para armar
los moldes, nada más. Ej: en algún controller ---> usuario.find(1).then()....s
*/
class Raza extends sequelize_1.Model {
}
Raza.init({
    id_raza: {
        type: new sequelize_1.DataTypes.INTEGER,
        validate: {
            isNull: false,
            isInt: true,
            min: 0
        },
        autoIncrement: true,
        primaryKey: true,
    },
    nombre_raza: {
        type: new sequelize_1.DataTypes.STRING(45),
        allowNull: false,
    }
}, {
    sequelize: database_1.sequelize,
    timestamps: false,
    tableName: 'raza',
});
exports.default = Raza;
